{
    "id": "1b12f7f5-5a13-464f-8ccf-d0da16fefb51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 1,
    "bbox_right": 54,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e83812f3-e2c4-482b-a5e4-5960c03e22f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b12f7f5-5a13-464f-8ccf-d0da16fefb51",
            "compositeImage": {
                "id": "4371fad4-f583-489a-9fc0-1fba77cf5eb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e83812f3-e2c4-482b-a5e4-5960c03e22f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b23709a-854c-4746-8159-daef0fda3d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e83812f3-e2c4-482b-a5e4-5960c03e22f6",
                    "LayerId": "e085e200-fc6d-454a-8870-ab73737f2890"
                }
            ]
        },
        {
            "id": "614aeacc-ad69-4054-b811-0e079b4ae569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b12f7f5-5a13-464f-8ccf-d0da16fefb51",
            "compositeImage": {
                "id": "463a5528-9cf8-4515-a4b0-8caee15c1f07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614aeacc-ad69-4054-b811-0e079b4ae569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5b4493d-6095-4b3d-9fd8-fd44f023a113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614aeacc-ad69-4054-b811-0e079b4ae569",
                    "LayerId": "e085e200-fc6d-454a-8870-ab73737f2890"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "e085e200-fc6d-454a-8870-ab73737f2890",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b12f7f5-5a13-464f-8ccf-d0da16fefb51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 27,
    "yorig": 41
}