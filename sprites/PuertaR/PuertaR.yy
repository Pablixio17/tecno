{
    "id": "c2f07c1c-3cb9-4c53-93d0-115a1dac9778",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PuertaR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 62,
    "bbox_right": 75,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24176a2e-0238-4f4f-bd69-2083770f9db6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2f07c1c-3cb9-4c53-93d0-115a1dac9778",
            "compositeImage": {
                "id": "26c71265-8634-4fe2-b3ac-f77bd5c87432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24176a2e-0238-4f4f-bd69-2083770f9db6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60c4709f-0682-4e98-9b53-f23cd8769ec0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24176a2e-0238-4f4f-bd69-2083770f9db6",
                    "LayerId": "bcb411dc-dfa8-4597-a0dc-075d5d4e40d8"
                }
            ]
        },
        {
            "id": "5835b612-e6d4-43d9-bc85-1e7d98a599aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2f07c1c-3cb9-4c53-93d0-115a1dac9778",
            "compositeImage": {
                "id": "bdd376e2-9260-412c-bf2e-1e18a94774f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5835b612-e6d4-43d9-bc85-1e7d98a599aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ea5aa41-7c25-4776-94c5-3b2f370179f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5835b612-e6d4-43d9-bc85-1e7d98a599aa",
                    "LayerId": "bcb411dc-dfa8-4597-a0dc-075d5d4e40d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bcb411dc-dfa8-4597-a0dc-075d5d4e40d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2f07c1c-3cb9-4c53-93d0-115a1dac9778",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}