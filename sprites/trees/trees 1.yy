{
    "id": "6f33862c-4359-4caa-8ecb-06c6ce170ff6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "trees",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 535,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 152,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfa1fdf8-f478-44db-be83-dea9568296cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f33862c-4359-4caa-8ecb-06c6ce170ff6",
            "compositeImage": {
                "id": "c46f0e0c-b807-4578-b8e6-5d846f9667db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa1fdf8-f478-44db-be83-dea9568296cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bb0256b-9424-485a-8a44-538e1c8c63be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa1fdf8-f478-44db-be83-dea9568296cd",
                    "LayerId": "af24b67b-a4c8-4b09-83b8-94ac9886f4e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 536,
    "layers": [
        {
            "id": "af24b67b-a4c8-4b09-83b8-94ac9886f4e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f33862c-4359-4caa-8ecb-06c6ce170ff6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}