{
    "id": "cd6d8d62-a054-462e-85c8-d59b4083f356",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ventana_rota",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 144,
    "bbox_left": 86,
    "bbox_right": 95,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c1cc1bf-8dea-48e3-b8e3-a99ea6641730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd6d8d62-a054-462e-85c8-d59b4083f356",
            "compositeImage": {
                "id": "a5c75024-1d7f-4f62-a724-743b715c390e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c1cc1bf-8dea-48e3-b8e3-a99ea6641730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb9b4d7-06cb-431c-8a86-d60e8c7f658e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c1cc1bf-8dea-48e3-b8e3-a99ea6641730",
                    "LayerId": "cacf9972-e5c0-43c0-bd3c-492e1b747694"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "cacf9972-e5c0-43c0-bd3c-492e1b747694",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd6d8d62-a054-462e-85c8-d59b4083f356",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}