{
    "id": "a3b1130c-6b4d-4af8-afbf-e0877c39358e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_text",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cace60ca-860f-46fc-a9f2-5dbd1860ae9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b1130c-6b4d-4af8-afbf-e0877c39358e",
            "compositeImage": {
                "id": "1b35e893-a06f-4261-a78a-bf8d21b3172d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cace60ca-860f-46fc-a9f2-5dbd1860ae9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26c4c952-b7a1-4623-90b4-bf9126ebaecc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cace60ca-860f-46fc-a9f2-5dbd1860ae9f",
                    "LayerId": "4b87dfbd-73f7-4cc1-8086-b9d5d17b1ac7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4b87dfbd-73f7-4cc1-8086-b9d5d17b1ac7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3b1130c-6b4d-4af8-afbf-e0877c39358e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}