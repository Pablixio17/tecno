{
    "id": "25d61280-558f-45df-a158-83acf06d50ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTextBoxBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa5bdd76-50ab-4b7e-a36c-a4da712e5c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25d61280-558f-45df-a158-83acf06d50ba",
            "compositeImage": {
                "id": "93216364-51c2-41c3-bebe-ec107f0c00cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa5bdd76-50ab-4b7e-a36c-a4da712e5c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14cabf50-46b2-4ad4-9015-0cf476320649",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa5bdd76-50ab-4b7e-a36c-a4da712e5c60",
                    "LayerId": "3ec7ed6b-2203-485f-a42b-27a4cf0d5c30"
                }
            ]
        },
        {
            "id": "157db14e-fcf8-4ad3-b876-29e2e61635ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25d61280-558f-45df-a158-83acf06d50ba",
            "compositeImage": {
                "id": "c5b748a7-a326-40d5-bb48-6b4f3a08877a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "157db14e-fcf8-4ad3-b876-29e2e61635ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9941e927-6842-4152-a9ee-7e80744ee03e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "157db14e-fcf8-4ad3-b876-29e2e61635ee",
                    "LayerId": "3ec7ed6b-2203-485f-a42b-27a4cf0d5c30"
                }
            ]
        },
        {
            "id": "09cc4bd5-5cab-42b1-9e86-550c6e8d121d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25d61280-558f-45df-a158-83acf06d50ba",
            "compositeImage": {
                "id": "4cb6a9c3-22e2-4aef-99e5-e4fe5a7223ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09cc4bd5-5cab-42b1-9e86-550c6e8d121d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40121de-6e4b-4a16-b293-ef5b312c0e99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09cc4bd5-5cab-42b1-9e86-550c6e8d121d",
                    "LayerId": "3ec7ed6b-2203-485f-a42b-27a4cf0d5c30"
                }
            ]
        },
        {
            "id": "79c77e2b-c3a4-4a24-8c43-fbfa7b8fd135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25d61280-558f-45df-a158-83acf06d50ba",
            "compositeImage": {
                "id": "99b5e239-36b2-40b7-8d3e-d5632bf11119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79c77e2b-c3a4-4a24-8c43-fbfa7b8fd135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e262832-5474-4764-81b1-b7f1f1c47b32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79c77e2b-c3a4-4a24-8c43-fbfa7b8fd135",
                    "LayerId": "3ec7ed6b-2203-485f-a42b-27a4cf0d5c30"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 24,
    "layers": [
        {
            "id": "3ec7ed6b-2203-485f-a42b-27a4cf0d5c30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25d61280-558f-45df-a158-83acf06d50ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}