{
    "id": "78dfb83c-c58e-4f6f-b772-e7b8cf71986b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCave2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d4ddb33-d965-426d-b68e-5b1b3b43045f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78dfb83c-c58e-4f6f-b772-e7b8cf71986b",
            "compositeImage": {
                "id": "abd21091-8dd2-4587-b7ae-1eec4096500a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d4ddb33-d965-426d-b68e-5b1b3b43045f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "590072e2-032b-470e-9bfd-6103519113e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d4ddb33-d965-426d-b68e-5b1b3b43045f",
                    "LayerId": "7996e115-a1fb-4469-a66d-12ec4902f580"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "7996e115-a1fb-4469-a66d-12ec4902f580",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78dfb83c-c58e-4f6f-b772-e7b8cf71986b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}