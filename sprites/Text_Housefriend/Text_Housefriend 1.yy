{
    "id": "a9591bbf-77ad-48cf-9195-2ede6d5d4ea9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Text_Housefriend",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e70c517-e6ad-41b7-9464-540186d930b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9591bbf-77ad-48cf-9195-2ede6d5d4ea9",
            "compositeImage": {
                "id": "d1188ae5-409a-4ef7-ae51-77bd4b575c57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e70c517-e6ad-41b7-9464-540186d930b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced2ba6a-3cf2-4cc5-8d00-fdaebde886ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e70c517-e6ad-41b7-9464-540186d930b6",
                    "LayerId": "dd8dfb2a-edb8-4701-bc48-e91242483a8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "dd8dfb2a-edb8-4701-bc48-e91242483a8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9591bbf-77ad-48cf-9195-2ede6d5d4ea9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": -234,
    "yorig": 13
}