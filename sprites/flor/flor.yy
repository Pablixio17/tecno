{
    "id": "5710f6d4-57e7-4233-af59-20b0f5a3fd10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1737522f-ee46-4849-8714-33b833c76c9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5710f6d4-57e7-4233-af59-20b0f5a3fd10",
            "compositeImage": {
                "id": "5d84b55b-4980-47f5-9bd6-f9a047bed184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1737522f-ee46-4849-8714-33b833c76c9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f4b0d42-31aa-4e38-89ca-e4f0a9980d59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1737522f-ee46-4849-8714-33b833c76c9b",
                    "LayerId": "5d15f940-9028-484d-a36f-5749bb3ea4e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5d15f940-9028-484d-a36f-5749bb3ea4e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5710f6d4-57e7-4233-af59-20b0f5a3fd10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}