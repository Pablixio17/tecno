{
    "id": "f2be2462-47da-4768-b5e7-d6248648b184",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b6c6805-0731-42db-a872-100db48a83a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2be2462-47da-4768-b5e7-d6248648b184",
            "compositeImage": {
                "id": "692acf4b-7516-4307-af40-df1bb7b5cff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b6c6805-0731-42db-a872-100db48a83a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd2ac37b-df63-4887-8fdf-ecc15bc9d5c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b6c6805-0731-42db-a872-100db48a83a6",
                    "LayerId": "4acf0e4f-7a8d-46ea-af1c-34f002d2c396"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4acf0e4f-7a8d-46ea-af1c-34f002d2c396",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2be2462-47da-4768-b5e7-d6248648b184",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}