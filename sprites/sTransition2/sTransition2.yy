{
    "id": "005defc4-2cd3-4198-95b0-d19580759604",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTransition2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eaabbe0d-53dd-4f10-bf93-aa71e4312857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005defc4-2cd3-4198-95b0-d19580759604",
            "compositeImage": {
                "id": "04949c9b-cfb0-483e-9afa-7ad7bee43a2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaabbe0d-53dd-4f10-bf93-aa71e4312857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a337c60-b2e3-49b7-aa2b-0562a903b7e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaabbe0d-53dd-4f10-bf93-aa71e4312857",
                    "LayerId": "7e2bbf8f-6804-4ded-8428-17cefdaa3bc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7e2bbf8f-6804-4ded-8428-17cefdaa3bc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "005defc4-2cd3-4198-95b0-d19580759604",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}