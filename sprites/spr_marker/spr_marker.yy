{
    "id": "2017e5a1-bb7e-4684-913f-1e5b9602adb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb03e6e7-a81b-4601-af4b-dbc23bbcc87c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2017e5a1-bb7e-4684-913f-1e5b9602adb7",
            "compositeImage": {
                "id": "1b6d849d-de4c-4078-81bb-24d6539d99e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb03e6e7-a81b-4601-af4b-dbc23bbcc87c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e14eb64-7215-459a-ba3c-63311529e1ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb03e6e7-a81b-4601-af4b-dbc23bbcc87c",
                    "LayerId": "c54d79bb-d323-4484-a702-b8c29a64df05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "c54d79bb-d323-4484-a702-b8c29a64df05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2017e5a1-bb7e-4684-913f-1e5b9602adb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 0
}