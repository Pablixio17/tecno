{
    "id": "a39d19be-c349-4439-939d-4f40400244c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Frontcave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8994738b-fe6d-4f87-90b2-586859d2eb89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a39d19be-c349-4439-939d-4f40400244c0",
            "compositeImage": {
                "id": "7c4f4b1f-708d-4f3f-bb85-ad99f4c8a5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8994738b-fe6d-4f87-90b2-586859d2eb89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f5d7cee-ab6f-415d-a799-5c36d47bf0f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8994738b-fe6d-4f87-90b2-586859d2eb89",
                    "LayerId": "90db5b5f-47a0-4a59-9cb3-331347fb3256"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "90db5b5f-47a0-4a59-9cb3-331347fb3256",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a39d19be-c349-4439-939d-4f40400244c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 61,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}