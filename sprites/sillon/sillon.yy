{
    "id": "23d188e5-5504-4a62-ac32-fcc528b48c0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sillon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab12fde0-c5eb-4495-94f8-53ee414bde9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d188e5-5504-4a62-ac32-fcc528b48c0a",
            "compositeImage": {
                "id": "8678e726-0093-476d-99ca-300a35ea59b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab12fde0-c5eb-4495-94f8-53ee414bde9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaa8462e-caef-4103-91ba-060052b3921a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab12fde0-c5eb-4495-94f8-53ee414bde9f",
                    "LayerId": "32bb7137-1d33-429a-8826-b56950dd7677"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "32bb7137-1d33-429a-8826-b56950dd7677",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23d188e5-5504-4a62-ac32-fcc528b48c0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}