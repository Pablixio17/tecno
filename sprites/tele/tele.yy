{
    "id": "24391afd-a75e-48a9-a7cb-3033c72a8fe0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tele",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 4,
    "bbox_right": 59,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68096ec5-1e43-4ac1-b33f-a5fc4970d5b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24391afd-a75e-48a9-a7cb-3033c72a8fe0",
            "compositeImage": {
                "id": "3402cde3-64dd-4bea-844c-1fa9cb5d0385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68096ec5-1e43-4ac1-b33f-a5fc4970d5b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6749cae-b31c-45f7-b2a0-9b2dfa956f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68096ec5-1e43-4ac1-b33f-a5fc4970d5b8",
                    "LayerId": "abf40811-e13a-45fe-9e10-3eb059b24a67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "abf40811-e13a-45fe-9e10-3eb059b24a67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24391afd-a75e-48a9-a7cb-3033c72a8fe0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}