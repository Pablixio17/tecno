{
    "id": "b24cd18a-c885-45cc-ad30-1c65aa53ee5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SYMI_A",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 7,
    "bbox_right": 47,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "351c981c-4d28-40af-9e6d-36ac6220311c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b24cd18a-c885-45cc-ad30-1c65aa53ee5c",
            "compositeImage": {
                "id": "328c1783-2a71-45e1-8a42-4459ba6f02ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "351c981c-4d28-40af-9e6d-36ac6220311c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19f95eb6-1594-43ba-b757-3a5cd4f953fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "351c981c-4d28-40af-9e6d-36ac6220311c",
                    "LayerId": "87ee1083-6dfe-47e2-900e-69b50e65c472"
                }
            ]
        },
        {
            "id": "61dc1570-361c-4f6b-8b83-c4f11740ed37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b24cd18a-c885-45cc-ad30-1c65aa53ee5c",
            "compositeImage": {
                "id": "afd4c1f0-4e47-42ae-aa15-99f6ddb51486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61dc1570-361c-4f6b-8b83-c4f11740ed37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9719174e-ce36-41bc-9335-1e590cb30f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61dc1570-361c-4f6b-8b83-c4f11740ed37",
                    "LayerId": "87ee1083-6dfe-47e2-900e-69b50e65c472"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 88,
    "layers": [
        {
            "id": "87ee1083-6dfe-47e2-900e-69b50e65c472",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b24cd18a-c885-45cc-ad30-1c65aa53ee5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}