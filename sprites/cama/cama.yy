{
    "id": "91f95d2d-1c51-46e2-8fe8-aaf4722434bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cama",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 116,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4079c951-4ca3-4eed-ba91-8eedcfc80796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91f95d2d-1c51-46e2-8fe8-aaf4722434bb",
            "compositeImage": {
                "id": "d88d0edd-000e-444a-a332-83e6d6e651e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4079c951-4ca3-4eed-ba91-8eedcfc80796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d57dc1e5-3e76-47a1-a1dd-204cdb65d012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4079c951-4ca3-4eed-ba91-8eedcfc80796",
                    "LayerId": "d275301f-03e3-4f79-95c8-f1af60e9e1b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d275301f-03e3-4f79-95c8-f1af60e9e1b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91f95d2d-1c51-46e2-8fe8-aaf4722434bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 188,
    "yorig": 22
}