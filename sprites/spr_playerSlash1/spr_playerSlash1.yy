{
    "id": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerSlash1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 6,
    "bbox_right": 56,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47b4f85e-8a12-4bb8-b6dd-64d4d91715cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "compositeImage": {
                "id": "24032279-fdc1-4e26-a732-8d9b6dc8e7cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b4f85e-8a12-4bb8-b6dd-64d4d91715cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0820f086-989d-45a5-a61f-9c6c99dd5c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b4f85e-8a12-4bb8-b6dd-64d4d91715cd",
                    "LayerId": "c42da62e-5232-468a-9f40-7edfd9bb8a51"
                },
                {
                    "id": "38b86fe8-d2a2-4905-9d5c-512abd65b287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b4f85e-8a12-4bb8-b6dd-64d4d91715cd",
                    "LayerId": "30b59a27-7c7f-4772-8094-5f69bce7668d"
                },
                {
                    "id": "e2f37d2e-e85a-422f-b8e1-28c497dfd077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b4f85e-8a12-4bb8-b6dd-64d4d91715cd",
                    "LayerId": "4be8db19-3963-4ff1-b965-44cc1b018136"
                },
                {
                    "id": "044cc25f-5a31-41ad-a277-f66526ba03cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b4f85e-8a12-4bb8-b6dd-64d4d91715cd",
                    "LayerId": "9f48e170-5a65-4e26-91d6-702f02e960d1"
                },
                {
                    "id": "e88b2e51-ae06-443e-b857-d4c071f25527",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b4f85e-8a12-4bb8-b6dd-64d4d91715cd",
                    "LayerId": "c5fef39c-1a3c-4cd6-91fb-252732bdc1ae"
                },
                {
                    "id": "bc2674a8-b80f-4713-9008-b14a8059cca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b4f85e-8a12-4bb8-b6dd-64d4d91715cd",
                    "LayerId": "4dcd8083-c3a9-4041-9eee-3c7e5cfeee39"
                }
            ]
        },
        {
            "id": "12f72bc3-d93c-4030-bdf2-45885cf02d36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "compositeImage": {
                "id": "10d484f4-b9fb-4755-8756-988331a52b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f72bc3-d93c-4030-bdf2-45885cf02d36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4703a110-10d4-4837-a269-d71819d8864c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f72bc3-d93c-4030-bdf2-45885cf02d36",
                    "LayerId": "c42da62e-5232-468a-9f40-7edfd9bb8a51"
                },
                {
                    "id": "eae36b5c-2e9a-4fb1-8c42-f77be1bc3249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f72bc3-d93c-4030-bdf2-45885cf02d36",
                    "LayerId": "30b59a27-7c7f-4772-8094-5f69bce7668d"
                },
                {
                    "id": "37705ba5-5f0c-4880-9515-421fd3decd97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f72bc3-d93c-4030-bdf2-45885cf02d36",
                    "LayerId": "4be8db19-3963-4ff1-b965-44cc1b018136"
                },
                {
                    "id": "c8409131-7feb-4e7d-9845-3b2ac5f82db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f72bc3-d93c-4030-bdf2-45885cf02d36",
                    "LayerId": "9f48e170-5a65-4e26-91d6-702f02e960d1"
                },
                {
                    "id": "25f0d51a-0809-4f77-9096-08dcfc0b43f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f72bc3-d93c-4030-bdf2-45885cf02d36",
                    "LayerId": "c5fef39c-1a3c-4cd6-91fb-252732bdc1ae"
                },
                {
                    "id": "64be98f7-4b41-4bd5-bf0a-176232d5e230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f72bc3-d93c-4030-bdf2-45885cf02d36",
                    "LayerId": "4dcd8083-c3a9-4041-9eee-3c7e5cfeee39"
                }
            ]
        },
        {
            "id": "66e39832-e4c7-4bff-ac54-478ceb02cf57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "compositeImage": {
                "id": "d6d4f00e-0c73-4c6e-a238-84d415db7ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66e39832-e4c7-4bff-ac54-478ceb02cf57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e0489ed-4c8f-45ed-bb83-d3398c25a253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e39832-e4c7-4bff-ac54-478ceb02cf57",
                    "LayerId": "c42da62e-5232-468a-9f40-7edfd9bb8a51"
                },
                {
                    "id": "35a69b86-9878-452c-9ce5-39fde418fc52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e39832-e4c7-4bff-ac54-478ceb02cf57",
                    "LayerId": "30b59a27-7c7f-4772-8094-5f69bce7668d"
                },
                {
                    "id": "07d92bf9-4318-4202-a9b0-d77f96fe0f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e39832-e4c7-4bff-ac54-478ceb02cf57",
                    "LayerId": "4be8db19-3963-4ff1-b965-44cc1b018136"
                },
                {
                    "id": "5a83e4b8-c8b8-418f-93a4-e9ee5824c6af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e39832-e4c7-4bff-ac54-478ceb02cf57",
                    "LayerId": "9f48e170-5a65-4e26-91d6-702f02e960d1"
                },
                {
                    "id": "5ce7ed48-be10-44b3-91c0-aaeb8c8dea2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e39832-e4c7-4bff-ac54-478ceb02cf57",
                    "LayerId": "c5fef39c-1a3c-4cd6-91fb-252732bdc1ae"
                },
                {
                    "id": "0fab590d-0788-4f7d-9661-bc01dfe437e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e39832-e4c7-4bff-ac54-478ceb02cf57",
                    "LayerId": "4dcd8083-c3a9-4041-9eee-3c7e5cfeee39"
                }
            ]
        },
        {
            "id": "052df935-eaa3-4a47-ad67-e0029daf5d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "compositeImage": {
                "id": "408c5fb0-cae1-4ec0-9b14-1ac0946ff37b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "052df935-eaa3-4a47-ad67-e0029daf5d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e31bfe8-f619-4e40-bb51-41f1878a13b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052df935-eaa3-4a47-ad67-e0029daf5d4e",
                    "LayerId": "c42da62e-5232-468a-9f40-7edfd9bb8a51"
                },
                {
                    "id": "bfe172c4-e27e-4476-834e-24c1b0624539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052df935-eaa3-4a47-ad67-e0029daf5d4e",
                    "LayerId": "30b59a27-7c7f-4772-8094-5f69bce7668d"
                },
                {
                    "id": "86498603-3ca1-47c2-81b3-8c4bc1af9971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052df935-eaa3-4a47-ad67-e0029daf5d4e",
                    "LayerId": "4be8db19-3963-4ff1-b965-44cc1b018136"
                },
                {
                    "id": "67510cc2-c7e0-4955-abaf-c21fb565bcde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052df935-eaa3-4a47-ad67-e0029daf5d4e",
                    "LayerId": "9f48e170-5a65-4e26-91d6-702f02e960d1"
                },
                {
                    "id": "247f8311-10eb-46cd-b0da-1dac704d6a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052df935-eaa3-4a47-ad67-e0029daf5d4e",
                    "LayerId": "c5fef39c-1a3c-4cd6-91fb-252732bdc1ae"
                },
                {
                    "id": "fbe59875-5f48-4711-9a52-bd562cac5037",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052df935-eaa3-4a47-ad67-e0029daf5d4e",
                    "LayerId": "4dcd8083-c3a9-4041-9eee-3c7e5cfeee39"
                }
            ]
        },
        {
            "id": "6411b7f7-f630-460c-a181-06c644a52575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "compositeImage": {
                "id": "93319841-79d1-4988-98dd-e4a3bb8f2cc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6411b7f7-f630-460c-a181-06c644a52575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "776bfcf2-6149-4c77-9b30-5484c5ce9238",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6411b7f7-f630-460c-a181-06c644a52575",
                    "LayerId": "c42da62e-5232-468a-9f40-7edfd9bb8a51"
                },
                {
                    "id": "77e51362-7965-4a2b-94e5-31f1ef2cea95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6411b7f7-f630-460c-a181-06c644a52575",
                    "LayerId": "30b59a27-7c7f-4772-8094-5f69bce7668d"
                },
                {
                    "id": "047ab775-7c3e-4cdf-ae01-da97af5f10bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6411b7f7-f630-460c-a181-06c644a52575",
                    "LayerId": "4be8db19-3963-4ff1-b965-44cc1b018136"
                },
                {
                    "id": "70760f86-f78f-4380-855d-4acd7f02c2ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6411b7f7-f630-460c-a181-06c644a52575",
                    "LayerId": "9f48e170-5a65-4e26-91d6-702f02e960d1"
                },
                {
                    "id": "69677578-7a07-48a5-8db2-4f7b59abec75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6411b7f7-f630-460c-a181-06c644a52575",
                    "LayerId": "c5fef39c-1a3c-4cd6-91fb-252732bdc1ae"
                },
                {
                    "id": "608f8bfb-5d11-4f90-bff7-3e13e679bc6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6411b7f7-f630-460c-a181-06c644a52575",
                    "LayerId": "4dcd8083-c3a9-4041-9eee-3c7e5cfeee39"
                }
            ]
        },
        {
            "id": "1bf8f88d-e052-4435-861d-e0c1a3c8d88c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "compositeImage": {
                "id": "930a656a-44d5-40cf-acc2-bf6189dc34fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bf8f88d-e052-4435-861d-e0c1a3c8d88c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b833e4a9-8f4e-426b-86a2-3140946531b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf8f88d-e052-4435-861d-e0c1a3c8d88c",
                    "LayerId": "c42da62e-5232-468a-9f40-7edfd9bb8a51"
                },
                {
                    "id": "5de9d7be-0a58-490d-ac37-1b5a088c66af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf8f88d-e052-4435-861d-e0c1a3c8d88c",
                    "LayerId": "30b59a27-7c7f-4772-8094-5f69bce7668d"
                },
                {
                    "id": "fa992aaa-c6bc-4826-a4d7-a4cc56d7dfe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf8f88d-e052-4435-861d-e0c1a3c8d88c",
                    "LayerId": "4be8db19-3963-4ff1-b965-44cc1b018136"
                },
                {
                    "id": "7ca23118-7b4c-4a98-853b-73b75075b073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf8f88d-e052-4435-861d-e0c1a3c8d88c",
                    "LayerId": "9f48e170-5a65-4e26-91d6-702f02e960d1"
                },
                {
                    "id": "a3fc7409-3e60-4617-9e12-400afec2a9c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf8f88d-e052-4435-861d-e0c1a3c8d88c",
                    "LayerId": "c5fef39c-1a3c-4cd6-91fb-252732bdc1ae"
                },
                {
                    "id": "8f5f5bbe-542f-4920-9e0b-34ff81553514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf8f88d-e052-4435-861d-e0c1a3c8d88c",
                    "LayerId": "4dcd8083-c3a9-4041-9eee-3c7e5cfeee39"
                }
            ]
        },
        {
            "id": "158451f3-79f6-4d05-8068-65958c69edc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "compositeImage": {
                "id": "c59adadf-61d3-4e11-b1e9-869627a17313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "158451f3-79f6-4d05-8068-65958c69edc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5653477d-98e6-4537-9d1a-d3ef91ac62e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158451f3-79f6-4d05-8068-65958c69edc7",
                    "LayerId": "c42da62e-5232-468a-9f40-7edfd9bb8a51"
                },
                {
                    "id": "fb537470-6370-4033-8db5-ef6e36f9d69c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158451f3-79f6-4d05-8068-65958c69edc7",
                    "LayerId": "30b59a27-7c7f-4772-8094-5f69bce7668d"
                },
                {
                    "id": "e014860c-34fd-415c-adc4-15ad1a1d9001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158451f3-79f6-4d05-8068-65958c69edc7",
                    "LayerId": "4be8db19-3963-4ff1-b965-44cc1b018136"
                },
                {
                    "id": "78f49bad-ded4-40cc-991d-501413b074a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158451f3-79f6-4d05-8068-65958c69edc7",
                    "LayerId": "9f48e170-5a65-4e26-91d6-702f02e960d1"
                },
                {
                    "id": "3996a6e7-60ce-4f6f-8362-2357da6d6369",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158451f3-79f6-4d05-8068-65958c69edc7",
                    "LayerId": "c5fef39c-1a3c-4cd6-91fb-252732bdc1ae"
                },
                {
                    "id": "a983a623-a11c-4639-ae8f-399ca548e9a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158451f3-79f6-4d05-8068-65958c69edc7",
                    "LayerId": "4dcd8083-c3a9-4041-9eee-3c7e5cfeee39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 99,
    "layers": [
        {
            "id": "c42da62e-5232-468a-9f40-7edfd9bb8a51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "30b59a27-7c7f-4772-8094-5f69bce7668d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4be8db19-3963-4ff1-b965-44cc1b018136",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9f48e170-5a65-4e26-91d6-702f02e960d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c5fef39c-1a3c-4cd6-91fb-252732bdc1ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 5",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4dcd8083-c3a9-4041-9eee-3c7e5cfeee39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94e6dba0-2a5b-4c37-8844-9158d73c9850",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 49
}