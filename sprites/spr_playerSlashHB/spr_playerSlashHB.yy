{
    "id": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerSlashHB",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7069efb-deae-498a-adad-5aae29902c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "compositeImage": {
                "id": "068efb01-6e7f-4933-8a8f-05ce1b0cd015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7069efb-deae-498a-adad-5aae29902c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1caff79d-ec2c-4658-9010-86eb7ecbc001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7069efb-deae-498a-adad-5aae29902c87",
                    "LayerId": "1dff8ec8-2fd8-4436-b89c-1dcf81ee5792"
                },
                {
                    "id": "f6ccb283-80e9-4ab8-bcdc-bcae8ef9d7c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7069efb-deae-498a-adad-5aae29902c87",
                    "LayerId": "f293b41d-2e27-4b98-9b03-301401a91d81"
                },
                {
                    "id": "281e1b55-597d-4b2f-95c3-3d1acd74766f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7069efb-deae-498a-adad-5aae29902c87",
                    "LayerId": "d393dc7e-a3dc-4798-bf11-4f2db6bb2424"
                },
                {
                    "id": "1257a515-3440-4500-a143-7789b954979a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7069efb-deae-498a-adad-5aae29902c87",
                    "LayerId": "495133b8-0231-48a0-9439-afaad41bd0c2"
                },
                {
                    "id": "28827fb2-1fa5-423d-8d57-c7d724dba9ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7069efb-deae-498a-adad-5aae29902c87",
                    "LayerId": "ba948ef9-6950-4ab9-b6ed-6e167139bcd2"
                },
                {
                    "id": "804eecde-3e05-4381-97b4-6b6503c6fc23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7069efb-deae-498a-adad-5aae29902c87",
                    "LayerId": "0edb2861-28de-40f6-921f-b46a4f1be229"
                },
                {
                    "id": "5a823e43-0722-4dfb-91bb-7d02f01835d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7069efb-deae-498a-adad-5aae29902c87",
                    "LayerId": "90eb7216-7e67-4422-9967-948aac2f22fa"
                }
            ]
        },
        {
            "id": "8f8ded6b-5488-4f19-ac9e-3f5858c2ed8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "compositeImage": {
                "id": "ddbcf603-da3e-4164-9c37-b776b57d61a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f8ded6b-5488-4f19-ac9e-3f5858c2ed8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afe28b9f-a4aa-460e-aa8f-b35e909d4596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8ded6b-5488-4f19-ac9e-3f5858c2ed8f",
                    "LayerId": "1dff8ec8-2fd8-4436-b89c-1dcf81ee5792"
                },
                {
                    "id": "f014ee3e-c17c-473f-a54c-dbb0ce23a409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8ded6b-5488-4f19-ac9e-3f5858c2ed8f",
                    "LayerId": "f293b41d-2e27-4b98-9b03-301401a91d81"
                },
                {
                    "id": "d282646a-8d0a-4462-a838-8e303668ff0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8ded6b-5488-4f19-ac9e-3f5858c2ed8f",
                    "LayerId": "d393dc7e-a3dc-4798-bf11-4f2db6bb2424"
                },
                {
                    "id": "51438bb9-8f1d-452c-b8f0-fc97a07e2e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8ded6b-5488-4f19-ac9e-3f5858c2ed8f",
                    "LayerId": "495133b8-0231-48a0-9439-afaad41bd0c2"
                },
                {
                    "id": "448c6f2c-5fa2-4d3c-b9fe-b32b771b601c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8ded6b-5488-4f19-ac9e-3f5858c2ed8f",
                    "LayerId": "ba948ef9-6950-4ab9-b6ed-6e167139bcd2"
                },
                {
                    "id": "7845b553-ccad-4558-ac87-aaa2036259e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8ded6b-5488-4f19-ac9e-3f5858c2ed8f",
                    "LayerId": "0edb2861-28de-40f6-921f-b46a4f1be229"
                },
                {
                    "id": "d3bcc611-baf0-44f4-848d-6ba2e3d23b24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8ded6b-5488-4f19-ac9e-3f5858c2ed8f",
                    "LayerId": "90eb7216-7e67-4422-9967-948aac2f22fa"
                }
            ]
        },
        {
            "id": "1c3d2f82-a1dd-4d43-87ee-a69301b0de6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "compositeImage": {
                "id": "648a4977-4556-4589-a8c3-02fc6f37d3bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c3d2f82-a1dd-4d43-87ee-a69301b0de6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ab03708-e50e-41ab-abf7-558eb2549715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3d2f82-a1dd-4d43-87ee-a69301b0de6f",
                    "LayerId": "1dff8ec8-2fd8-4436-b89c-1dcf81ee5792"
                },
                {
                    "id": "8bb28160-c847-4ad7-bb2e-fe1f7cba81b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3d2f82-a1dd-4d43-87ee-a69301b0de6f",
                    "LayerId": "f293b41d-2e27-4b98-9b03-301401a91d81"
                },
                {
                    "id": "b0ad03fa-c776-408a-b538-dd2b0ddf3307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3d2f82-a1dd-4d43-87ee-a69301b0de6f",
                    "LayerId": "d393dc7e-a3dc-4798-bf11-4f2db6bb2424"
                },
                {
                    "id": "00a88308-9b72-4bae-8fe0-71ba4f0e86d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3d2f82-a1dd-4d43-87ee-a69301b0de6f",
                    "LayerId": "495133b8-0231-48a0-9439-afaad41bd0c2"
                },
                {
                    "id": "d1b839f2-7f9f-4663-8309-bfd3516192e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3d2f82-a1dd-4d43-87ee-a69301b0de6f",
                    "LayerId": "ba948ef9-6950-4ab9-b6ed-6e167139bcd2"
                },
                {
                    "id": "a9a5e638-f3d4-4bc4-8554-99c5960b96dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3d2f82-a1dd-4d43-87ee-a69301b0de6f",
                    "LayerId": "0edb2861-28de-40f6-921f-b46a4f1be229"
                },
                {
                    "id": "507c0e2f-4aa1-4714-b5aa-f7ac40dba703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3d2f82-a1dd-4d43-87ee-a69301b0de6f",
                    "LayerId": "90eb7216-7e67-4422-9967-948aac2f22fa"
                }
            ]
        },
        {
            "id": "fcbd7096-760d-4bdc-b266-973f2d7f932e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "compositeImage": {
                "id": "e37f3d02-b624-4c5d-a938-b877c91577ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcbd7096-760d-4bdc-b266-973f2d7f932e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c74be9dc-5094-422c-8f91-fc68daa0612c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbd7096-760d-4bdc-b266-973f2d7f932e",
                    "LayerId": "1dff8ec8-2fd8-4436-b89c-1dcf81ee5792"
                },
                {
                    "id": "41241fa8-7f58-481e-9f08-474d108b2a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbd7096-760d-4bdc-b266-973f2d7f932e",
                    "LayerId": "f293b41d-2e27-4b98-9b03-301401a91d81"
                },
                {
                    "id": "0b2c7331-ee2d-4031-bcf7-2d1de9fe6844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbd7096-760d-4bdc-b266-973f2d7f932e",
                    "LayerId": "d393dc7e-a3dc-4798-bf11-4f2db6bb2424"
                },
                {
                    "id": "8d296729-e539-4bb6-b6cf-1e7a23dbbaba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbd7096-760d-4bdc-b266-973f2d7f932e",
                    "LayerId": "495133b8-0231-48a0-9439-afaad41bd0c2"
                },
                {
                    "id": "9490f2ec-5328-4053-ba7d-47913b114f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbd7096-760d-4bdc-b266-973f2d7f932e",
                    "LayerId": "ba948ef9-6950-4ab9-b6ed-6e167139bcd2"
                },
                {
                    "id": "ea7bcef4-aeba-44a3-9c78-e4ec2358b500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbd7096-760d-4bdc-b266-973f2d7f932e",
                    "LayerId": "0edb2861-28de-40f6-921f-b46a4f1be229"
                },
                {
                    "id": "b0248c85-c310-4da3-8290-c15c0f9143e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbd7096-760d-4bdc-b266-973f2d7f932e",
                    "LayerId": "90eb7216-7e67-4422-9967-948aac2f22fa"
                }
            ]
        },
        {
            "id": "d2464372-229b-4cd4-bbc0-1c3ede986430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "compositeImage": {
                "id": "8c9efbd8-79fa-4bd7-9915-4e295d294a6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2464372-229b-4cd4-bbc0-1c3ede986430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67880622-fce3-44c5-8c29-e1f34bba43a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2464372-229b-4cd4-bbc0-1c3ede986430",
                    "LayerId": "1dff8ec8-2fd8-4436-b89c-1dcf81ee5792"
                },
                {
                    "id": "ce9c41a3-fa4c-4f62-9e5d-7275f0cbe0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2464372-229b-4cd4-bbc0-1c3ede986430",
                    "LayerId": "f293b41d-2e27-4b98-9b03-301401a91d81"
                },
                {
                    "id": "490f15f8-92aa-46d7-976c-7ff73a97007a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2464372-229b-4cd4-bbc0-1c3ede986430",
                    "LayerId": "d393dc7e-a3dc-4798-bf11-4f2db6bb2424"
                },
                {
                    "id": "0d39924d-3e53-4078-9d02-b92d07cd0354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2464372-229b-4cd4-bbc0-1c3ede986430",
                    "LayerId": "495133b8-0231-48a0-9439-afaad41bd0c2"
                },
                {
                    "id": "80c9dc86-e11d-46d1-b85d-ca0d1917ecfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2464372-229b-4cd4-bbc0-1c3ede986430",
                    "LayerId": "ba948ef9-6950-4ab9-b6ed-6e167139bcd2"
                },
                {
                    "id": "ed59770b-948f-44ca-b989-01ecc0bcd729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2464372-229b-4cd4-bbc0-1c3ede986430",
                    "LayerId": "0edb2861-28de-40f6-921f-b46a4f1be229"
                },
                {
                    "id": "b6be6069-14c1-45b2-83b9-eb4f558b9d43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2464372-229b-4cd4-bbc0-1c3ede986430",
                    "LayerId": "90eb7216-7e67-4422-9967-948aac2f22fa"
                }
            ]
        },
        {
            "id": "a79d3309-eb3f-4401-b9da-0d72cc2743e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "compositeImage": {
                "id": "47f59670-1a4f-4b23-ae64-692686ad9072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a79d3309-eb3f-4401-b9da-0d72cc2743e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b071a9d-875c-4d88-b405-fa3dc6d91e78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79d3309-eb3f-4401-b9da-0d72cc2743e1",
                    "LayerId": "1dff8ec8-2fd8-4436-b89c-1dcf81ee5792"
                },
                {
                    "id": "87f3b547-08e8-48f8-98e5-6e4fb1397deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79d3309-eb3f-4401-b9da-0d72cc2743e1",
                    "LayerId": "f293b41d-2e27-4b98-9b03-301401a91d81"
                },
                {
                    "id": "862f3822-e164-4459-aafc-f0c4bb7651b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79d3309-eb3f-4401-b9da-0d72cc2743e1",
                    "LayerId": "d393dc7e-a3dc-4798-bf11-4f2db6bb2424"
                },
                {
                    "id": "27f2e559-b9f4-40ae-9b06-6f8465786ad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79d3309-eb3f-4401-b9da-0d72cc2743e1",
                    "LayerId": "495133b8-0231-48a0-9439-afaad41bd0c2"
                },
                {
                    "id": "da9ee2c7-06e5-4463-899d-fb4625373d0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79d3309-eb3f-4401-b9da-0d72cc2743e1",
                    "LayerId": "ba948ef9-6950-4ab9-b6ed-6e167139bcd2"
                },
                {
                    "id": "1b14a6c0-7bbf-4d09-b804-87d0963e6773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79d3309-eb3f-4401-b9da-0d72cc2743e1",
                    "LayerId": "0edb2861-28de-40f6-921f-b46a4f1be229"
                },
                {
                    "id": "df6f6a4d-735c-4c65-9f0d-dc56dfe612ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79d3309-eb3f-4401-b9da-0d72cc2743e1",
                    "LayerId": "90eb7216-7e67-4422-9967-948aac2f22fa"
                }
            ]
        },
        {
            "id": "890d1ded-cd55-42c4-940a-121f3cea9a56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "compositeImage": {
                "id": "fddb0ede-cab9-4210-9dde-cfc85469ba52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "890d1ded-cd55-42c4-940a-121f3cea9a56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d7bc908-5819-4582-89ea-e7d908d7139a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "890d1ded-cd55-42c4-940a-121f3cea9a56",
                    "LayerId": "1dff8ec8-2fd8-4436-b89c-1dcf81ee5792"
                },
                {
                    "id": "912d7481-162b-4921-92e9-4366f05f64c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "890d1ded-cd55-42c4-940a-121f3cea9a56",
                    "LayerId": "f293b41d-2e27-4b98-9b03-301401a91d81"
                },
                {
                    "id": "3a1c7436-47e7-4793-82d3-7c9fcf6e8957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "890d1ded-cd55-42c4-940a-121f3cea9a56",
                    "LayerId": "d393dc7e-a3dc-4798-bf11-4f2db6bb2424"
                },
                {
                    "id": "170ea136-cc62-4ccb-bd9b-52cc1118554c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "890d1ded-cd55-42c4-940a-121f3cea9a56",
                    "LayerId": "495133b8-0231-48a0-9439-afaad41bd0c2"
                },
                {
                    "id": "b1bf893e-8aba-4fd7-973e-b8fa6bd0bb5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "890d1ded-cd55-42c4-940a-121f3cea9a56",
                    "LayerId": "ba948ef9-6950-4ab9-b6ed-6e167139bcd2"
                },
                {
                    "id": "8e00bcf4-34a0-48d7-b164-f9c6052bf409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "890d1ded-cd55-42c4-940a-121f3cea9a56",
                    "LayerId": "0edb2861-28de-40f6-921f-b46a4f1be229"
                },
                {
                    "id": "41bd0fad-a74e-4fc3-a4f3-b69ce03d3187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "890d1ded-cd55-42c4-940a-121f3cea9a56",
                    "LayerId": "90eb7216-7e67-4422-9967-948aac2f22fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 99,
    "layers": [
        {
            "id": "90eb7216-7e67-4422-9967-948aac2f22fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 6",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1dff8ec8-2fd8-4436-b89c-1dcf81ee5792",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "f293b41d-2e27-4b98-9b03-301401a91d81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "d393dc7e-a3dc-4798-bf11-4f2db6bb2424",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "495133b8-0231-48a0-9439-afaad41bd0c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "ba948ef9-6950-4ab9-b6ed-6e167139bcd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 5",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "0edb2861-28de-40f6-921f-b46a4f1be229",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b310ed4d-a695-4b04-8cb9-f2d28b2bfae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 49
}