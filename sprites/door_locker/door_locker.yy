{
    "id": "37fb16e8-13aa-4fac-bfcb-6027107d9f09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "door_locker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff078969-165b-413e-95f7-2fe2294093ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37fb16e8-13aa-4fac-bfcb-6027107d9f09",
            "compositeImage": {
                "id": "5ea5fa71-76f0-4751-b8fc-2d6ace3bb86e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff078969-165b-413e-95f7-2fe2294093ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8d30d0-44fc-47fe-9793-7d37a28661df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff078969-165b-413e-95f7-2fe2294093ea",
                    "LayerId": "fc58533a-5494-40a9-8834-45e16a7d79be"
                }
            ]
        },
        {
            "id": "317aed78-968c-4534-86f4-9c164a109d10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37fb16e8-13aa-4fac-bfcb-6027107d9f09",
            "compositeImage": {
                "id": "e034f593-3135-4775-9b2c-42f189a1f0cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "317aed78-968c-4534-86f4-9c164a109d10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05be3e73-40bb-4a51-8e60-58c34a197d3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "317aed78-968c-4534-86f4-9c164a109d10",
                    "LayerId": "fc58533a-5494-40a9-8834-45e16a7d79be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fc58533a-5494-40a9-8834-45e16a7d79be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37fb16e8-13aa-4fac-bfcb-6027107d9f09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}