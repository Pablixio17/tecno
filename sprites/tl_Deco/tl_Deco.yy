{
    "id": "c26fdf41-7a23-42f1-b9ba-2a263fa39df4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tl_Deco",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 924,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cee84e8e-b400-4117-99f6-0176c2c4fa22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c26fdf41-7a23-42f1-b9ba-2a263fa39df4",
            "compositeImage": {
                "id": "b223cdb7-ae15-4bdb-92bf-0dc35d7a78b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee84e8e-b400-4117-99f6-0176c2c4fa22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf3ad02-92b2-41ab-accc-9d386c02e44f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee84e8e-b400-4117-99f6-0176c2c4fa22",
                    "LayerId": "4a53560d-7027-4049-8924-143b37fd6915"
                }
            ]
        }
    ],
    "gridX": 64,
    "gridY": 64,
    "height": 1024,
    "layers": [
        {
            "id": "4a53560d-7027-4049-8924-143b37fd6915",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c26fdf41-7a23-42f1-b9ba-2a263fa39df4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}