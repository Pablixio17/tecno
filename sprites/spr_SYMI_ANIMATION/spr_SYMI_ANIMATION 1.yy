{
    "id": "60ef3584-0544-4307-a79d-6819a81bff25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SYMI_ANIMATION",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 20,
    "bbox_right": 35,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6461abf-97c7-45ed-8ac6-ca583cb7de0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ef3584-0544-4307-a79d-6819a81bff25",
            "compositeImage": {
                "id": "1c623bfe-0ece-4e00-8724-2b82960ae4a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6461abf-97c7-45ed-8ac6-ca583cb7de0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78658feb-0ca7-4612-90dd-1206248f7f86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6461abf-97c7-45ed-8ac6-ca583cb7de0c",
                    "LayerId": "5c2d90fb-88ec-4190-b217-f3098d5febcd"
                }
            ]
        },
        {
            "id": "d09dc102-75ba-4d7f-816e-3bc04888ecb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ef3584-0544-4307-a79d-6819a81bff25",
            "compositeImage": {
                "id": "1ffec118-ad7b-4c44-bdf8-c51ac63eb2d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d09dc102-75ba-4d7f-816e-3bc04888ecb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caf05739-3284-4c1f-8e89-b68809a5a793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09dc102-75ba-4d7f-816e-3bc04888ecb0",
                    "LayerId": "5c2d90fb-88ec-4190-b217-f3098d5febcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 88,
    "layers": [
        {
            "id": "5c2d90fb-88ec-4190-b217-f3098d5febcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60ef3584-0544-4307-a79d-6819a81bff25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 0
}