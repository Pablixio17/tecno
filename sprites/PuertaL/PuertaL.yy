{
    "id": "9a981d0d-75c5-43f7-b540-1558551b8b1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PuertaL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 62,
    "bbox_right": 75,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19ac783b-5e2c-4036-9016-e8d036c5fe9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a981d0d-75c5-43f7-b540-1558551b8b1e",
            "compositeImage": {
                "id": "a50de510-da09-4d1a-be50-8ef003fbdda0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ac783b-5e2c-4036-9016-e8d036c5fe9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3bff2ac-1dac-4042-b3a5-09b9d88d208a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ac783b-5e2c-4036-9016-e8d036c5fe9b",
                    "LayerId": "945b01bf-4ee1-41e5-ae33-6ae5407bec6e"
                }
            ]
        },
        {
            "id": "2cc802b5-6a35-4eee-a3eb-a5b9b407a3a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a981d0d-75c5-43f7-b540-1558551b8b1e",
            "compositeImage": {
                "id": "05d0b4fb-3800-4f3d-938d-46ea8d005d3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cc802b5-6a35-4eee-a3eb-a5b9b407a3a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c8b7f3d-a9b1-4ed4-90d8-9c0b69439298",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cc802b5-6a35-4eee-a3eb-a5b9b407a3a2",
                    "LayerId": "945b01bf-4ee1-41e5-ae33-6ae5407bec6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "945b01bf-4ee1-41e5-ae33-6ae5407bec6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a981d0d-75c5-43f7-b540-1558551b8b1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}