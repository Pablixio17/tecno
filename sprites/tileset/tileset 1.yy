{
    "id": "d08f865e-f01e-4d1f-bffb-0efae42618a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 256,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9df6f807-d977-4318-8c71-670235861b2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d08f865e-f01e-4d1f-bffb-0efae42618a5",
            "compositeImage": {
                "id": "48a96ebc-9894-4f98-ac1c-54e5d601796e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9df6f807-d977-4318-8c71-670235861b2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a19a565-7adf-4c92-a676-628d4fa97fb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9df6f807-d977-4318-8c71-670235861b2a",
                    "LayerId": "d6a32b59-3984-438d-b502-08e93981fcdc"
                }
            ]
        }
    ],
    "gridX": 64,
    "gridY": 64,
    "height": 258,
    "layers": [
        {
            "id": "d6a32b59-3984-438d-b502-08e93981fcdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d08f865e-f01e-4d1f-bffb-0efae42618a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": -50,
    "yorig": 131
}