{
    "id": "7df77f07-8021-46ee-8ba7-5fb71e565de0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerfalling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 3,
    "bbox_right": 97,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6d6ff74-652a-46d1-b7c8-3e360559c22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7df77f07-8021-46ee-8ba7-5fb71e565de0",
            "compositeImage": {
                "id": "1ca57a7f-8ae5-4163-b1b6-3ece49f9ee59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d6ff74-652a-46d1-b7c8-3e360559c22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74146c7c-2561-42c0-8b44-c010e68bfa6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d6ff74-652a-46d1-b7c8-3e360559c22f",
                    "LayerId": "247175cf-d9e8-4700-88fe-5d3b394b4e6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "247175cf-d9e8-4700-88fe-5d3b394b4e6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7df77f07-8021-46ee-8ba7-5fb71e565de0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 31
}