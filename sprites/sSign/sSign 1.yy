{
    "id": "e35eb2a1-762a-483c-908a-3ecf3b4c7945",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 2,
    "bbox_right": 57,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3d4e70c-8770-41b4-a49c-d2320a23d686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e35eb2a1-762a-483c-908a-3ecf3b4c7945",
            "compositeImage": {
                "id": "79987bb1-199d-42de-be21-cba20b44583d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3d4e70c-8770-41b4-a49c-d2320a23d686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77d78019-4175-46f3-b665-78fb4eee9071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3d4e70c-8770-41b4-a49c-d2320a23d686",
                    "LayerId": "94cabd70-eb39-4dc5-b7ca-fe3855861cc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "94cabd70-eb39-4dc5-b7ca-fe3855861cc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e35eb2a1-762a-483c-908a-3ecf3b4c7945",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}