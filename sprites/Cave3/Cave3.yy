{
    "id": "ef599512-b018-402a-a38f-1a233881bce0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Cave3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19340b7f-232c-4456-a7d2-a03f72106c80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef599512-b018-402a-a38f-1a233881bce0",
            "compositeImage": {
                "id": "39bbf29d-3b28-40ef-a135-9319a4150cc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19340b7f-232c-4456-a7d2-a03f72106c80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1719ca50-1376-4006-8fba-18e5070abc7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19340b7f-232c-4456-a7d2-a03f72106c80",
                    "LayerId": "5892eb0e-f099-4e98-aeed-b9c7af0f0bcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "5892eb0e-f099-4e98-aeed-b9c7af0f0bcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef599512-b018-402a-a38f-1a233881bce0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}