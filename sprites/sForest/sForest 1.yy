{
    "id": "fd907a04-2eb1-4388-b0d9-bb4754802938",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sForest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64367711-0d2c-4049-8fa7-12281cf56b5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd907a04-2eb1-4388-b0d9-bb4754802938",
            "compositeImage": {
                "id": "ff762027-9fd1-4a2b-874a-bc4b353eb543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64367711-0d2c-4049-8fa7-12281cf56b5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f281ca2a-5122-4cf6-9e49-2c751dceacab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64367711-0d2c-4049-8fa7-12281cf56b5a",
                    "LayerId": "d1f5b64e-bc8e-48d1-a1c0-ca19dc415b59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "d1f5b64e-bc8e-48d1-a1c0-ca19dc415b59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd907a04-2eb1-4388-b0d9-bb4754802938",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}