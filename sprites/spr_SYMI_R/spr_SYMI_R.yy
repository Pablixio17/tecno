{
    "id": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SYMI_R",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 20,
    "bbox_right": 35,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "799b5b08-abc6-4e5e-924a-e41443233c09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "18aa378e-0903-419a-8576-933a166eea7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "799b5b08-abc6-4e5e-924a-e41443233c09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1278c05a-e7ba-403b-a192-462d82ea2a02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "799b5b08-abc6-4e5e-924a-e41443233c09",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "46fc3cd4-77f1-4114-a3e1-2ea5baf3d5b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "3a4947fe-1d7d-4269-bab5-d4323dc4a201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46fc3cd4-77f1-4114-a3e1-2ea5baf3d5b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f74911-50c3-4a0f-9c1a-39be4f05a126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46fc3cd4-77f1-4114-a3e1-2ea5baf3d5b9",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "7ee462ef-e253-41ff-beb2-d8cb87c2bbef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "76357ffe-9888-4f21-9250-67ed61c3ce8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ee462ef-e253-41ff-beb2-d8cb87c2bbef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "276c2143-09af-4e4e-a2b5-c72acdd87cc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ee462ef-e253-41ff-beb2-d8cb87c2bbef",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "8b2af6b2-b977-4e76-bb62-9bc14ab007c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "9e134ba8-755d-42d7-b752-4b85d2c302d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b2af6b2-b977-4e76-bb62-9bc14ab007c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ff800f3-e0a6-42d9-885a-53cc66f38b0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b2af6b2-b977-4e76-bb62-9bc14ab007c6",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "00786bbc-8ea1-4a59-827c-c71dcdb52f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "f6a6dfd6-877e-4606-b6d1-3a8d38b6ac14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00786bbc-8ea1-4a59-827c-c71dcdb52f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "508efdf9-a649-4a44-b226-44f9703c91e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00786bbc-8ea1-4a59-827c-c71dcdb52f50",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "e6db5876-fd30-4506-8161-7201631d6aab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "534d3504-003d-4266-a502-d0a5712792d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6db5876-fd30-4506-8161-7201631d6aab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9ee278f-7ddb-4acf-9628-52e5b9fbd4f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6db5876-fd30-4506-8161-7201631d6aab",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "fe76b102-66be-4a0b-b8e5-59b1b00ca527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "106b627c-8ce8-4efd-8358-45300f361392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe76b102-66be-4a0b-b8e5-59b1b00ca527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc9a1037-1fda-4a38-80b8-34140df57e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe76b102-66be-4a0b-b8e5-59b1b00ca527",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "16986a13-96eb-4acb-9574-cfbe581cb9dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "2cf3f89e-f2f1-4698-a762-a6be27339f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16986a13-96eb-4acb-9574-cfbe581cb9dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96531e17-85f7-4746-b09d-6c8a05c53838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16986a13-96eb-4acb-9574-cfbe581cb9dd",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "db26c6c0-e78d-4a3d-b4da-904744fef2f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "79a02e71-012b-47dd-8a8f-4b3d90a15d01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db26c6c0-e78d-4a3d-b4da-904744fef2f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee3b4a6f-67dc-4a3b-8aa9-0c955e4dc742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db26c6c0-e78d-4a3d-b4da-904744fef2f2",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "b0dbd49d-5080-4f27-bb5a-f838fa50f750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "a708a2e8-3b29-48ab-b5a6-7ed32a4ad5a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0dbd49d-5080-4f27-bb5a-f838fa50f750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b952a8-a37c-458b-9928-facfd23ad825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0dbd49d-5080-4f27-bb5a-f838fa50f750",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "4dcce65c-4308-45da-863d-c9555d9f6472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "aa36ea01-2e05-4fd4-847a-03b1a4cc1ba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dcce65c-4308-45da-863d-c9555d9f6472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76fc3187-ecd9-497a-9b5c-fbd9909db64a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dcce65c-4308-45da-863d-c9555d9f6472",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "cc3ea53a-6a67-4c50-af22-9a1f4e6c1975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "0d17fa1e-ab01-4c8f-9a7d-eb850c1a2d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc3ea53a-6a67-4c50-af22-9a1f4e6c1975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "896de62c-f1c7-4c05-8d7f-107224410e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc3ea53a-6a67-4c50-af22-9a1f4e6c1975",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "b0a316a2-0a30-4f93-97fa-24972fe612d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "4c6b4509-c485-4f49-86b8-8d4afd7c5f2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0a316a2-0a30-4f93-97fa-24972fe612d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bbed449-a2b2-452d-bff8-712952dc4d51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0a316a2-0a30-4f93-97fa-24972fe612d2",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "91c37c2e-cd54-4c30-9fbf-121cfdd14a4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "1883b7df-0d1a-4b60-ab59-3ce4ce1a2456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c37c2e-cd54-4c30-9fbf-121cfdd14a4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3589574-d07e-49bf-b6c7-889634b368df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c37c2e-cd54-4c30-9fbf-121cfdd14a4d",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "541627d8-a4d0-4772-adf1-0c6c5d270751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "bdba170f-a68f-4654-a3cc-b5af46afb3e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "541627d8-a4d0-4772-adf1-0c6c5d270751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ad195a-909c-458b-ae1d-0153e65b7b8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "541627d8-a4d0-4772-adf1-0c6c5d270751",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        },
        {
            "id": "c0e85223-5058-430c-97ba-44cef5e53bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "compositeImage": {
                "id": "9bb94813-e68f-45a6-9c8e-f934b16afbee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0e85223-5058-430c-97ba-44cef5e53bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43e382f5-35c3-4d49-a6b2-c90b7278086e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0e85223-5058-430c-97ba-44cef5e53bba",
                    "LayerId": "a815b23d-f3fb-4316-b2d8-bd4666c0310c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 88,
    "layers": [
        {
            "id": "a815b23d-f3fb-4316-b2d8-bd4666c0310c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31172ad8-ca84-42e4-8973-ebc393d89fb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 0
}