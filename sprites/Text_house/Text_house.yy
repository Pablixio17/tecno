{
    "id": "deb16986-446a-4f8e-9038-e5935803edd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Text_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3362282-0ea1-49d7-84b6-c987acdbc374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "deb16986-446a-4f8e-9038-e5935803edd3",
            "compositeImage": {
                "id": "bde5a81c-f31b-4b8c-8fe1-81ef7c4395a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3362282-0ea1-49d7-84b6-c987acdbc374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ef8bb57-236c-41a9-9bf1-3b8ac01c924f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3362282-0ea1-49d7-84b6-c987acdbc374",
                    "LayerId": "3971cc0a-288b-496f-a7fe-dea1f0c335e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "3971cc0a-288b-496f-a7fe-dea1f0c335e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "deb16986-446a-4f8e-9038-e5935803edd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}