{
    "id": "16294412-376f-49ad-9634-2cb8c3391c5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 0,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f729c2a8-993d-44dd-aa34-0514adf4772f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "01b34231-77ce-4232-8a67-10161b9c3fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f729c2a8-993d-44dd-aa34-0514adf4772f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba5cd142-1b66-418b-bdab-41347ecde6f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f729c2a8-993d-44dd-aa34-0514adf4772f",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "0bbe5462-9968-4346-8da2-18469681bbd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "d16fa93d-c803-4416-b18b-8d6307e2e8f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbe5462-9968-4346-8da2-18469681bbd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "656ccad8-12c2-4586-be04-2bee13573ac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbe5462-9968-4346-8da2-18469681bbd1",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "c7df8801-d89e-45fb-a32b-90685641329b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "2a12b558-28c2-4c0a-aaff-f2cd763d8c06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7df8801-d89e-45fb-a32b-90685641329b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59b525d8-602e-4beb-9260-aeab8e814c0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7df8801-d89e-45fb-a32b-90685641329b",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "6e7c01fc-8fab-47d4-92de-2b6a20bec84e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "e1ff2961-d153-48c7-b343-cd956a61bf94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e7c01fc-8fab-47d4-92de-2b6a20bec84e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caa09c2a-086d-442e-9d5e-b182a6177e3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e7c01fc-8fab-47d4-92de-2b6a20bec84e",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "df6d2780-d4ef-412d-b295-ee3e44ad43fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "3372de67-b90e-4919-9aaa-c8e725e2f5c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df6d2780-d4ef-412d-b295-ee3e44ad43fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6359297f-569b-45ef-a028-b5e39b424a63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df6d2780-d4ef-412d-b295-ee3e44ad43fd",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "ae85ba3a-4564-4f39-aaba-f5e5b48908a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "0e66ef50-1e38-48c0-85f0-6faa146dbe20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae85ba3a-4564-4f39-aaba-f5e5b48908a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9b61c18-a023-42a0-b47b-523a64f76d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae85ba3a-4564-4f39-aaba-f5e5b48908a7",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "ac3aa511-b3c8-42b8-8a0f-884605bd77eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "823ca5d8-605f-4f89-9e99-6ed3523b7204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac3aa511-b3c8-42b8-8a0f-884605bd77eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a756c69f-34e8-4887-8a40-e3bb694d9030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac3aa511-b3c8-42b8-8a0f-884605bd77eb",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "88088be0-5d44-495b-befa-12a2476f19aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "2b44bb30-1d3e-40fe-b355-2794c80b8b04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88088be0-5d44-495b-befa-12a2476f19aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b4f781-553f-415d-b613-33fe69601a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88088be0-5d44-495b-befa-12a2476f19aa",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "e73051d8-e6d5-49b6-bffa-2cb44aece3c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "07bae869-660a-4441-b668-9a14894e9098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e73051d8-e6d5-49b6-bffa-2cb44aece3c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d103a201-e54e-46bd-bddc-f1d124b6b49f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e73051d8-e6d5-49b6-bffa-2cb44aece3c3",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "32356ca8-bd90-4048-bf99-a408d6bd74ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "84e00513-52ec-4d52-ad45-4f53d6f8bc13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32356ca8-bd90-4048-bf99-a408d6bd74ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "414c8939-3735-4652-91be-32e31687c814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32356ca8-bd90-4048-bf99-a408d6bd74ba",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "2bc72b17-c789-4838-abe8-d5f22b88100b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "2bba77c4-63e5-4620-a4e3-0520bb32aaa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc72b17-c789-4838-abe8-d5f22b88100b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d34abd6-71da-4817-9d33-c861b7a5df8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc72b17-c789-4838-abe8-d5f22b88100b",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "3edc5802-309b-41c8-ac45-4441066f21d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "7e208e63-a3a6-4e78-80da-5cf5a576d1db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3edc5802-309b-41c8-ac45-4441066f21d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ceea81e-c2b3-4167-a8b3-f0fa3a1ffbb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3edc5802-309b-41c8-ac45-4441066f21d6",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "cb3d4995-4831-4e16-bd53-a849d1f9e491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "b558a41a-9824-400d-bb37-22a9fb78afdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb3d4995-4831-4e16-bd53-a849d1f9e491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7407895-8594-45f1-a294-073433a41fb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb3d4995-4831-4e16-bd53-a849d1f9e491",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "17a492ea-8301-45b4-b199-5fb5d1c2f54c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "bf2884b7-1ceb-4796-b94a-b687454f37fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17a492ea-8301-45b4-b199-5fb5d1c2f54c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86550f0b-4e65-40de-aa65-9d094ad48226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17a492ea-8301-45b4-b199-5fb5d1c2f54c",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "41b4abfa-efb8-4df9-9676-86f8eb2cf4bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "78b24d85-cb13-48b2-b6d7-98d71b89e9ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41b4abfa-efb8-4df9-9676-86f8eb2cf4bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbc4aa20-4f82-448f-8427-13b9e52e14ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41b4abfa-efb8-4df9-9676-86f8eb2cf4bc",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        },
        {
            "id": "34d0a539-6afd-4d77-b732-085bca6a46fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "compositeImage": {
                "id": "f6f1d61a-6fbb-48af-b949-3a54d6f653d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34d0a539-6afd-4d77-b732-085bca6a46fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bfeb35f-5a0c-4978-8652-96e8c0bfd42d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34d0a539-6afd-4d77-b732-085bca6a46fa",
                    "LayerId": "3424d561-af75-4207-a507-463a4462a1dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "3424d561-af75-4207-a507-463a4462a1dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16294412-376f-49ad-9634-2cb8c3391c5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 45
}