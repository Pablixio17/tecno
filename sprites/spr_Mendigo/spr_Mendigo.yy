{
    "id": "09766134-fb48-45cb-a9ba-0a10adbb0c91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Mendigo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": -8,
    "bbox_right": 69,
    "bbox_top": -4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0269ccc7-f2db-4fed-902e-d6db31d5a881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09766134-fb48-45cb-a9ba-0a10adbb0c91",
            "compositeImage": {
                "id": "04e8bacf-cd8e-4684-9f31-d8b7559a7a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0269ccc7-f2db-4fed-902e-d6db31d5a881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "247c3e10-3de9-42ac-a241-d4ba6aa18aca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0269ccc7-f2db-4fed-902e-d6db31d5a881",
                    "LayerId": "b917e367-1b46-4718-958b-1ace409d0503"
                }
            ]
        },
        {
            "id": "12b50b9e-e004-44b3-8a19-b0a88aec0032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09766134-fb48-45cb-a9ba-0a10adbb0c91",
            "compositeImage": {
                "id": "36c0e00f-178d-4107-958e-54cedc4baec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12b50b9e-e004-44b3-8a19-b0a88aec0032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "848bdf79-6804-481b-81cf-fbe71ec0f018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12b50b9e-e004-44b3-8a19-b0a88aec0032",
                    "LayerId": "b917e367-1b46-4718-958b-1ace409d0503"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "b917e367-1b46-4718-958b-1ace409d0503",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09766134-fb48-45cb-a9ba-0a10adbb0c91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 45
}