{
    "id": "1a349654-056b-4077-8b43-04380c1bd6ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "door_unlocker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 46,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eafb9758-8cab-469c-98bf-deb6264fdc05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a349654-056b-4077-8b43-04380c1bd6ae",
            "compositeImage": {
                "id": "2e58a2cb-71b3-4279-9390-0b4edf602b34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eafb9758-8cab-469c-98bf-deb6264fdc05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27600322-e8b1-4909-b2a1-d5db93cdf549",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eafb9758-8cab-469c-98bf-deb6264fdc05",
                    "LayerId": "d10ba30d-e1ec-4149-8a1a-299511b14237"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d10ba30d-e1ec-4149-8a1a-299511b14237",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a349654-056b-4077-8b43-04380c1bd6ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}