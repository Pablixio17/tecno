{
    "id": "b438a86a-1389-4e16-89a4-e8f9493a66e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3375eb8f-91a8-464e-9463-969691eea878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b438a86a-1389-4e16-89a4-e8f9493a66e9",
            "compositeImage": {
                "id": "e43e3413-9d0e-460d-9c8e-0512b0ad4898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3375eb8f-91a8-464e-9463-969691eea878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b0c6f4e-d7b2-4e78-891c-4ed8e7bc8807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3375eb8f-91a8-464e-9463-969691eea878",
                    "LayerId": "864552d3-86d9-4411-840d-053d25c9070e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "864552d3-86d9-4411-840d-053d25c9070e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b438a86a-1389-4e16-89a4-e8f9493a66e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}