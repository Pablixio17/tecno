{
    "id": "b50b6966-6f87-48c5-87c2-bff64e166f95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sForest2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 795,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 392,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0098911f-002e-4bdd-9130-13e0cbf538ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b50b6966-6f87-48c5-87c2-bff64e166f95",
            "compositeImage": {
                "id": "78f2f1a0-0445-4433-862b-ab7cba65de64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0098911f-002e-4bdd-9130-13e0cbf538ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9005ebfe-2ba9-4581-a02f-a277c8dc57ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0098911f-002e-4bdd-9130-13e0cbf538ec",
                    "LayerId": "ea8799f3-484e-44d4-9c53-3e4cab709854"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 796,
    "layers": [
        {
            "id": "ea8799f3-484e-44d4-9c53-3e4cab709854",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b50b6966-6f87-48c5-87c2-bff64e166f95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 221,
    "yorig": 110
}