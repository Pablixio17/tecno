{
    "id": "43efe181-e149-41f2-a102-505177b6766a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mesilla",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecf89de2-d786-4019-b05f-5faca6c5c6be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43efe181-e149-41f2-a102-505177b6766a",
            "compositeImage": {
                "id": "5011bb85-93f3-4aea-81f9-9c660aff8a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf89de2-d786-4019-b05f-5faca6c5c6be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "937daf06-9645-4730-a437-121906d5828d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf89de2-d786-4019-b05f-5faca6c5c6be",
                    "LayerId": "18bcb213-ddf2-4735-b8c9-926472574670"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "18bcb213-ddf2-4735-b8c9-926472574670",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43efe181-e149-41f2-a102-505177b6766a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}