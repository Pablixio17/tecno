{
    "id": "1eb1b910-2a1a-45d7-8c7f-851d9a2005ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc6973f1-7004-4b40-85e9-60eb102a5195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1eb1b910-2a1a-45d7-8c7f-851d9a2005ea",
            "compositeImage": {
                "id": "6df6766f-0108-45b2-968f-994fdaee5ced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc6973f1-7004-4b40-85e9-60eb102a5195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "782e174e-c8f8-4815-9155-1988ffee2bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc6973f1-7004-4b40-85e9-60eb102a5195",
                    "LayerId": "87295936-87d8-48bd-826b-2e57cc75c74a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "87295936-87d8-48bd-826b-2e57cc75c74a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1eb1b910-2a1a-45d7-8c7f-851d9a2005ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}