{
    "id": "20b35234-3e89-4162-9224-12594267853d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59afbde1-684c-4920-828e-c18d0e957eda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "7e325ad1-1e94-4f6e-bf46-990b22976dc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59afbde1-684c-4920-828e-c18d0e957eda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ee702d0-42e2-43e8-afb4-052db986c14e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59afbde1-684c-4920-828e-c18d0e957eda",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "5d4df879-fc19-4d81-b9a5-adcad144ba8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "82a7a46d-bb6a-4f0a-982b-f792235d45f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4df879-fc19-4d81-b9a5-adcad144ba8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b2242d-1a5a-40d4-87b9-6aecba919936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4df879-fc19-4d81-b9a5-adcad144ba8d",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "dd876d57-584d-4669-882e-f4606c3e0cd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "be013a86-1549-4737-85e4-0d08e4dca54f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd876d57-584d-4669-882e-f4606c3e0cd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43302b27-c873-4231-baf6-03d03f3c1495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd876d57-584d-4669-882e-f4606c3e0cd8",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "26c7c1cb-f282-40ac-b2b2-3781b791802e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "48b34841-c053-4b3c-a200-c701abb34e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c7c1cb-f282-40ac-b2b2-3781b791802e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dd46f6a-5e2e-4aef-8692-c36eecf6d865",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c7c1cb-f282-40ac-b2b2-3781b791802e",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "12076917-5acb-4fa5-8eed-1e2c90560c62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "da6da3d3-28e1-4778-bb6b-2f82d141d6fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12076917-5acb-4fa5-8eed-1e2c90560c62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc919349-b0e8-4f18-b773-031eefee507d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12076917-5acb-4fa5-8eed-1e2c90560c62",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "bed193e7-1c6c-45e7-982a-f898cd6482cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "23f6a557-d1c1-4b65-a838-bea45ba97909",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed193e7-1c6c-45e7-982a-f898cd6482cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d28bdfc-3a2e-43c6-bb99-061c8b5383f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed193e7-1c6c-45e7-982a-f898cd6482cc",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "3f6689d7-3086-4177-a5e2-3c5ba87d49ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "2b4e0cc5-67fd-42c2-b646-31bccadc6176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f6689d7-3086-4177-a5e2-3c5ba87d49ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b85f813-b50f-4957-8bc4-b8d9908a5e48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f6689d7-3086-4177-a5e2-3c5ba87d49ff",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "6b2fed18-5392-4c7f-b601-b2a1d618a148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "adbb335f-74fe-4d5a-940c-2bf7cf474f71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b2fed18-5392-4c7f-b601-b2a1d618a148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bfef15b-70ff-49aa-9eb7-038a62a03b55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b2fed18-5392-4c7f-b601-b2a1d618a148",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "02537fc1-ffad-4951-bcaa-3a744b8264b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "ffb4053a-3dfc-416b-bfde-99fd365feff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02537fc1-ffad-4951-bcaa-3a744b8264b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c60a2fa-38da-4197-9490-7dae5f03e84e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02537fc1-ffad-4951-bcaa-3a744b8264b3",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "9f258cd9-b3c9-417d-8734-8e4952b522e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "d69f4db0-9dc7-438f-b757-962e97759c48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f258cd9-b3c9-417d-8734-8e4952b522e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afd3b403-756a-4950-8ac4-28f5b232054a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f258cd9-b3c9-417d-8734-8e4952b522e5",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        },
        {
            "id": "10d03293-f0b4-4dfa-aa41-8afd4b233760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "compositeImage": {
                "id": "fbcf8986-6247-4d79-8e26-a444f0ceb2f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d03293-f0b4-4dfa-aa41-8afd4b233760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b5f2e7-b277-4105-9964-b47d7940b745",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d03293-f0b4-4dfa-aa41-8afd4b233760",
                    "LayerId": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 8,
    "layers": [
        {
            "id": "1d41b4da-ce2d-41f0-8418-9f6207edd0d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20b35234-3e89-4162-9224-12594267853d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}