{
    "id": "52187377-3f3b-45a6-ac19-367850e813ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Symimuerto",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 3,
    "bbox_right": 98,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cbc6c3b-7e89-4746-a589-5170aa561567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52187377-3f3b-45a6-ac19-367850e813ba",
            "compositeImage": {
                "id": "c7bbbbdb-c4e6-4cd0-b81b-3272ecb94371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cbc6c3b-7e89-4746-a589-5170aa561567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9289d95-7b4f-4509-b35f-2a2681f48b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cbc6c3b-7e89-4746-a589-5170aa561567",
                    "LayerId": "9495e59c-d6bb-4ffd-a9ab-e4b6cdc2e03f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "9495e59c-d6bb-4ffd-a9ab-e4b6cdc2e03f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52187377-3f3b-45a6-ac19-367850e813ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 0,
    "yorig": 0
}