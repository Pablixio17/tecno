{
    "id": "deeb2dcc-9f3e-470d-9bb8-a6408f334733",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Text_Village",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7133207-bdf4-4459-9e9f-4645bc43cf9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "deeb2dcc-9f3e-470d-9bb8-a6408f334733",
            "compositeImage": {
                "id": "de2536b3-fd10-48ce-badd-edfa9a122741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7133207-bdf4-4459-9e9f-4645bc43cf9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adf3fda1-72b5-482b-a4c6-729b933ba966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7133207-bdf4-4459-9e9f-4645bc43cf9a",
                    "LayerId": "8d4e870e-1e46-497b-a7d4-522d628534da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "8d4e870e-1e46-497b-a7d4-522d628534da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "deeb2dcc-9f3e-470d-9bb8-a6408f334733",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}