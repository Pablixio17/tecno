{
    "id": "20677ca6-f404-4693-9fdd-ad7880adff5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Playerfall2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 18,
    "bbox_right": 118,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f728eb4c-9470-4f30-b4cb-06591ccfe169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
            "compositeImage": {
                "id": "f197cbee-dae5-42e2-95e6-4e2baa776dce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f728eb4c-9470-4f30-b4cb-06591ccfe169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ec81e4-165f-4c7f-90b9-c366a6533773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f728eb4c-9470-4f30-b4cb-06591ccfe169",
                    "LayerId": "34209e04-6c18-4264-8f7a-be107aad9406"
                }
            ]
        },
        {
            "id": "22f333e8-719d-454e-abed-8c0d0ec5e362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
            "compositeImage": {
                "id": "451635d9-3411-4720-bbf1-74feeb93f0e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f333e8-719d-454e-abed-8c0d0ec5e362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450dabd3-e90d-4915-ad1d-adf27220b710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f333e8-719d-454e-abed-8c0d0ec5e362",
                    "LayerId": "34209e04-6c18-4264-8f7a-be107aad9406"
                }
            ]
        },
        {
            "id": "2c66e4ac-e5a2-4d1a-80c9-d8426ea9f894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
            "compositeImage": {
                "id": "3202b59b-c2d4-4429-9bc7-7f88cd146848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c66e4ac-e5a2-4d1a-80c9-d8426ea9f894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4c2711-0179-4b97-95a9-4725e4394bda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c66e4ac-e5a2-4d1a-80c9-d8426ea9f894",
                    "LayerId": "34209e04-6c18-4264-8f7a-be107aad9406"
                }
            ]
        },
        {
            "id": "6d27f06d-4185-4cf7-83e9-a371abadf34a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
            "compositeImage": {
                "id": "04ce234f-e9dd-4701-a78e-7fa889f2214e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d27f06d-4185-4cf7-83e9-a371abadf34a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d696a560-72d1-4096-88a3-d39496571609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d27f06d-4185-4cf7-83e9-a371abadf34a",
                    "LayerId": "34209e04-6c18-4264-8f7a-be107aad9406"
                }
            ]
        },
        {
            "id": "fc737bb0-b576-494d-8b5d-df41634ffc20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
            "compositeImage": {
                "id": "042e5068-39fe-412d-8af8-245c23e5b909",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc737bb0-b576-494d-8b5d-df41634ffc20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f91c46-a749-4046-a417-251a78aa47ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc737bb0-b576-494d-8b5d-df41634ffc20",
                    "LayerId": "34209e04-6c18-4264-8f7a-be107aad9406"
                }
            ]
        },
        {
            "id": "f6d8d7e6-ab4a-4e73-a8cc-2a431508a1f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
            "compositeImage": {
                "id": "f7c52aef-4650-4ad7-9bc3-2ad5d0e0f280",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d8d7e6-ab4a-4e73-a8cc-2a431508a1f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "030985af-a772-4c0c-8a72-357f5280fefe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d8d7e6-ab4a-4e73-a8cc-2a431508a1f5",
                    "LayerId": "34209e04-6c18-4264-8f7a-be107aad9406"
                }
            ]
        },
        {
            "id": "5d4135f6-b995-42b5-a792-33a7b9e3a2d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
            "compositeImage": {
                "id": "c2bcbb01-2433-46e2-80fc-5a9ef13cacea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4135f6-b995-42b5-a792-33a7b9e3a2d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aac90c8a-49ec-42c2-b2bf-2920a0ba339c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4135f6-b995-42b5-a792-33a7b9e3a2d9",
                    "LayerId": "34209e04-6c18-4264-8f7a-be107aad9406"
                }
            ]
        },
        {
            "id": "20617fb5-5ad8-4309-8d2c-b2d05a1f6a44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
            "compositeImage": {
                "id": "9de81ebd-058e-4f83-81d9-fab94c77f116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20617fb5-5ad8-4309-8d2c-b2d05a1f6a44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f15a3f58-ec20-4448-8026-e1dec79943df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20617fb5-5ad8-4309-8d2c-b2d05a1f6a44",
                    "LayerId": "34209e04-6c18-4264-8f7a-be107aad9406"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "34209e04-6c18-4264-8f7a-be107aad9406",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 122,
    "xorig": 61,
    "yorig": 64
}