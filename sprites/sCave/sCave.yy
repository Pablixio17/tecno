{
    "id": "2b2ea554-db73-4200-baac-c77ee3b95fd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09f24ece-7b92-4ecc-8872-4feda9c35d1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b2ea554-db73-4200-baac-c77ee3b95fd1",
            "compositeImage": {
                "id": "e02c9515-5750-4def-884c-6da59ab3d691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09f24ece-7b92-4ecc-8872-4feda9c35d1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad9c44c1-d014-45d8-a172-d7051c15fd45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09f24ece-7b92-4ecc-8872-4feda9c35d1b",
                    "LayerId": "a794fbf4-531e-47b4-accc-e229abf5e769"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "a794fbf4-531e-47b4-accc-e229abf5e769",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b2ea554-db73-4200-baac-c77ee3b95fd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}