{
    "id": "1788e6ae-6dbf-41fe-a293-482e0996448f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Playerfall1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 3,
    "bbox_right": 121,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad16464b-acfb-4606-a591-4476fbf4d5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1788e6ae-6dbf-41fe-a293-482e0996448f",
            "compositeImage": {
                "id": "7d7fc080-6b93-4b33-9a23-049c905d6ed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad16464b-acfb-4606-a591-4476fbf4d5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01b0cf0b-4bd5-4bf7-8278-bce3061d4162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad16464b-acfb-4606-a591-4476fbf4d5b0",
                    "LayerId": "8186bcf6-a131-43de-b034-1da4cc079ee1"
                }
            ]
        },
        {
            "id": "ee214230-0e0d-4277-b6e8-bee74ba54597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1788e6ae-6dbf-41fe-a293-482e0996448f",
            "compositeImage": {
                "id": "2aaebcd5-6188-4996-ac97-b79ddb65f9cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee214230-0e0d-4277-b6e8-bee74ba54597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e79f38-f29e-4b4a-ba52-73ee7821ce28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee214230-0e0d-4277-b6e8-bee74ba54597",
                    "LayerId": "8186bcf6-a131-43de-b034-1da4cc079ee1"
                }
            ]
        },
        {
            "id": "b6807379-b63d-4591-9feb-fce5be73836a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1788e6ae-6dbf-41fe-a293-482e0996448f",
            "compositeImage": {
                "id": "7850088c-ac37-4c1c-bfe2-83ca85aee597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6807379-b63d-4591-9feb-fce5be73836a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfda1139-49dc-49e4-bbb2-42b045e3ed75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6807379-b63d-4591-9feb-fce5be73836a",
                    "LayerId": "8186bcf6-a131-43de-b034-1da4cc079ee1"
                }
            ]
        },
        {
            "id": "b21113db-0773-453e-a71e-1c3fe01ff98b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1788e6ae-6dbf-41fe-a293-482e0996448f",
            "compositeImage": {
                "id": "a703fadb-ed6a-4772-b8b7-83a178e79c63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b21113db-0773-453e-a71e-1c3fe01ff98b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93c90539-b99d-47e0-9fe4-ee99dfde59e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b21113db-0773-453e-a71e-1c3fe01ff98b",
                    "LayerId": "8186bcf6-a131-43de-b034-1da4cc079ee1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8186bcf6-a131-43de-b034-1da4cc079ee1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1788e6ae-6dbf-41fe-a293-482e0996448f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 122,
    "xorig": 61,
    "yorig": 64
}