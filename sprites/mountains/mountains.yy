{
    "id": "3f0ed7f6-8b91-4be7-b951-c13561d94988",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mountains",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed3b47c0-9b9e-4573-b18f-af96ac0e3a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f0ed7f6-8b91-4be7-b951-c13561d94988",
            "compositeImage": {
                "id": "c834f5e6-f649-413b-9b11-105971bc4750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed3b47c0-9b9e-4573-b18f-af96ac0e3a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a508ed-72bc-46a4-80af-65b30802fa02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed3b47c0-9b9e-4573-b18f-af96ac0e3a9b",
                    "LayerId": "7d176ae7-5f9f-40fd-81cb-987475708e70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7d176ae7-5f9f-40fd-81cb-987475708e70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f0ed7f6-8b91-4be7-b951-c13561d94988",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}