{
    "id": "8f2ccbeb-6d71-4ae9-ac3f-4a253783d77f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTransition1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "525dc600-8c86-4a8b-bc98-36f1c3d7fdb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f2ccbeb-6d71-4ae9-ac3f-4a253783d77f",
            "compositeImage": {
                "id": "278a69cb-fafc-473f-9304-ec08a0dc7d6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "525dc600-8c86-4a8b-bc98-36f1c3d7fdb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4963bdca-2924-4933-936f-dad3eacfddea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "525dc600-8c86-4a8b-bc98-36f1c3d7fdb1",
                    "LayerId": "90460519-8a79-4965-8a5d-82e5846ee551"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "90460519-8a79-4965-8a5d-82e5846ee551",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f2ccbeb-6d71-4ae9-ac3f-4a253783d77f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}