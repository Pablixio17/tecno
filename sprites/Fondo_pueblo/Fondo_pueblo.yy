{
    "id": "eed5fdd1-59ef-4623-8d26-b8e0dd79195d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Fondo_pueblo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cea4318-fa03-4bf1-ae3b-185cf0760d37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eed5fdd1-59ef-4623-8d26-b8e0dd79195d",
            "compositeImage": {
                "id": "1f4e4274-6260-4953-b2bb-3c44188ba40d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cea4318-fa03-4bf1-ae3b-185cf0760d37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "710a02e2-f637-471b-a6a6-b155f59ef53a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cea4318-fa03-4bf1-ae3b-185cf0760d37",
                    "LayerId": "f16e4c1b-1367-401c-9e88-877e4199bc8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "f16e4c1b-1367-401c-9e88-877e4199bc8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eed5fdd1-59ef-4623-8d26-b8e0dd79195d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}