{
    "id": "55ef98fb-39ee-4322-900d-91cb8b3a97ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SYMI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 16,
    "bbox_right": 43,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81f53544-62a9-4448-9d2a-1177b0945f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55ef98fb-39ee-4322-900d-91cb8b3a97ac",
            "compositeImage": {
                "id": "16e3b155-ef2b-43f8-ae42-df1f63d8918b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f53544-62a9-4448-9d2a-1177b0945f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bd310f9-8f16-4318-aaf4-0b4239681e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f53544-62a9-4448-9d2a-1177b0945f76",
                    "LayerId": "c920c487-77b3-40b2-a5e6-7d7aeb8eb58a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 88,
    "layers": [
        {
            "id": "c920c487-77b3-40b2-a5e6-7d7aeb8eb58a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55ef98fb-39ee-4322-900d-91cb8b3a97ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}