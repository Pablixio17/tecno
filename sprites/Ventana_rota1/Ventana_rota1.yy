{
    "id": "ecb294ca-83c1-400d-a768-4f52f4f5cfe1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ventana_rota1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59af63f2-02ff-4266-9b45-2297cc70040e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecb294ca-83c1-400d-a768-4f52f4f5cfe1",
            "compositeImage": {
                "id": "04b8f607-9aab-472d-9ab5-460e2bff8968",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59af63f2-02ff-4266-9b45-2297cc70040e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8351018a-4f32-4a92-9f35-26b2e4e7de73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59af63f2-02ff-4266-9b45-2297cc70040e",
                    "LayerId": "a7ba9a1d-2a05-43c0-ac4e-2e9327d51b3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a7ba9a1d-2a05-43c0-ac4e-2e9327d51b3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecb294ca-83c1-400d-a768-4f52f4f5cfe1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}