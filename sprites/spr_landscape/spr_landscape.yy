{
    "id": "3080c388-ac7e-4b91-bdd0-fa1346da697e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_landscape",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 703,
    "bbox_left": 528,
    "bbox_right": 989,
    "bbox_top": 640,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b073cd2b-f0e0-4c9e-9a2b-cd7c37fd03b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3080c388-ac7e-4b91-bdd0-fa1346da697e",
            "compositeImage": {
                "id": "54999829-2da2-4d77-b376-5e1b0bea7fda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b073cd2b-f0e0-4c9e-9a2b-cd7c37fd03b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33959ca9-7e27-48e6-983f-d671fe65e4f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b073cd2b-f0e0-4c9e-9a2b-cd7c37fd03b9",
                    "LayerId": "d3159b96-ce36-42ff-b417-7a988cc1aef6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "d3159b96-ce36-42ff-b417-7a988cc1aef6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3080c388-ac7e-4b91-bdd0-fa1346da697e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}