{
    "id": "2118332a-3772-4187-97b0-0844075d165c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SYMI_D",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 33,
    "bbox_right": 78,
    "bbox_top": 31,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21529782-d9c4-4199-bd4c-971928a2c25b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2118332a-3772-4187-97b0-0844075d165c",
            "compositeImage": {
                "id": "2ce7d3c6-9532-455c-824e-a734323d0c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21529782-d9c4-4199-bd4c-971928a2c25b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18a6173-53fc-464b-a334-ecb06c36107f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21529782-d9c4-4199-bd4c-971928a2c25b",
                    "LayerId": "e30f79ca-5f9f-43b3-80c7-82c6d1f04432"
                }
            ]
        },
        {
            "id": "023b1458-f6a9-4774-b4ce-0c48fc99320f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2118332a-3772-4187-97b0-0844075d165c",
            "compositeImage": {
                "id": "e76c98b0-521e-4553-8f8f-458d4c7672e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "023b1458-f6a9-4774-b4ce-0c48fc99320f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7937238a-bbab-4641-8563-4b06ea469b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "023b1458-f6a9-4774-b4ce-0c48fc99320f",
                    "LayerId": "e30f79ca-5f9f-43b3-80c7-82c6d1f04432"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "e30f79ca-5f9f-43b3-80c7-82c6d1f04432",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2118332a-3772-4187-97b0-0844075d165c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 0,
    "yorig": 0
}