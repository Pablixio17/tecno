{
    "id": "e5c66efb-3ae7-43bf-a6da-d700ba2a5993",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05a862a8-6198-47cd-a2f1-18b7dc0017c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5c66efb-3ae7-43bf-a6da-d700ba2a5993",
            "compositeImage": {
                "id": "061a7a7e-25c8-459b-993d-96e76bad67a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a862a8-6198-47cd-a2f1-18b7dc0017c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82a4eade-d036-4364-8fae-02fa9838f04b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a862a8-6198-47cd-a2f1-18b7dc0017c5",
                    "LayerId": "495302f5-6afd-4f09-ba87-46e082c33271"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "495302f5-6afd-4f09-ba87-46e082c33271",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5c66efb-3ae7-43bf-a6da-d700ba2a5993",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}