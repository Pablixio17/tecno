{
    "id": "fa2020c9-b00c-46e9-b166-ae633831b2c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lampara",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f642c967-9f7d-46b0-9e91-40dab01b9ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa2020c9-b00c-46e9-b166-ae633831b2c9",
            "compositeImage": {
                "id": "c885583e-00d9-4a8b-ab14-73f58d1c69d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f642c967-9f7d-46b0-9e91-40dab01b9ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66285656-1cca-4172-aaaf-05b28257fe1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f642c967-9f7d-46b0-9e91-40dab01b9ca9",
                    "LayerId": "dc8220b2-76cb-4c5a-b470-20aebaad14e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dc8220b2-76cb-4c5a-b470-20aebaad14e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa2020c9-b00c-46e9-b166-ae633831b2c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}