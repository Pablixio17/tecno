{
    "id": "c08af290-a720-4211-a1bc-cb13f9bf3f20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "puerta_provisional",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ddf53de-605a-49c3-a3de-ddb1416509b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c08af290-a720-4211-a1bc-cb13f9bf3f20",
            "compositeImage": {
                "id": "7a6b23d1-754b-4373-b5c7-16f77fddd34a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ddf53de-605a-49c3-a3de-ddb1416509b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "239bf578-0503-48e5-ac61-49ee095b297c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ddf53de-605a-49c3-a3de-ddb1416509b2",
                    "LayerId": "ae0685b2-2a27-43d1-8983-218dbe13f7b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ae0685b2-2a27-43d1-8983-218dbe13f7b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c08af290-a720-4211-a1bc-cb13f9bf3f20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 127
}