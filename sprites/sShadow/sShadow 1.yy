{
    "id": "c1c97b2b-360b-43e6-a4f4-16ddaad13d94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d7385c0-8aab-40d9-bf3b-d5358139c85f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1c97b2b-360b-43e6-a4f4-16ddaad13d94",
            "compositeImage": {
                "id": "7fafdc6c-4d16-48b0-b9c5-7eb9a2b15529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d7385c0-8aab-40d9-bf3b-d5358139c85f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fd1336a-5760-4047-8272-73e2ae660205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d7385c0-8aab-40d9-bf3b-d5358139c85f",
                    "LayerId": "cc685c1f-8a9c-4188-ad6a-0e41c6388a5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "cc685c1f-8a9c-4188-ad6a-0e41c6388a5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1c97b2b-360b-43e6-a4f4-16ddaad13d94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 128
}