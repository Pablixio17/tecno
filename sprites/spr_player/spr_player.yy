{
    "id": "c101a335-1bee-4b69-b84e-b870535cb985",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 7,
    "bbox_right": 55,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98a2753c-58bd-43ed-b69c-d7c569e8c2d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c101a335-1bee-4b69-b84e-b870535cb985",
            "compositeImage": {
                "id": "ff6aa8fb-4a0b-4859-a12c-e021b536df82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98a2753c-58bd-43ed-b69c-d7c569e8c2d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90d4b15c-1976-450d-892c-a2b6ff0ce05f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98a2753c-58bd-43ed-b69c-d7c569e8c2d9",
                    "LayerId": "6a770e88-1688-4771-aadf-d52f6ce100c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 88,
    "layers": [
        {
            "id": "6a770e88-1688-4771-aadf-d52f6ce100c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c101a335-1bee-4b69-b84e-b870535cb985",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 44
}