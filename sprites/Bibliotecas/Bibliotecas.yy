{
    "id": "ded9c5bc-708c-42e5-80ec-e930bc37a919",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Bibliotecas",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31ab2e59-06e6-4b23-8782-d42810d5b2c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ded9c5bc-708c-42e5-80ec-e930bc37a919",
            "compositeImage": {
                "id": "0fffbfe7-c8d8-4203-a68f-80c3699031cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ab2e59-06e6-4b23-8782-d42810d5b2c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b211bab-965c-4d96-b879-e9658976d36d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ab2e59-06e6-4b23-8782-d42810d5b2c7",
                    "LayerId": "d923e38f-796f-47cf-b702-756057b3a49a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d923e38f-796f-47cf-b702-756057b3a49a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ded9c5bc-708c-42e5-80ec-e930bc37a919",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}