// Calculate movement
/*
#region vars calcular movimiento


#region cambio de estado
if (key_slash) and (vSpeed <= 0) and (!key_jump) and (place_meeting(x,y+1,oWall)) state = PLAYERSTATE.ATTACK_SLASH
#endregion
#region shadow 1
if (!place_meeting(x+1,y,obj_sizeshadow)){
		obj_sizeshadow.image_alpha = 0.5;
}else{
	
		obj_sizeshadow.image_alpha = 0;
	}

	#endregion
#region shadow 2
if (!place_meeting(x+1,y,obj_sizeshadow2)){
		obj_sizeshadow2.image_alpha = 0.9;
}else{
	
		obj_sizeshadow2.image_alpha = 0;
	}

	#endregion
	
#region dialogar

if(mouse_check_button_pressed(mb_left)){
	//1.Check for any entity to activate
	//2.If there is nothing, or there is something,but it has no script -Attack
	//Otherwise if it has a script , activate it
	//If is an NPC make face towards us
	
	var _activateX = lengthdir_x(10,direction);
	var _activateY = lengthdir_y(10,direction);
	activate =	instance_position(x+_activateX,y+_activateY,pEntity)
	
	if(activate == noone || activate.entityActivateScript == -1)
	{
		state = PLAYERSTATE.FREE;
		
	}	
	else
	{
		ScriptExecuteArray(activate.entityActivateScript,activate.entityActivateArgs);
		
		if (activate.entityNPC){
			with(activate){
				direction = point_direction(x,y,other.x,other.y);
				image_index = CARDINAL_DIR
				with(oCamera){
					target = pEntity;
				}
				
			}
			
		}
	}
	
}
#endregion