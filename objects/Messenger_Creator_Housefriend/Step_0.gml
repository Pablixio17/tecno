/// @desc
room_transitioner_time3++;
#region Housefriend
if (room_transitioner_time3 = room_speed/3){
		with(Housefriend_messenger) image_alpha = 0.1;
	}
	if (room_transitioner_time3 = room_speed/2.75){
		with(Housefriend_messenger) image_alpha = 0.2;
	}
	if (room_transitioner_time3 = room_speed/2.5){
		with(Housefriend_messenger) image_alpha = 0.3;
	}
	if (room_transitioner_time3 = room_speed/2.25){
		with(Housefriend_messenger) image_alpha = 0.4;
	}
	if (room_transitioner_time3 = room_speed/2){
		with(Housefriend_messenger) image_alpha = 0.5;
	}
	if (room_transitioner_time3 = room_speed/1.75){
		with(Housefriend_messenger) image_alpha = 0.6;
	}
	if (room_transitioner_time3 = room_speed/1.5){
		with(Housefriend_messenger) image_alpha = 0.7;
	}
	if (room_transitioner_time3 = room_speed/1.25){
		with(Housefriend_messenger) image_alpha = 0.8;
	}
	if (room_transitioner_time3 = room_speed){
		with(Housefriend_messenger) image_alpha = 0.9;
	}
	if (room_transitioner_time3 = room_speed){
		with(Housefriend_messenger) image_alpha = 1;
	}
	if (room_transitioner_time3 >= room_speed){
		timer5--;
		if(timer5 <= 0){
		Housefriend_messenger.image_alpha = 0;
		obj_player.hasmotion = true;
		}
		
	}
	#endregion