{
    "id": "2f77b26e-3859-4a9e-ac86-30ce0bf09b49",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_SYMIAnimation",
    "eventList": [
        {
            "id": "850fe9b4-fbe0-4f76-97af-b37a23267241",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f77b26e-3859-4a9e-ac86-30ce0bf09b49"
        },
        {
            "id": "ddc01f07-62a0-4a3d-a5de-6b4028633170",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f77b26e-3859-4a9e-ac86-30ce0bf09b49"
        },
        {
            "id": "b45c13f5-1326-4520-9eda-809ba40e22e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f4597536-c0d3-4c41-a843-2a1802954b42",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f77b26e-3859-4a9e-ac86-30ce0bf09b49"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "60ef3584-0544-4307-a79d-6819a81bff25",
    "visible": true
}