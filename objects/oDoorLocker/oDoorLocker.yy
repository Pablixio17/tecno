{
    "id": "1f706aaf-a523-4f98-aa1c-92ea0dd05fc0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoorLocker",
    "eventList": [
        {
            "id": "b15613ee-cb4c-4ac1-8631-4f4e35903d82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f706aaf-a523-4f98-aa1c-92ea0dd05fc0"
        },
        {
            "id": "8b0c31a6-345c-4fed-ad17-ee1ef0c6e04a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f706aaf-a523-4f98-aa1c-92ea0dd05fc0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "37fb16e8-13aa-4fac-bfcb-6027107d9f09",
    "visible": false
}