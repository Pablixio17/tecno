{
    "id": "40a07484-d028-4539-bcf2-d82b8eceb4d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoorL",
    "eventList": [
        {
            "id": "c891c2f2-0bc0-45b2-8d24-70bb9095d7ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6964a90e-d74c-4ca4-9655-ccf2843f89fe",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "40a07484-d028-4539-bcf2-d82b8eceb4d9"
        },
        {
            "id": "c427a6d2-7729-40f1-8eb3-ab2ffba641f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "40a07484-d028-4539-bcf2-d82b8eceb4d9"
        },
        {
            "id": "66acf38f-b072-4272-9fe4-fd85017f0a35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "40a07484-d028-4539-bcf2-d82b8eceb4d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c2f07c1c-3cb9-4c53-93d0-115a1dac9778",
    "visible": true
}