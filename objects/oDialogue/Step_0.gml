/// @desc
lerpProgress += (1-lerpProgress)/50;
textProgress += global.textspeed;

x1 = lerp(x1,x1target,lerpProgress);
x2 = lerp(x2,x2target,lerpProgress);

//if (keyboard_check_pressed(vk_space))
//{
	var _messagelength = string_length(message);
	if(textProgress >= _messagelength){
		time--;
		if (time <= 0)instance_destroy();
		if(instance_exists(oDialogueQueued)){
			with(oDialogueQueued)ticket--;
	}
	else{
		if(textProgress > 2){
			textProgress = _messagelength;
		}
	}
}
//}