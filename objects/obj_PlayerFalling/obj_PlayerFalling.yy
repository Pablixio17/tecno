{
    "id": "aaded7f2-e4fd-4d3f-8ac9-fa1e27f9b5cf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PlayerFalling",
    "eventList": [
        {
            "id": "57b81b1d-8c7e-4451-bdd1-d742ad94c6a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aaded7f2-e4fd-4d3f-8ac9-fa1e27f9b5cf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7df77f07-8021-46ee-8ba7-5fb71e565de0",
    "visible": true
}