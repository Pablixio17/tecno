{
    "id": "5dfd87ef-34f3-426b-b3b8-31baf40b8e14",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_DoorUnlocker",
    "eventList": [
        {
            "id": "4a22fbe8-9711-4073-872d-f921fc2e428f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6964a90e-d74c-4ca4-9655-ccf2843f89fe",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5dfd87ef-34f3-426b-b3b8-31baf40b8e14"
        },
        {
            "id": "0ccb08e9-d56a-4b2d-9662-a29eaf551427",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5dfd87ef-34f3-426b-b3b8-31baf40b8e14"
        },
        {
            "id": "2cb22cdb-3ab0-4034-9c1b-5faba1deefe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5dfd87ef-34f3-426b-b3b8-31baf40b8e14"
        }
    ],
    "maskSpriteId": "1a349654-056b-4077-8b43-04380c1bd6ae",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a349654-056b-4077-8b43-04380c1bd6ae",
    "visible": false
}