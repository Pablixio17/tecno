{
    "id": "3ef9711c-57f4-4827-b514-da6c724bdf19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playerfall2",
    "eventList": [
        {
            "id": "04daddf5-e511-40f4-8e80-07f954cd84b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3ef9711c-57f4-4827-b514-da6c724bdf19"
        },
        {
            "id": "3a781e04-aa21-4220-b2b4-6bcc5d418819",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3ef9711c-57f4-4827-b514-da6c724bdf19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20677ca6-f404-4693-9fdd-ad7880adff5c",
    "visible": true
}