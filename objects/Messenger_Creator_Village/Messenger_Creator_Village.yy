{
    "id": "8c781b2e-9237-4d42-9a7a-c50cfcf772d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Messenger_Creator_Village",
    "eventList": [
        {
            "id": "3a9288d0-d7d0-4cbe-a57a-5826080cede5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "8c781b2e-9237-4d42-9a7a-c50cfcf772d7"
        },
        {
            "id": "93f68f21-85d9-48b8-b8e1-584768cd0ea0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8c781b2e-9237-4d42-9a7a-c50cfcf772d7"
        },
        {
            "id": "51a83fe1-a6b8-46ea-95dd-e27e6ddca6e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8c781b2e-9237-4d42-9a7a-c50cfcf772d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}