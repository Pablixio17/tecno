{
    "id": "98c1607e-227f-47b1-987c-4280fa753c92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transition2",
    "eventList": [
        {
            "id": "9f49c1ff-0885-4d4c-bab7-35e7ed293325",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aaded7f2-e4fd-4d3f-8ac9-fa1e27f9b5cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "98c1607e-227f-47b1-987c-4280fa753c92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "005defc4-2cd3-4198-95b0-d19580759604",
    "visible": true
}