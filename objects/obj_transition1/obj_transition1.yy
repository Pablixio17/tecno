{
    "id": "ef08104d-2853-497d-8b39-9eb379d88a92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transition1",
    "eventList": [
        {
            "id": "adf01da6-7437-4062-a804-684a0edfcdf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6964a90e-d74c-4ca4-9655-ccf2843f89fe",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ef08104d-2853-497d-8b39-9eb379d88a92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8f2ccbeb-6d71-4ae9-ac3f-4a253783d77f",
    "visible": true
}