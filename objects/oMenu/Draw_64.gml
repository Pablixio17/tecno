/// @desc Draw Menu

draw_set_font(fMenu);
draw_set_halign(fa_right);
draw_set_valign(fa_bottom);

for (var i = 0; i < menu_items;i++)
{
	var _col;
	var _offset = 2
	var txt = menu[i]
		if (menu_cursor == i){
			txt = string_insert("> ",txt ,0)
			var _col = c_white
		}
		else
		{
			var _col = c_gray
		}
		var xx = menu_x
		var yy = menu_y - (menu_itemheight * ( i* 1.5 ))
		draw_set_color(c_black)
		draw_text(xx,yy,txt)
		draw_text(xx+_offset,yy,txt)
		draw_text(xx,yy,txt)
		draw_text(xx,yy-_offset,txt)
		draw_set_color(_col)
		draw_text(xx,yy,txt)
}
draw_set_font(fnt_normal)