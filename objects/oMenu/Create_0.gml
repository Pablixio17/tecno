/// @desc GUI/Vars/Menu setup
#macro SAVEFILE "Save.sav"



gui_width = display_get_gui_width();
gui_heigth = display_get_gui_height();
gui_margin = 32


menu_x = gui_width + 200
menu_y = gui_heigth- gui_margin;
menu_x_target = gui_width- gui_margin
menu_speed = 25;
menu_font =	fMenu;
menu_itemheight = font_get_size(fMenu);
menu_commited = -1;
menu_control = true;

menu[2] = "Nuevo Juego";
menu[1] = "Continuar";
menu[0] = "Salir";

menu_items = array_length_1d(menu);
menu_cursor = 2;

