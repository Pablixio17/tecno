{
    "id": "7ae60dd8-c3d9-46c9-ad62-3fc8750a67b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDust",
    "eventList": [
        {
            "id": "391665f7-1677-4e28-8bb5-eb56917014ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7ae60dd8-c3d9-46c9-ad62-3fc8750a67b8"
        },
        {
            "id": "d68cfbd0-e449-4a66-9351-83fa27331c80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7ae60dd8-c3d9-46c9-ad62-3fc8750a67b8"
        },
        {
            "id": "63abc276-4830-4277-8e82-b1c4272347ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7ae60dd8-c3d9-46c9-ad62-3fc8750a67b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20b35234-3e89-4162-9224-12594267853d",
    "visible": true
}