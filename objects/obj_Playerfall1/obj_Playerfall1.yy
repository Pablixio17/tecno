{
    "id": "14a384b5-312d-442e-a62d-d0c488c2c98a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Playerfall1",
    "eventList": [
        {
            "id": "4234dfb9-11f4-47bc-9655-0c4a7c147861",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "14a384b5-312d-442e-a62d-d0c488c2c98a"
        },
        {
            "id": "662fe3ed-3656-471d-b6a8-875de8a53d04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "14a384b5-312d-442e-a62d-d0c488c2c98a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1788e6ae-6dbf-41fe-a293-482e0996448f",
    "visible": true
}