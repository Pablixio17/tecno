{
    "id": "89df00f1-dd44-46b3-a5c9-29d2e06170e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_SYMI",
    "eventList": [
        {
            "id": "e7012c65-ff43-4a68-8b5e-de5e7805ce5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89df00f1-dd44-46b3-a5c9-29d2e06170e7"
        },
        {
            "id": "c4a333b7-3a61-4521-9fcb-974946861dc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89df00f1-dd44-46b3-a5c9-29d2e06170e7"
        },
        {
            "id": "56cd4f26-6824-4858-ba41-e15ceebc2648",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "89df00f1-dd44-46b3-a5c9-29d2e06170e7"
        },
        {
            "id": "8e6cc347-debd-43dd-80a6-0d88b91acd26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "89df00f1-dd44-46b3-a5c9-29d2e06170e7"
        }
    ],
    "maskSpriteId": "55ef98fb-39ee-4322-900d-91cb8b3a97ac",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "55ef98fb-39ee-4322-900d-91cb8b3a97ac",
    "visible": true
}