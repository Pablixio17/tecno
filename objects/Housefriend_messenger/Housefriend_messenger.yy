{
    "id": "2550348b-16b7-425e-97de-effdc133a166",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Housefriend_messenger",
    "eventList": [
        {
            "id": "f7c11fa8-ac44-4350-8ac5-4b629aaa17ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2550348b-16b7-425e-97de-effdc133a166"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9591bbf-77ad-48cf-9195-2ede6d5d4ea9",
    "visible": true
}