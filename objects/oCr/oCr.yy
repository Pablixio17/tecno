{
    "id": "071b2d6c-a43f-4343-a073-4511c9b56b27",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCr",
    "eventList": [
        {
            "id": "390a8321-ecb3-4540-a574-8a5de72c1ef1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "071b2d6c-a43f-4343-a073-4511c9b56b27"
        },
        {
            "id": "d406aae8-2530-4c93-acb3-a0602fc7de00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "071b2d6c-a43f-4343-a073-4511c9b56b27"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f2be2462-47da-4768-b5e7-d6248648b184",
    "visible": true
}