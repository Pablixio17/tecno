{
    "id": "0f5cd03d-9c48-4de8-8f19-20e32e3dc457",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "door_1",
    "eventList": [
        {
            "id": "de1b3b1c-93a2-4e05-a07b-f00a4bbccd20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "0f5cd03d-9c48-4de8-8f19-20e32e3dc457"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c08af290-a720-4211-a1bc-cb13f9bf3f20",
    "visible": true
}