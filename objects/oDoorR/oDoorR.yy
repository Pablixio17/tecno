{
    "id": "e35c2fa6-d092-4ac1-9c17-e24c118715e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoorR",
    "eventList": [
        {
            "id": "461dfbb8-23df-4f1a-a056-d2d493917f70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6964a90e-d74c-4ca4-9655-ccf2843f89fe",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e35c2fa6-d092-4ac1-9c17-e24c118715e8"
        },
        {
            "id": "c9c4019b-8d41-42b7-9ef4-b94222e5c793",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e35c2fa6-d092-4ac1-9c17-e24c118715e8"
        },
        {
            "id": "b3228848-9cbb-4f91-92d5-80440b58b89d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e35c2fa6-d092-4ac1-9c17-e24c118715e8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9a981d0d-75c5-43f7-b540-1558551b8b1e",
    "visible": true
}