{
    "id": "2e68457e-cb11-44cd-8693-81b1c8d2ee4e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oText",
    "eventList": [
        {
            "id": "0ec71b6b-c1d1-4cdb-89ab-fecfcb3c143c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e68457e-cb11-44cd-8693-81b1c8d2ee4e"
        },
        {
            "id": "b677a08f-cea8-412e-a86d-a93440f75f2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2e68457e-cb11-44cd-8693-81b1c8d2ee4e"
        },
        {
            "id": "9f57d7ac-7e20-437c-b228-334cee7e5ee1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2e68457e-cb11-44cd-8693-81b1c8d2ee4e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}