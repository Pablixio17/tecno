{
    "id": "92fed961-5cbb-42d9-9c1e-761f859c61e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Messenger_Creator_house",
    "eventList": [
        {
            "id": "cf3a17de-0d19-4db2-a6fc-6d600ea0e06f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "92fed961-5cbb-42d9-9c1e-761f859c61e0"
        },
        {
            "id": "962129b1-6c76-445f-85c6-66488a890e09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92fed961-5cbb-42d9-9c1e-761f859c61e0"
        },
        {
            "id": "4e33dd50-f2e2-4ca4-93b1-ec881ef9fb46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "92fed961-5cbb-42d9-9c1e-761f859c61e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}