/// @desc
room_transitioner_time++;
#region Casa
if (room_transitioner_time = room_speed/3){
		with(House_messenger) image_alpha = 0.1;
	}
	if (room_transitioner_time = room_speed/2.75){
		with(House_messenger) image_alpha = 0.2;
	}
	if (room_transitioner_time = room_speed/2.5){
		with(House_messenger) image_alpha = 0.3;
	}
	if (room_transitioner_time = room_speed/2.25){
		with(House_messenger) image_alpha = 0.4;
	}
	if (room_transitioner_time = room_speed/2){
		with(House_messenger) image_alpha = 0.5;
	}
	if (room_transitioner_time = room_speed/1.75){
		with(House_messenger) image_alpha = 0.6;
	}
	if (room_transitioner_time = room_speed/1.5){
		with(House_messenger) image_alpha = 0.7;
	}
	if (room_transitioner_time = room_speed/1.25){
		with(House_messenger) image_alpha = 0.8;
	}
	if (room_transitioner_time = room_speed){
		with(House_messenger) image_alpha = 0.9;
	}
	if (room_transitioner_time = room_speed){
		with(House_messenger) image_alpha = 1;
	}
	if (room_transitioner_time >= room_speed){
		timer3--;
		if(timer3 <= 0){
		House_messenger.image_alpha = 0;
		obj_player.hasmotion = true;
		}
		
	}
	#endregion