{
    "id": "6964a90e-d74c-4ca4-9655-ccf2843f89fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "a67407f3-fbd0-46ea-bb67-a718a45b087f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6964a90e-d74c-4ca4-9655-ccf2843f89fe"
        },
        {
            "id": "e8de5717-9e5e-4503-87b7-b3be3db664df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6964a90e-d74c-4ca4-9655-ccf2843f89fe"
        },
        {
            "id": "b9873915-6b34-41d0-b822-79d79bc616a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6964a90e-d74c-4ca4-9655-ccf2843f89fe"
        },
        {
            "id": "80dc2f9e-39ff-4cb9-8d29-4437e1f6df38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "6964a90e-d74c-4ca4-9655-ccf2843f89fe"
        },
        {
            "id": "75a045fe-45b5-4797-ad20-34e4464284e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "6964a90e-d74c-4ca4-9655-ccf2843f89fe"
        },
        {
            "id": "15cb8c94-f4ba-4bf3-b289-9a6229acb9c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "6964a90e-d74c-4ca4-9655-ccf2843f89fe"
        },
        {
            "id": "de1de88e-ffb0-44c7-bffa-d4cfaacdf40f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6964a90e-d74c-4ca4-9655-ccf2843f89fe"
        }
    ],
    "maskSpriteId": "c101a335-1bee-4b69-b84e-b870535cb985",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c101a335-1bee-4b69-b84e-b870535cb985",
    "visible": true
}