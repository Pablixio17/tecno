///@description Core Player Logic
// get player inputs
if (hasmotion){
#region teclas
key_left = keyboard_check(vk_left)|| keyboard_check(ord("A"))
key_right = keyboard_check(vk_right) || keyboard_check(ord("D"))
key_jump = keyboard_check_pressed(vk_space);
key_up = keyboard_check(vk_up) || keyboard_check(ord("W"))
key_down = keyboard_check(vk_down) || keyboard_check(ord("S"))
key_slash = mouse_check_button(mb_left)

#endregion


grounded = place_meeting(x,y+1,oWall)
switch balancingstate{
case pState.normal:{
	var _dir = key_right - key_left;
		hSpeed += _dir *  walkAcceleration;
		
		if (_dir == 0){
			var _hFriction = hFrictionGround;
			if (!grounded) _hFriction = hFrictionAir;
			hSpeed = Approach(hSpeed,0,_hFriction);
		}
			hSpeed = clamp(hSpeed, -walkspeed,walkspeed);
			
			vSpeed += gravity_;
			
			if(key_jump) && (grounded){
			grounded = false;	
			vSpeedFraction = 0;
			vSpeed = -jumpspeed;
			}
			if (mouse_check_button_pressed(mb_right)){
				grapple_X = mouse_x;
				grapple_Y = mouse_y;
				rope_x = x;
				rope_y = y;
				ropeAngleVelocity = 0;
				ropeAngle = point_direction(grapple_X,grapple_Y,x,y);
				ropeLength = point_distance(grapple_X,grapple_Y,x,y);
				balancingstate = pState.swing;
			}
			if (key_slash){
				balancingstate = pState.ATTACK_SLASH;
			}
		}break;
		
		case pState.swing:{
			var _ropeAngleAcceleration = -0.2 * dcos(ropeAngle);
			_ropeAngleAcceleration += (key_right-key_left)*0.08
			ropeLength += (key_down-key_up)*2;
			ropeLength = max(ropeLength,0);
			
			ropeAngleVelocity += _ropeAngleAcceleration;
			ropeAngle += ropeAngleVelocity;
			ropeAngleVelocity *= 0.99;
			
			rope_x = grapple_X+ lengthdir_x(ropeLength,ropeAngle);
			rope_y = grapple_Y+ lengthdir_y(ropeLength,ropeAngle);
			
			hSpeed = rope_x - x;
			vSpeed = rope_y - y;
			
			if(key_jump){
				balancingstate = pState.normal;
				vSpeedFraction = 0;
				vSpeed = -jumpspeed
			}
			}break;
			
			case pState.ATTACK_SLASH:{
		//Start of the attack
		if(sprite_index != spr_playerSlash1){
			sprite_index = spr_playerSlash1;
			image_index = 0;
			ds_list_clear(hitByAttack);
		}

		// Use attack hitbox
		mask_index = spr_playerSlashHB;
		var hitByAttackNow = ds_list_create();
		var hits = instance_place_list(x,y,obj_SYMI,hitByAttackNow,false);
		if (hits > 0){
			for (var i  = 0; i < hits; i++){
				// if this instance has not yet been hit by this attack
				var hitID = hitByAttackNow[| i];
				if (ds_list_find_index(hitByAttack,hitID) == -1){
					ds_list_add(hitByAttack,hitID);
					with (hitID){
						EnemyHit(1);
					}
				}
			}
		}
		ds_list_destroy(hitByAttackNow);
		mask_index = spr_player;
		if(animationEnd){
		sprite_index = spr_player;
		balancingstate = pState.normal
		}
			}break;
}
	hSpeed +=hSpeedFraction;
	vSpeed +=vSpeedFraction;
	hSpeedFraction= frac(hSpeed);
	vSpeedFraction= frac(vSpeed);
	hSpeed -= hSpeedFraction;
	vSpeed -= vSpeedFraction;
			
	if(place_meeting(x+hSpeed,y,oWall))
	{
		var _hStep = sign(hSpeed);
		hSpeed = 0;
		hSpeedFraction = 0;
		while(!place_meeting(x+_hStep,y,oWall)) x+= _hStep;
		if(balancingstate == pState.swing){
			ropeAngle = point_direction(grapple_X,grapple_Y,x,y);
			ropeAngleVelocity = 0;
			
		}
	}
	x+= hSpeed;
	if(place_meeting(x,y+vSpeed,oWall))
	{
		var _vStep = sign(vSpeed);
		vSpeed = 0;
		vSpeedFraction = 0;
		while(!place_meeting(x,y+_vStep,oWall)) y+= _vStep;
		if(balancingstate == pState.swing){
			ropeAngle = point_direction(grapple_X,grapple_Y,x,y);
			ropeAngleVelocity = 0;
			
		}
	}
	y+= vSpeed;
	if (!place_meeting(x,y+1,oWall))
{
	sprite_index = spr_playerA
	image_speed=0
	if(vSpeed > 0) image_index = 1; else image_index = 0
}else {
	if (sprite_index = spr_playerA){
		repeat(5){
			with(instance_create_layer(x,bbox_bottom,"Bullets",oDust)){
				vSpeed = 0;
			}
		}
	}
		image_speed = 1
		if (hSpeed==0)
		{
		sprite_index= spr_player	
		}
		else
		{
		sprite_index= spr_playerR	
		}
        }

		
if (hSpeed!=0){
image_xscale = sign(hSpeed)
}


#region shadow 1
if (!place_meeting(x+1,y,obj_sizeshadow)){
		obj_sizeshadow.image_alpha = 0.5;
}else{
	
		obj_sizeshadow.image_alpha = 0;
	}

	#endregion
#region shadow 2
if (!place_meeting(x+1,y,obj_sizeshadow2)){
		obj_sizeshadow2.image_alpha = 0.9;
}else{
	
		obj_sizeshadow2.image_alpha = 0;
	}

	#endregion
	
#region dialogar

if(mouse_check_button_pressed(mb_left)){
	//1.Check for any entity to activate
	//2.If there is nothing, or there is something,but it has no script -Attack
	//Otherwise if it has a script , activate it
	//If is an NPC make face towards us
	
	var _activateX = lengthdir_x(10,direction);
	var _activateY = lengthdir_y(10,direction);
	activate =	instance_position(x+_activateX,y+_activateY,pEntity)
	
	if(activate == noone || activate.entityActivateScript == -1)
	{
		balancingstate = pState.normal;
		
	}	
	else
	{
		ScriptExecuteArray(activate.entityActivateScript,activate.entityActivateArgs);
		
		if (activate.entityNPC){
			with(activate){
				direction = point_direction(x,y,other.x,other.y);
				image_index = CARDINAL_DIR
				with(oCamera){
					target = pEntity;
				}
				
			}
			
		}
	}
	
}
}
#endregion





