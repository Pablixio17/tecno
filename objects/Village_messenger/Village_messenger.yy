{
    "id": "68702129-23fd-4e88-b1c6-74debb62cf3d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Village_messenger",
    "eventList": [
        {
            "id": "0b52ced1-a26a-4ee1-9fe1-4482a124b565",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68702129-23fd-4e88-b1c6-74debb62cf3d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "deeb2dcc-9f3e-470d-9bb8-a6408f334733",
    "visible": true
}