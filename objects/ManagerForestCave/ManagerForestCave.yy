{
    "id": "d1ce7774-3dbf-4ae9-8211-cfd8a47930c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ManagerForestCave",
    "eventList": [
        {
            "id": "5a6bf7d4-d40d-4296-87c9-407231609be4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d1ce7774-3dbf-4ae9-8211-cfd8a47930c0"
        },
        {
            "id": "398acbf7-cd56-43ae-8818-35573cb6cbc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "d1ce7774-3dbf-4ae9-8211-cfd8a47930c0"
        },
        {
            "id": "9ccb2c36-bc90-4acc-9a1b-c2b7151975f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d1ce7774-3dbf-4ae9-8211-cfd8a47930c0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}