{
    "id": "777f23b7-79f2-42ba-9fe7-fc4a57adbda5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSign",
    "eventList": [
        {
            "id": "73543010-268a-4dfc-904d-d7bd1f2963f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "777f23b7-79f2-42ba-9fe7-fc4a57adbda5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "3bb686a2-e357-4a77-a4de-db451436a1f9",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"default\"",
            "varName": "text",
            "varType": 2
        }
    ],
    "solid": false,
    "spriteId": "e35eb2a1-762a-483c-908a-3ecf3b4c7945",
    "visible": true
}