{
    "id": "c872282d-fed9-4f8d-9629-8bb6aa10d5a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_deadE",
    "eventList": [
        {
            "id": "0996c4e2-8072-4ca0-ad02-930c610cc099",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c872282d-fed9-4f8d-9629-8bb6aa10d5a9"
        },
        {
            "id": "4483fea6-50ac-41e8-9663-ec0e01d0377e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c872282d-fed9-4f8d-9629-8bb6aa10d5a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "52187377-3f3b-45a6-ac19-367850e813ba",
    "visible": true
}