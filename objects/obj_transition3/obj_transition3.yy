{
    "id": "9119e3db-35e3-40ae-9b6f-3342c9ca4ab6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transition3",
    "eventList": [
        {
            "id": "56d22001-5325-4d0f-9897-3664f5f436f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9119e3db-35e3-40ae-9b6f-3342c9ca4ab6"
        },
        {
            "id": "6879b7de-3cfe-4f2a-a596-61c159dd34e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2f77b26e-3859-4a9e-ac86-30ce0bf09b49",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9119e3db-35e3-40ae-9b6f-3342c9ca4ab6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "005defc4-2cd3-4198-95b0-d19580759604",
    "visible": true
}