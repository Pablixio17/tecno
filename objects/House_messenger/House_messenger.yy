{
    "id": "9d4b5184-0503-430f-a8df-218ee64d1f66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "House_messenger",
    "eventList": [
        {
            "id": "1e40eec6-815b-4597-b3d0-812e0fe84c34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "9d4b5184-0503-430f-a8df-218ee64d1f66"
        },
        {
            "id": "1702db30-20cb-4c2c-9401-618278b72502",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d4b5184-0503-430f-a8df-218ee64d1f66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "deb16986-446a-4f8e-9038-e5935803edd3",
    "visible": true
}