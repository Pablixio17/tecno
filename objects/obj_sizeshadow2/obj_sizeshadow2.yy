{
    "id": "dc68b830-302a-4168-aca3-3e01c4860556",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sizeshadow2",
    "eventList": [
        {
            "id": "ef7f7157-6ac9-4d3d-936d-9b5362c9249c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc68b830-302a-4168-aca3-3e01c4860556"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c1c97b2b-360b-43e6-a4f4-16ddaad13d94",
    "visible": true
}