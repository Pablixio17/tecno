{
    "id": "3d935c7d-59b1-46f5-b891-e2b96a6d799f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sizeshadow",
    "eventList": [
        {
            "id": "096cbd6a-091b-4895-8dc4-ca69f33564e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d935c7d-59b1-46f5-b891-e2b96a6d799f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c1c97b2b-360b-43e6-a4f4-16ddaad13d94",
    "visible": true
}