{
    "id": "47574f27-1aaf-454e-a78b-18df6621e1da",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_normal",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ec4e171d-1dc8-495b-90e6-2585906af36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "303a4dd7-d332-402e-b39c-2dbd00c4512a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 165,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d8aa4954-0aa0-42c0-b0aa-1c6e347a8c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a9172aa3-0e50-4d5b-b05b-2f90f993b1d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 147,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9223d492-8b66-47f8-9ffe-d6454e43fd33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 136,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5fb64fb6-4abc-4e88-9b00-58687485c4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 120,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "eaaa1346-d45e-4df3-be50-7eb2e73db590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 107,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2aba3bcc-31d7-4c4b-a289-204717efed6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 102,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "900f7d5f-551e-4c66-b858-e9b7a8a43b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 95,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6837552b-51d7-467f-95eb-479c8e244079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 88,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b90a28b5-8310-44a0-a056-41966af5a0df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 170,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6846a1e4-9dcc-4f7d-89e7-73f077bbf3a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 77,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "08ddcc84-3191-4e91-ac73-1c591ae49219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 61,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c3e7101a-2bb2-4f0b-9ca5-2b82a84a8a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8d275ad4-07b4-4db9-a116-3d4860d38454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 49,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "98574955-b7cd-4925-9b17-548effd3eb65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 42,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ffd60291-e71a-464e-9a8c-23cdd887ea5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 31,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cd147957-6c42-4d64-857a-80d4b0efe99f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "26ee9858-f723-47f6-b632-796598c2894c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0c1cc729-ef7f-4cca-9637-a3c14cbca67a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "698579ae-5d2c-4158-98c1-7cd4c4dcf32c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2a80728e-f4f0-43a0-aff3-75d45d7a9499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "12fb3cca-05d7-4345-a5bb-5e60fb758e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "505e4fd4-c0a6-47b2-a624-17f9e1c4fbea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "25a1c739-c8cd-4bc6-8537-be09c3ba8c3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "360bef72-301a-48bc-b18d-9b09af1facee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1b94fa08-1029-4f44-897e-0df703440c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fd418e65-6581-402a-ba72-541503e83ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "38ad73bf-c7ad-49f3-a2d2-8bbc7a2cf0bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5595035f-d007-4c58-97be-1a0104c029f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 154,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b2676fe3-3a88-4993-9943-4262b1e43a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 143,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5ecace7c-2f5f-45f9-be80-e51bc7897e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "94e9245d-6e29-454f-abd4-596f0e12ccfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b2e6d782-1917-40dc-ae3a-c37c41195e5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1fe259cb-de61-4d03-9a8b-8073034a2c34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 89,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "32c867d2-a097-4d27-b73a-6761d05c1d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8dde4c45-c3ae-4c86-a9df-aac0efb64bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 64,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "894860ed-59bb-4942-a807-50d149b038c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6b6038a8-3669-4f08-a752-5a96f416d387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4774f15c-f4d0-4e63-9174-317e1a3f72f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 28,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a327f783-2c7c-470a-ad33-2deb02c6bed1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c2d9a3dd-c6ac-4d0d-9970-110ff50ef99f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3c48cf3d-1bd8-4cb7-ae0f-d8fd02b77b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "701ba20c-358a-4466-9370-6e819557681b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 235,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a642a58d-f08c-465d-b08f-5e20feb9866d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 225,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "301a5635-c22a-4a52-99dc-71d17c29e0e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "532f4aca-b6cd-4d9f-9d02-d97a20325c0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 228,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "86f78ac4-a4a7-4d34-a7f4-e886b283c696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 214,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5d1feff9-62de-40dc-bfc5-dc37a6417350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 203,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c95102f9-f055-4aaa-a5cb-6267e2b19bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "add70e05-0e5f-48b7-bc32-4529c1fa677a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "72352508-d1e1-4b62-9295-4b50a495350b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cd1d82b9-a2d1-423c-bef1-d1c8d7a25d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "77bc93f3-5652-41b0-8704-05320ef9147c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2946bd78-56e7-49ee-9aa3-93fc38026119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5c2505b7-79d4-4086-a4b3-f34b06c7aa78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "86afaed2-a232-4af4-8c79-ebae943b23a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "972dda24-7f65-4f11-a1be-037186c81658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d0d95cf3-1836-43e3-82e8-749cfee036af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3695ca77-09b4-4fc5-918c-21193d40ce5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a6d2b905-68cd-43ba-bfbc-6284d42a8dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a1ffa4b4-d77b-4d00-8b75-ef3a902619ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2f65036f-16c9-4682-bd4e-8cc9ca754146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3dad563b-963d-4e74-ab91-5e844afe5215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "21ad01fc-b1b7-4c84-a22a-f53f73cf6196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8ef06660-cdf2-4f34-8326-aa658f93f467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d1afa4ea-3263-4ada-b849-cb6daa83ca47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2ba9967c-0fdb-4afa-855f-b9cc12a3b829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5a530cf9-beb5-4be3-96b9-d7cb81d57620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e99fdbb3-384e-47e6-ab92-ac02d3096b97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0b1d9515-4abb-4211-80d3-5499fdd09cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3061bca5-c36b-4af8-99b2-7efcc2b4f427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "67622b9a-17af-43ca-ba0e-3bd6baa638bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "550bf480-b749-4c6a-ab73-d158e9fae535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 12,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "66a2bda7-a7bb-436a-9d53-95bd4a314835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 187,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f947a7eb-9d6e-4d40-ab0f-a93396de5007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 178,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f8275c56-5b70-4f56-a1f8-d4acf0e9fcc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "55c91cd2-c5b3-43cc-a5f3-035de373eae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 160,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a7441c06-94e1-40a6-87cf-bf2c423de7be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 151,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "33840b78-eb39-4030-b797-cabc21dc7685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 140,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a256748f-9763-4da6-b5e5-7e3af0870c61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8976eee2-f74c-46e6-a639-d68ea206f1a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "057b7681-0248-40dd-8728-cd76c5be5756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 113,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5a1b6370-1349-4953-8984-771536d18145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 193,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5878e25a-acce-43b8-9b48-7ae8d7c17c26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 106,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d25e8877-2022-43e7-82c0-10934736305a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 88,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "90d70543-950a-4c2c-97a4-a3f299c45c18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "be1ca3f2-790f-4ad5-9f6c-8c15f2f0c7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "dbedc58e-9997-4190-8508-52ac9c167a98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "79d534ca-adbe-477f-a756-bc7d21701463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b4d98bcb-9681-4ae0-b4b2-33bdd5f466d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 34,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1dcd166b-b11d-4a11-b319-0ae8c0d2960c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 27,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ac1df411-e91f-463c-b7b1-d43d19f3b978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 23,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "13dca6a0-f6a0-43bc-a34f-a122a0900390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "dccc757f-cefd-4913-872f-8d9eef8a51a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c8349386-1027-4c60-8d66-056f0bb942f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 208,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "4c449bac-c16e-4386-964a-a88a1a357071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "e98193fe-2192-4ccb-bbd2-ef952a32ab94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "9e7a699a-0fc7-44b2-b994-69dcbba18fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "1983eb14-9c5d-47c0-8f5c-deabdc5cb964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "80f8b8c0-3101-463e-9538-d667a7ed70a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "4b1fd078-3b01-4ca2-8005-9290a38dfeda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "9284e38a-8620-4f57-b228-73a7d7bba17a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "90d6d7d2-ee1d-4266-b9ea-4e3563ac544b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "96a7b9b4-83e2-4061-92f3-fcaf996953d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "45f1c6fa-3736-4ccf-a7b2-0351fe12734b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "2656d2ec-dd40-440e-9f31-cd070a748a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "f3b4baa7-be54-4ba0-a53d-f11b40c40efe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "04ccc6b8-9440-4650-9e80-d47a83927413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "98fe00b5-7226-4776-a8d7-6bc464607c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "a33f91db-b3c3-42a0-977a-efad2550090a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "935b7944-24bb-425a-b9c3-9805694ce328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "1ef8573e-daa5-45ad-8042-f0af82bd6118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "f6055045-be4f-4c84-8b09-927734fcb81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "704d9eaf-2bef-4efd-af6d-a9a28ee246c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "61d77888-dd19-4a76-a695-ff4504b49cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "e9a346a4-1a45-4c06-a832-29452ee6ebe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "5d6b4fdf-6dc6-4056-8ce0-65ae6db1d68a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "206854e6-81f4-4f91-b246-a3c5389ceab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "3df433a7-3165-418b-adbf-5df88a62cb83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "ba679ffa-6a9c-416c-ae87-114b8ff4cbda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "fe4ce2db-f4fe-43f5-abb4-3ecf366bd6a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "deaf7e4b-159e-477c-a207-8c8351661d79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "fd1d8f7e-0e54-4096-893a-6753d99cb8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "9d8b0cd2-58aa-493a-848d-463d644a2d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "0969cd70-baa8-4bfc-af85-7c5ac4a639b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "c0879c39-690d-4ad5-a896-2a678314fab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "316738ec-992f-4908-96bd-f15d8d5f060f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "7340c71c-9100-4108-a189-d0c83b64c4f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "264db044-72da-4af7-911c-ee2dfc4dd980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "d67cb225-36c2-4590-a264-46de4ce06dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "f5c924b0-af68-445b-93db-30570350fccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "49819593-6c00-4d85-84c3-cc5e9138cdb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "66d8747e-e51c-4ab6-b761-bf08bd0c1461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "512ec243-a8a5-4128-9687-2370ef108d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "35879a4a-2d2a-498a-83e9-77ef0cd15246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "58daae30-dc44-4a46-b1ed-15bce827173c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "72383b2a-db64-465c-8754-404f356ea579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "69237458-f329-4861-9303-0ebbb902903e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "ad3377db-d9d9-49bc-9693-cef60aa6666d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "91127b2b-eacb-436b-aeb4-50bfcef160a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "26ce5f2b-c01d-412c-be39-cd73b2256878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "0a8aa0ba-d93c-4f54-81fe-29b33bd78b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "63a60c93-1ed0-4ef0-b88a-87a807657591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "ca502f58-196d-4bfe-afcf-41127c644f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "d1fe044f-cb5f-4d07-b27d-6adbae70c019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "e709e534-b1d2-479b-b632-3e5fc1cef852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "3e72401b-ed7e-4081-be09-d956d6b5e607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "758cc569-82c4-436e-af2f-df51976fb1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "f3f298c5-f079-46fb-85a5-c86eaf8dded7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "aed3fbcb-1268-49cc-8a01-598c9c8f4778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "2204f928-c0e9-446c-9332-78cb991c2036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "e565031f-f802-4b93-a1db-0ddfe7064011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "b75836ce-0578-49e8-9002-6c1953b3c801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "51608dd7-2afc-447f-971b-732cbe2f5f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "2e364e24-ab0a-44a7-a7f2-a99b7956fae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "4f5e33f1-3676-4b82-a162-4ee5d098c5bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "41204e42-a304-49e2-8ca2-0e146ef17097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "779ead52-39a8-4990-9c50-2b55c43561b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "ccaa5558-0028-4e7a-9e43-ec28e51f09c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "141e046c-596a-4c20-b176-d8ab77a4977a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "1b879660-aa94-463f-a119-9be3b8c00368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "f9d77440-a000-42aa-bbeb-c6ed89abef37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "64492de1-fc8e-41f3-a833-506803fbb250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "be4ee00d-1fcc-48c6-86b3-aefa7ff6b8dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "5ad118a6-cbfd-4ed1-9cdc-7ef56b8136ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "c5305b51-6c74-424a-a346-4859acfa2412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}