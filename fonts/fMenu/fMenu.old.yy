{
    "id": "be1be7e2-985b-4b7b-bf06-595127fba2c5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3e4aeedd-47ec-4640-876c-93ae85679ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 39,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6b35cc30-f80d-44b9-8ca6-9500b95bf5c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 39,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 339,
                "y": 84
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "09551b11-d098-4144-a1b6-1b698e07419c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 324,
                "y": 84
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0734e566-a559-4e71-935a-171d87c27e3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 39,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 298,
                "y": 84
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "67200a77-bfed-4ae1-9d51-3c4e57e1b1de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 277,
                "y": 84
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4b78308b-9166-4d9f-b54e-dd87f80127db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 39,
                "offset": 1,
                "shift": 38,
                "w": 36,
                "x": 239,
                "y": 84
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3133c802-d9ed-4561-8fc7-3d8f0477a0ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 39,
                "offset": 1,
                "shift": 25,
                "w": 25,
                "x": 212,
                "y": 84
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e9392c31-d116-47d2-833c-8c6459988377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 204,
                "y": 84
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "07178d9c-8ac6-4b83-b8d1-9240c673dbe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 189,
                "y": 84
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b4107596-27f7-4cd3-81e9-aa1cb47d03d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 175,
                "y": 84
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9c4a5dd3-e0f8-452b-86e6-de4131a940fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 348,
                "y": 84
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bdcf7696-776f-4ce7-ac49-e93c49aac013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 39,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 152,
                "y": 84
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6ae0ae68-ada0-44b8-9b47-e28e454f324c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 39,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 120,
                "y": 84
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c477ed19-64e5-43d9-85d3-88614ef62d6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 106,
                "y": 84
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fd7891ac-a62d-4694-9daa-959ec17914cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 39,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 98,
                "y": 84
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "17eeac7b-85df-4eb8-a84a-ea053ea9054f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 81,
                "y": 84
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "87dbdc4b-1e89-442b-b5b2-4adbce27c38d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 60,
                "y": 84
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e02a9cfd-82d8-4e4d-845d-0bc52ef43cd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 39,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 42,
                "y": 84
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "36535f9a-e3fd-4def-8d64-9b0d5e9f34cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 84
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ca116194-8d66-43f5-bee0-1e49bd77ce3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 84
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "89ad4413-5ac8-4bf2-b2ea-b5c8abafc87e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 474,
                "y": 43
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "576bc0aa-78a5-450d-950a-0f646f370717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 132,
                "y": 84
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a6c62919-5346-485d-bed7-eedac7395cf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 366,
                "y": 84
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e2aee0d3-46f0-462b-a13d-060236ca6bc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 387,
                "y": 84
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "49c4e49f-bfa9-4185-8083-7c53f104a902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 407,
                "y": 84
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "12bbd633-70f9-4da5-a3b6-48c5c06b69f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 344,
                "y": 125
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8b1c27a6-7358-473a-81b9-157743cb3255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 39,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 336,
                "y": 125
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a81740f8-98f6-4c50-b7f5-51c91236103f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 324,
                "y": 125
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3da7db05-9a7d-483f-9c30-19e95d70e36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 39,
                "offset": 2,
                "shift": 26,
                "w": 21,
                "x": 301,
                "y": 125
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5b0a9f37-e053-42c5-bca5-752203d858cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 39,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 279,
                "y": 125
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2be1b777-c11e-4e88-8fa3-b0b65cec2fa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 39,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 256,
                "y": 125
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "68608e77-8f5d-447f-9b0b-7616a835b3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 238,
                "y": 125
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1738073e-0349-4e7e-a0f3-01a995de14d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 39,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 209,
                "y": 125
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f82e845c-897f-4792-9ca7-92f25cbbc30f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 39,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 184,
                "y": 125
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b91cc759-0935-4877-81af-e72c275afed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 39,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 162,
                "y": 125
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fec36641-b646-4eab-a4da-b6948303e248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 39,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 140,
                "y": 125
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5d3df2e2-ea17-4777-87e5-d61699e7eda3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 39,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 116,
                "y": 125
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "21639487-af6b-42f4-b582-40e1d52f5a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 97,
                "y": 125
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ad1c1b5a-be58-4fd8-b377-3852d5bcd088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 79,
                "y": 125
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c25a6673-b3ff-4060-943f-d55d453c63f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 39,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 56,
                "y": 125
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a17876a3-1a0a-4360-a15a-0deda35dfc85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 39,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 33,
                "y": 125
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1a5f2d22-fa50-4312-8c94-f547dd01a68c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 125
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "322c250e-3807-4c73-8f80-02f7bf71e266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 125
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c9290f34-2c8c-4e73-a5aa-f4124cda58dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 39,
                "offset": 2,
                "shift": 22,
                "w": 21,
                "x": 474,
                "y": 84
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1033ac4b-4c1e-4bcf-a2f5-35af84efd56f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 39,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 456,
                "y": 84
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5b32928e-6ff8-4b44-8eb9-e7785de6d5b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 39,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 429,
                "y": 84
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ea152ab2-4d25-4c28-99c9-196a741ac01e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 39,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 451,
                "y": 43
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "dfc493d3-ce9b-4732-90d9-5316d91a932d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 39,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 426,
                "y": 43
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d59cdf01-660c-4ac2-a110-ec0f7d0698f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 39,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 405,
                "y": 43
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "eaf54338-b300-4cb8-965c-425faa7c39af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 39,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9f059ed9-ebf3-4072-939d-d94b2d138709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 39,
                "offset": 2,
                "shift": 23,
                "w": 22,
                "x": 415,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "89cb75eb-8bf2-40c7-8c98-48ed58d8d5c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c5ed1ba0-f1ff-481f-bc3a-03530ca7acdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1d9b8736-403d-43a1-ab34-bbce35307e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 39,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 350,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "523130b6-b380-4098-a806-a53d5e83a6d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 39,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 325,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4e47b2ba-58e0-4371-95f5-be423d0ddfde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 39,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 290,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5066c495-5a77-4190-bcc2-8e3a57e11a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 39,
                "offset": -1,
                "shift": 22,
                "w": 24,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ad490eaa-0c42-43f3-be09-ea0c1d33218d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 39,
                "offset": -1,
                "shift": 21,
                "w": 23,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4b8a871f-fbc0-4895-a62c-85762c4403f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cd8ba234-a3cc-4a1f-9f2b-79689324c5f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9164fa3a-7f9f-4792-88f8-da937a477d9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 39,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5159cd40-3935-4d6d-a0bc-8f178d073724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6def2195-20b8-4013-87ea-253418a6d67e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 39,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5bf14c5e-7bff-40b3-b6f7-71fd3310af6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 39,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9f773c3c-aef4-4ea2-b957-50a2c6291f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 39,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f7d020d1-81ac-404b-a42d-615ad8c4678d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "29f2f7ae-dcf2-493c-a2c4-8712eb9dd60f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c6cda51c-224a-4fdf-b696-805675eadab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "dccd4c6f-1e33-4857-bc27-bb51ca29c779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cd0004f5-13ac-4e52-bec2-062c08d26b34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "cd1d248f-211c-4155-a5b8-8b14aa884765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 14,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f0fbb42f-ca9c-45ea-8236-ea460981f28d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "00799c69-2577-4893-b7dc-1c7f9ca9cf94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 186,
                "y": 43
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e9eae83e-5453-4352-94d8-e71803ea8a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 498,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "09b551d4-6c18-4efb-bd52-6b58b0093ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 39,
                "offset": -2,
                "shift": 12,
                "w": 12,
                "x": 373,
                "y": 43
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "253af51f-f56b-4638-8033-f07135aefa22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 18,
                "x": 353,
                "y": 43
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2d57bf09-a2fa-4ba3-8340-99a5f49ea5c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 39,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 345,
                "y": 43
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9892eafb-dcff-4a3e-9415-7dcf8d6fcff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 39,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 316,
                "y": 43
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a33c70ac-2408-4ec5-844f-c702b2a6573e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 297,
                "y": 43
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8bf03cff-66ba-424b-a0a8-7483602ad27a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 276,
                "y": 43
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "eee84149-e0ad-403d-8acc-10a7ab8b2da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 256,
                "y": 43
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dff43ba8-d7aa-48e5-a41a-2af6228992ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 235,
                "y": 43
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6fae43a0-14af-47ea-993a-67db68d54182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 221,
                "y": 43
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "17307c66-33d2-4b10-bd37-63010f7564a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 387,
                "y": 43
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f9962c86-962e-43a8-87c5-ce69498d25db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 205,
                "y": 43
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d1f6de66-f126-43ba-805a-b863759babc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 166,
                "y": 43
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7eaac760-cc2c-4ef8-af32-174d73ad98a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 145,
                "y": 43
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c820bcb4-6dc5-48f8-b2ce-f7eaca1d1a63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 39,
                "offset": 0,
                "shift": 28,
                "w": 29,
                "x": 114,
                "y": 43
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "894ca7cb-e2b0-42e3-9670-a9dfdb0917d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 39,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 91,
                "y": 43
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0838cf8e-16fe-4cd9-acfc-053462cae84b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 39,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 69,
                "y": 43
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2761ccdd-757f-4cfe-b1e8-2453a25f5df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 50,
                "y": 43
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "791712d7-0259-415a-adf6-d0109bf01743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 30,
                "y": 43
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e99b1aa3-d96b-4101-b297-c36b4ec7263e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 39,
                "offset": 7,
                "shift": 20,
                "w": 6,
                "x": 22,
                "y": 43
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "462d8258-fc0c-4b70-90f3-85f59180f673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8881e98f-2758-4fa0-81dc-12b2d63f2910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 39,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 364,
                "y": 125
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "21624479-69b2-4f61-9dbc-2a5ac6fde451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 39,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 389,
                "y": 125
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}