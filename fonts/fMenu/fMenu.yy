{
    "id": "be1be7e2-985b-4b7b-bf06-595127fba2c5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8c2e2569-9b01-467d-8da1-906a00b3a34b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 39,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2856e5f2-98c7-428f-9298-c82d6f8b3093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 39,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 339,
                "y": 84
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5aa1f874-1260-4f14-98e0-0064645282fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 39,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 324,
                "y": 84
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ac0221f9-9189-4adb-bc0a-0c4289f1b203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 39,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 298,
                "y": 84
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "bd8426f2-5b05-4b05-af19-83cf50fabe9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 277,
                "y": 84
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f65b1da7-7529-403f-87db-b6907554630b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 39,
                "offset": 1,
                "shift": 38,
                "w": 36,
                "x": 239,
                "y": 84
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fb7d1b20-e848-4e45-ac1a-6d1900d84d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 39,
                "offset": 1,
                "shift": 25,
                "w": 25,
                "x": 212,
                "y": 84
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2be0d568-36d7-4f9c-99a8-1c9ffe2f2538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 204,
                "y": 84
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "fe08f14b-bf51-4591-9e18-788ce34d04d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 189,
                "y": 84
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "06fffbd1-733c-432b-9498-7275c8240b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 175,
                "y": 84
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4eb886fb-a822-4aa8-a7b5-5ce8cb3ea622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 348,
                "y": 84
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3dc37846-585d-4078-b71e-a73a9bdc3c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 39,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 152,
                "y": 84
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c607769e-2f47-4292-92bc-7467f9061a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 39,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 120,
                "y": 84
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e1f481ed-f19f-4c92-8723-405bd7141129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 106,
                "y": 84
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c1ac18e2-884e-40b5-af5b-b8624f0b438b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 39,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 98,
                "y": 84
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "91c62120-ef1c-4695-9b79-48c22c6f1d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 81,
                "y": 84
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ae64d40e-eab6-40a5-a5c0-8fba2f7bc7cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 60,
                "y": 84
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "89769029-e9a5-4056-bf5d-9bd42284f6a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 39,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 42,
                "y": 84
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "041f4f63-d6d5-45d5-afd1-eac450fea963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 84
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "36a29c19-9693-42c8-9f0a-952b6a3fccca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 84
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cf8b68a3-e522-4668-8a6a-3ebd7ff3c134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 474,
                "y": 43
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d9b5d74f-0b07-43af-8d67-5ba4c00767a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 132,
                "y": 84
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1c213dbd-4918-450a-9aa8-54f075a80fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 366,
                "y": 84
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "81fe8533-0cdb-4b0e-b888-393549ebc2dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 387,
                "y": 84
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "25cab8b3-8e91-409d-bd53-66d297e5e353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 407,
                "y": 84
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "fb71f5ce-c03a-496f-a3fb-6507393cee64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 344,
                "y": 125
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c32acbdb-10dc-4c09-9084-8a2ac0004d2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 39,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 336,
                "y": 125
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6ac7d436-4271-4e5c-aa38-66118322ab77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 324,
                "y": 125
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d4e53a0b-efe3-40cb-9341-91482d859797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 39,
                "offset": 2,
                "shift": 26,
                "w": 21,
                "x": 301,
                "y": 125
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c5b29b0c-08c5-455e-b437-9e31552a14cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 39,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 279,
                "y": 125
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "871b7bc0-6607-475c-ba85-64fd613d8197",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 39,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 256,
                "y": 125
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c1fd94bd-59cd-4e86-82e8-ef717443a781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 238,
                "y": 125
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e4d976fc-0767-4779-9551-9350735b4509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 39,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 209,
                "y": 125
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "46bbc0ff-32ea-4174-8b81-eb24e4afeed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 39,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 184,
                "y": 125
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ae0db576-cebe-410b-acad-3a251f1e5e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 39,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 162,
                "y": 125
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "26c33696-e915-4faa-a4df-d285d2f4ca05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 39,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 140,
                "y": 125
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0aadf3d3-c3b7-4b67-95eb-022b67101d62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 39,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 116,
                "y": 125
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "23a85626-2d92-4c33-bb39-f753911c194d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 97,
                "y": 125
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "15e58a8a-7ce5-47d0-8fbd-c7830624ab7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 79,
                "y": 125
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fc51ed7c-80d2-44f0-9952-6d630b72fc42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 39,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 56,
                "y": 125
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "db5d4b18-acba-41ef-a080-e2c3c26756f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 39,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 33,
                "y": 125
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "bd4da947-1b6d-4242-bf11-40e7bfbc4b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 125
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "35f54b2e-263a-41d0-86e2-65bef5f87d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 125
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "80cdd6b6-0fe5-49e4-959b-4a84d979a38f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 39,
                "offset": 2,
                "shift": 22,
                "w": 21,
                "x": 474,
                "y": 84
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4890785d-1b55-4918-bab5-0e985ce7407d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 39,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 456,
                "y": 84
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5324d90e-83d1-426e-ae0d-5a1f7fa9dbbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 39,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 429,
                "y": 84
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e0e46c25-8564-4e9d-973b-040015968d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 39,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 451,
                "y": 43
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f7dd259d-370c-437b-b342-c9ae888d99e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 39,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 426,
                "y": 43
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6fe40b72-ed6f-4806-bf4d-18ebbaeff736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 39,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 405,
                "y": 43
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "70d46996-5284-43c8-aec6-00b3433d62bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 39,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7bb57f2a-42fc-467b-aa6b-f9df934dc6d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 39,
                "offset": 2,
                "shift": 23,
                "w": 22,
                "x": 415,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "7565201c-ed18-487a-9089-b85d4d674018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "67059bb6-b7ca-4186-a88f-de5bc5550dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c068146e-0a44-4796-a3f4-9570ffdffdbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 39,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 350,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2313223a-065d-4c87-aaf4-6fb161e70f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 39,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 325,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "90b587f6-ba14-4332-9cb2-044c844fa293",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 39,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 290,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "df4b19ee-7a97-438e-8f11-c2e08419686e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 39,
                "offset": -1,
                "shift": 22,
                "w": 24,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cafbf09f-8cab-4064-b2b6-44073f51a709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 39,
                "offset": -1,
                "shift": 21,
                "w": 23,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "caa35417-5eb2-47db-8a92-163d43366b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "80361e46-7416-4f79-8ddd-8bfdd8efe7ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c170afe0-10e6-4a10-8025-6d7723f738ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 39,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1716d4d4-1397-47f9-9b08-038a1fb2a80d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7bbe2e2e-5d17-4b30-a789-73da0d4b6ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 39,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5d3fa783-bf4b-49f9-9bf2-c96237899c05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 39,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "923df6bc-f84e-47af-a900-3a8d75148255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 39,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "89e5aeac-b0c9-4ec4-a95e-7dc5d5eb6add",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "68924611-3a0b-403c-8217-e602183dc2e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "dc6699b4-8392-4d26-9306-bca99b4bed45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4b29d535-41aa-4934-a33c-170853d2ee74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0bd1973f-bfa4-414c-b131-aa86323620ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d67466ba-8aca-4875-9a2c-6caabe77a410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 14,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "26cfeef7-65db-49a3-807d-c9e0fde6401e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "311c0228-1645-47d7-a54c-dd08744291df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 186,
                "y": 43
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "35dd3721-8d9c-4c68-9725-179a070c5d22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 498,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d89a6f75-2111-48cf-8886-323c62d24183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 39,
                "offset": -2,
                "shift": 12,
                "w": 12,
                "x": 373,
                "y": 43
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e4c3a320-3bf6-4468-b36f-2ffd0d17ac59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 18,
                "x": 353,
                "y": 43
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "92a408bb-ac71-4664-a207-ae1889193d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 39,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 345,
                "y": 43
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4c509589-1d0c-4615-9bfd-fc12745d6757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 39,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 316,
                "y": 43
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5797ff02-48f3-477a-a226-2be0f756e9f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 297,
                "y": 43
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f60d01e1-a361-4a0d-8d2c-88c6c000b467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 276,
                "y": 43
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "97daa910-0c03-4733-ba73-0e21649ba913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 256,
                "y": 43
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d2be3361-04dd-49ca-9568-a30b6a77ab8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 235,
                "y": 43
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e63222f4-d1f8-4f77-b307-422c9be74c60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 39,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 221,
                "y": 43
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a083fe7f-4dbd-4749-9ec8-7571a1dae2c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 387,
                "y": 43
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5401387f-f4c1-41b2-85eb-e2be406c8abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 205,
                "y": 43
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "aac405d1-e907-4bb2-a145-02b73e679b4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 39,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 166,
                "y": 43
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "52f939be-d6ab-400a-84e8-e916679f91cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 145,
                "y": 43
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "13349c62-3f9c-41fb-82d5-8fc0624716d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 39,
                "offset": 0,
                "shift": 28,
                "w": 29,
                "x": 114,
                "y": 43
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "921d0f57-e58d-449c-aef9-498b2a51ef70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 39,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 91,
                "y": 43
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d5cdd022-5555-46ce-bcad-f8c86ca23bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 39,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 69,
                "y": 43
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "289aa337-04b2-413f-af5f-856c19548c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 50,
                "y": 43
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "aa5c6ef8-a160-4d5a-a01f-a36a88e7069a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 30,
                "y": 43
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "5c60ef66-a101-4b49-8393-0aae4d845f51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 39,
                "offset": 7,
                "shift": 20,
                "w": 6,
                "x": 22,
                "y": 43
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f1e9ecc1-c259-44ea-bc22-7b3dbb293b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "05c66664-6bf4-4f24-8689-07206d4041ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 39,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 364,
                "y": 125
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1dbec633-a93d-4db9-9a6f-d08199246b6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 39,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 389,
                "y": 125
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}