{
    "id": "5a627af8-46d6-49cc-91e4-608622154ffd",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fText",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Open Sans",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "de7ddc97-07ae-45e4-99c0-9682165c5114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 44,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9558c854-f6f9-4215-bbb4-698b711ace03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 44,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 293,
                "y": 94
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "06f09979-50c0-43f4-a905-ef059a60668b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 44,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 277,
                "y": 94
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2732c946-cf43-4e07-844e-a37ed3cb0f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 44,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 254,
                "y": 94
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "10053fc5-abf4-4dba-a475-2ae28382cc78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 235,
                "y": 94
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1b4768b2-058c-4053-bc7f-d4e9e815edae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 44,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 203,
                "y": 94
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7aa8ccf0-c919-4efe-a0c7-14e58637158d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 44,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 176,
                "y": 94
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0ec84f6e-796c-4f1e-ba3e-70921a7610a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 44,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 167,
                "y": 94
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9e3530aa-c915-4753-adfe-a59daa5cd395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 44,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 155,
                "y": 94
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "eba5644c-e6db-4477-973d-ef09e7de850d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 44,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 142,
                "y": 94
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "31c06c85-3944-4c11-969f-4eb62c560da6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 44,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 303,
                "y": 94
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bffa9704-39e5-4933-ad0d-9931fdf7eb20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 44,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 124,
                "y": 94
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fd9f50a2-6a14-4eff-bd88-09689beb26a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 44,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 95,
                "y": 94
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "86f58f75-ecc1-4c9f-8f56-f82636b4d563",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 44,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 83,
                "y": 94
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "10d13918-69a4-473a-8381-823e36e710eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 44,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 74,
                "y": 94
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "13879275-2553-4694-8eb7-98622e80120a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 44,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 57,
                "y": 94
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6e817644-b61a-43fc-aa53-3b3c6cc4446b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 38,
                "y": 94
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "958a9e22-1791-4ae1-9a60-d761b2fbcac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 14,
                "x": 22,
                "y": 94
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d87b9b55-d27d-45ba-a7ba-61f968d259de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 44,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "399a72c2-ea93-4a98-af81-a2bb353a32fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 474,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3b356fa6-83c3-4629-a2fd-d86b7e36530a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 44,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 453,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "65c4a451-971e-44aa-b8c6-82dea4ad0f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 105,
                "y": 94
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a898e699-d1dc-44fb-946a-7ed3d9224fa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 322,
                "y": 94
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "42eea0d9-4d12-4120-bf96-0957de1bf189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 341,
                "y": 94
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0d97e7da-5019-4035-b1a4-1e7efce7d489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 44,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 360,
                "y": 94
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c364905c-cbc1-4dc6-81b5-aab557ba91d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 44,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 268,
                "y": 140
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c06c6b5d-21d2-41fc-adb6-d5e2fa497dcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 44,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 259,
                "y": 140
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f88ff752-4f9f-4fe7-9632-6de4399cf990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 44,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 249,
                "y": 140
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "aa8cd2bb-3965-40ad-9af8-a69695e6191e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 44,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 231,
                "y": 140
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "42befd69-64e9-485a-8a82-a3c3b2ac658c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 44,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 213,
                "y": 140
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3fd88986-6fe2-424a-aeb5-203f0865006f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 44,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 195,
                "y": 140
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7f8d8bd7-61ff-4662-a893-7ed3808fc0b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 44,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 177,
                "y": 140
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "cb76427d-4c3f-4af3-bf45-08befb60932b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 44,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 148,
                "y": 140
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "65f107a0-ce08-4b83-a0f3-539fb84c5e1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 44,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 122,
                "y": 140
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2591515c-144f-45e4-8d85-3f1af605cc26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 44,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 101,
                "y": 140
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1c4ee1c1-4455-42bd-a4ff-9597f0d39add",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 44,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 80,
                "y": 140
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "91218596-6392-45c9-bc0b-03a514994b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 44,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 58,
                "y": 140
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9940e784-56e1-4f50-8149-fd636608dc54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 44,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 41,
                "y": 140
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b90aa904-3677-4294-8446-f13ed31e2e5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 44,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 25,
                "y": 140
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2520fb56-6f36-4407-a905-57b44f467db9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 44,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 2,
                "y": 140
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8ab56586-1a06-4d13-bffb-484b0d647b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 44,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 472,
                "y": 94
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "713ce52a-bc0b-441a-9428-ce3a45e233ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 44,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 463,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "22275224-7c86-43d8-961f-d20e33df5e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 44,
                "offset": -3,
                "shift": 11,
                "w": 12,
                "x": 449,
                "y": 94
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9b4421c3-d733-4c56-b30e-0b2c7e5d7a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 44,
                "offset": 2,
                "shift": 22,
                "w": 21,
                "x": 426,
                "y": 94
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "659bd73c-5141-4257-b87b-a54962e74274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 44,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 408,
                "y": 94
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c2539991-2bbd-488c-b5fb-8553a7d8ca3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 44,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 379,
                "y": 94
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5c4ee839-1106-4ce4-a368-048bb41ebf77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 44,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 428,
                "y": 48
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4fb177e5-9039-4a09-a9c7-e9fd3a030efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 44,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 403,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "23769a2b-7f3f-4923-b02d-47eaa5712bac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 44,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 383,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "30bc3fdd-8c2a-47a2-a6fa-60235b8e3bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 44,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6998079d-af73-46ae-b50e-c2cbd885d1d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 44,
                "offset": 2,
                "shift": 22,
                "w": 21,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fa59ff8a-6d35-4758-ad90-b4f7c99e0779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 44,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 382,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1e1fd0ec-6573-4d7f-85ce-851aa684716a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 44,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "224f2273-38d6-4efc-8de1-7a8009c5c2ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 44,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 339,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ca88c1ee-69b1-4d33-9c04-01060a042713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 44,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ce391905-f4bd-4ac0-9571-896af68a1988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 44,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 279,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ee707212-84f2-48df-ab42-2a0984da23e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 44,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 253,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4c51e50b-be68-4c12-bd35-41f08b25a742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 44,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c57665c6-f76c-488c-a217-c4705aa48494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 44,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ad0afdfb-8ef2-4ae6-8101-05454248e926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 44,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6b9ace7d-aa7d-4a70-b698-a66193d5ef6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 44,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1797c2ed-a7c0-4a12-b1c5-eefe1687fd24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 44,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c7ced4f2-1503-46f2-beae-433677eda814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 44,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "60e4640d-b71e-4d84-9505-38374f8114f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 44,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5019434c-167e-4c07-a056-90d35833e494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 44,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "431c5b1c-7ca3-4b51-9ef9-16560418fc6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 44,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f29f776e-0d76-4441-8cd5-50e85ac1b320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 44,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "42f3f5eb-e3f6-4ce6-9e3f-63dcfc962798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 44,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "63dfc173-bfa8-4c25-b95f-07eeebc511cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 44,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "819184a3-46f6-4d7e-86a3-1e56d6c88e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 44,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5ed57daf-0551-44e4-aa70-c36266c23aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 44,
                "offset": 0,
                "shift": 13,
                "w": 15,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "528c4cf0-7a4f-44b8-b0a0-5c7e953e0c52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 44,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "37c73cbb-dd1a-4901-a887-e17038b43cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 44,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 160,
                "y": 48
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f9e7565b-44a8-4ed5-be29-ac8c5f13ecd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 44,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 482,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ec1afa50-eed9-4696-94cd-3993aac54a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 44,
                "offset": -2,
                "shift": 10,
                "w": 11,
                "x": 353,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "40d05894-8f51-43bc-b3ce-0d1cba2b57fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 44,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 331,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "16bc414c-63fa-4a37-b83a-a583c2af49d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 44,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 322,
                "y": 48
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "45f5c8c3-e796-4f93-9969-2a145e465496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 44,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 292,
                "y": 48
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7a60370a-802e-41fe-868e-a8232d28d17f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 44,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 272,
                "y": 48
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c3dfcaea-14b8-4b17-b1e7-9be5b45dbf6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 44,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 251,
                "y": 48
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "276aabab-1683-46be-8832-1fed62cb544a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 44,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 231,
                "y": 48
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "02493edb-549a-451f-af17-d68caa1672e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 44,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 211,
                "y": 48
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "dd78cede-2b6a-40cb-bb92-b921157f29ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 44,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 196,
                "y": 48
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "683b9640-d314-40d9-8195-8da9f20dc166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 44,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 366,
                "y": 48
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0e0be6b6-d06b-437b-b001-69c216381698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 44,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 180,
                "y": 48
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4d016dfd-4b25-4053-b3af-6e2cef36821d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 44,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 140,
                "y": 48
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3df1e2dc-f908-4140-ac40-b097f24a3e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 44,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 118,
                "y": 48
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6c51afef-46f6-4468-afe2-387011d50b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 44,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 87,
                "y": 48
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0df37e3f-b482-4162-b13d-a163c94539dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 44,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 65,
                "y": 48
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cbd26f48-aad1-4449-8f0f-6836525b7b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 44,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 42,
                "y": 48
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "869bb272-a37a-451d-95a6-ccc2d181ab50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 44,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 24,
                "y": 48
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "642fa02a-c581-4f59-baed-78280bb5827b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 44,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 9,
                "y": 48
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "43e1b0a1-24eb-4f8a-9a5b-5d8fa34b85a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 44,
                "offset": 6,
                "shift": 16,
                "w": 5,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b2fe23d9-5121-4dbe-b72c-85783ad72951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 44,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 492,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5b3ebe75-a479-41b4-8f2d-8e5ee7be85ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 44,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 288,
                "y": 140
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "06fa9bd1-f78e-4d92-9c6b-a21ba6d4a21c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 44,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 306,
                "y": 140
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "4d56065e-7a87-45f1-9571-3c8714d57f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 65
        },
        {
            "id": "9113a2d3-4ac7-44bf-ae10-07db3572932e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 84
        },
        {
            "id": "3fe4d311-8299-4c35-95e6-565b56470a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 86
        },
        {
            "id": "7fd519b3-d0e0-4972-9180-39999c445e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 87
        },
        {
            "id": "91e9241b-febd-420d-9f08-028f8a83c19a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 97
        },
        {
            "id": "db4c1929-c453-4bb6-b100-fad406de9717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 99
        },
        {
            "id": "08685d4d-6f3d-4ca4-b06e-899702fa832d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 100
        },
        {
            "id": "3579a688-7ad3-4236-956e-8712c6b39476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 101
        },
        {
            "id": "87a2413a-9c83-4e2a-9b5f-e8bf9c2420ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "00475858-e41a-466f-b5c0-9c123470d4b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 109
        },
        {
            "id": "97aa974f-6d90-4efe-9c15-040177ca3d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 110
        },
        {
            "id": "50b986f8-3354-4c5d-8c75-8bbbf79338f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 111
        },
        {
            "id": "c62c4179-3b28-4df6-ab01-6cc0f5234d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 112
        },
        {
            "id": "6634214d-b84c-42df-a739-d888e8580866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 113
        },
        {
            "id": "e58ae768-1f70-462a-a211-f7d320d93c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 114
        },
        {
            "id": "ecf2a06d-1278-4223-9620-b9c16b8d5b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "f496bf0a-51e8-4e7b-a8cf-4edb525ab41a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 117
        },
        {
            "id": "a7e42027-b586-473f-a1c3-185cec3c96f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 192
        },
        {
            "id": "f5f3b1a8-0a58-4ba4-8c1b-69f30a3aa2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 193
        },
        {
            "id": "2941e2ec-4792-4c9d-b113-4dcc6346bc74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 194
        },
        {
            "id": "27371cc0-e865-4283-beda-850b850d0ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 195
        },
        {
            "id": "8e6c6aa3-2ee6-44fd-ae06-b7c5e02e467e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 196
        },
        {
            "id": "953db0be-8b8e-460d-9a01-fa457b8572df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 197
        },
        {
            "id": "44c7320d-b632-41a2-a310-c80990ef271a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 224
        },
        {
            "id": "d91eb745-0493-4732-952b-e6c221e3d36e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 225
        },
        {
            "id": "adc79628-d8c4-4b65-99f8-0d254a6c69c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 226
        },
        {
            "id": "fa04e550-7224-42fa-8599-ed95aab89a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 227
        },
        {
            "id": "af6a1163-ec24-42f2-a744-1cf3603498c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 228
        },
        {
            "id": "dd5a09dc-4ce7-416c-8467-853e305f5a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 229
        },
        {
            "id": "c3307392-c228-4cb2-aa58-e1e6a8344af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 230
        },
        {
            "id": "564ebc06-f238-4cbd-9043-4b9332192340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 231
        },
        {
            "id": "8e17472a-e4c0-4654-8c34-7fa361a8d118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 232
        },
        {
            "id": "cf2dd27d-cec8-41a9-95e7-2fd6e78bfa78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 233
        },
        {
            "id": "5b998598-7530-4084-8835-716a8b7c3437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 234
        },
        {
            "id": "676c7749-d738-4735-9923-46304da45bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 235
        },
        {
            "id": "22f0d963-025c-47c6-b0c0-2414f93ac7ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 242
        },
        {
            "id": "f4f22932-0f9d-4cc7-9d13-b6b7df86ef6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 243
        },
        {
            "id": "21e9a8a1-cb0a-4a71-b6b9-9acbfe1812ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 244
        },
        {
            "id": "98cb8537-804f-4bd4-aaa7-09a1dddafa77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 245
        },
        {
            "id": "87ff7df5-38e3-46d9-89fe-d076a5f5d808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 246
        },
        {
            "id": "f83595a3-68fa-4848-9e95-55c250a65041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 248
        },
        {
            "id": "fcdc1c6f-11f8-4268-ab67-7aee022633e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 249
        },
        {
            "id": "995935d5-74e8-4bd7-884a-36b85b945f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 250
        },
        {
            "id": "f5494808-e989-49df-b0c0-0ee0c1cd998b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 251
        },
        {
            "id": "a24ded16-847a-4059-a7a5-cf47271348fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 252
        },
        {
            "id": "99c0f8cb-282d-4b14-8ae6-42701b89e01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 256
        },
        {
            "id": "cae87673-8c46-4be5-9a7b-f5cd6ee3b47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 257
        },
        {
            "id": "bb94f4e9-0449-49e3-b0a3-1d6899e597a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 258
        },
        {
            "id": "ecc231f6-06c1-4fc7-8d1d-af1ff30c7be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 259
        },
        {
            "id": "3ca3b631-1ce2-4566-ba8d-fcc9d486a1b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 260
        },
        {
            "id": "a50e0ee3-9b7d-4d8b-a9d6-5c5570ae5ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 261
        },
        {
            "id": "d90e8935-695f-4b6f-8fb5-e0532a6e0ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 263
        },
        {
            "id": "037468e8-cc02-4d42-ae8c-a5db92befa3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 265
        },
        {
            "id": "dfaf9595-8ced-4d90-8c2b-1ecf7e31df46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 267
        },
        {
            "id": "749b7da3-4955-4deb-8816-19e2ab03ca3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 269
        },
        {
            "id": "262a5f56-7223-4d78-a369-599f5946af38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 271
        },
        {
            "id": "0c16fac4-faf5-4a94-ad16-0a95373c1ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 273
        },
        {
            "id": "dde0f14f-bc5b-4767-a67a-786216c50f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 275
        },
        {
            "id": "1db6e4e4-03d9-4586-ab83-2f191ee0e576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 277
        },
        {
            "id": "15743424-b911-4841-b3da-429c8b651418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 279
        },
        {
            "id": "e1144a09-15b5-4b55-9f62-16611dac4747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 281
        },
        {
            "id": "28737e81-6cbf-46f6-a2b5-b4255db58137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 283
        },
        {
            "id": "b7190f0c-740f-4d9d-8cdd-ca7ee3bb2fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 285
        },
        {
            "id": "8510d33f-62a2-499b-b927-8b8dce8abc6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 287
        },
        {
            "id": "9c54ea0d-cbc6-400f-8105-279d68885656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 289
        },
        {
            "id": "e4f6b8c4-2c71-465f-82d9-2d22dccfe313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 291
        },
        {
            "id": "aa0af77f-09ce-408b-8a42-62c834c03db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 312
        },
        {
            "id": "44d84cad-012b-49f7-b103-ae8720bb36e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 324
        },
        {
            "id": "7e7dc053-afe5-43a8-b20b-c621f6335929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 326
        },
        {
            "id": "4c15f0e2-4f2a-4235-bde4-0e60be002e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 331
        },
        {
            "id": "cc936cfb-918c-4caf-bbf5-757198f5f413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 333
        },
        {
            "id": "2fe712bf-0492-4327-966a-a04d78edb612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 335
        },
        {
            "id": "8023f40a-9b76-4339-9e33-a98619742ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 337
        },
        {
            "id": "4d095642-d73c-4c45-8855-9109d7f4b986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 339
        },
        {
            "id": "1143a777-ecd7-4c89-98a4-ce16aa7ed1ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 341
        },
        {
            "id": "06b0bbcd-1115-413c-8ebb-3abc8ca2f457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 343
        },
        {
            "id": "90a9743e-b9ed-4624-9507-6bd81acea16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 347
        },
        {
            "id": "dd4be595-70c2-49f2-a6b4-60afd46c3fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 351
        },
        {
            "id": "6d94c583-4cac-4a6a-8496-1de43565d593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 354
        },
        {
            "id": "1b3b79de-0602-43e7-b7ea-2b080445f930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 356
        },
        {
            "id": "b5eaa7e2-53a8-4d12-b779-04e380295223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 361
        },
        {
            "id": "0b90c08a-f2a2-4329-a552-463a53c3a226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 363
        },
        {
            "id": "5187105b-6c18-4ae4-ad98-65ee5a0cfd6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 365
        },
        {
            "id": "e05ffc14-fd7a-4a72-a81b-4edbce844eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 367
        },
        {
            "id": "5bd50fd0-68c7-4d5c-a19a-9d08cd550ac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 369
        },
        {
            "id": "7dd7b94e-ed68-4f3b-8bc8-2975643cd009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 371
        },
        {
            "id": "b0dea503-b3ed-49a5-8f76-aea389d86510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 372
        },
        {
            "id": "738c6fc1-5241-44de-b93c-d99a5829f705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 417
        },
        {
            "id": "5fe9710e-9632-49f4-ac7f-35d49533cfec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 432
        },
        {
            "id": "0b070d32-3c17-4997-8195-988ce47428a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 506
        },
        {
            "id": "c1e9ef00-e5a5-4616-8495-075d7879e526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 507
        },
        {
            "id": "38ac43fb-0807-48f3-96ec-715cd0abb2cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 509
        },
        {
            "id": "3ed716b4-36d0-4c5a-b0c8-c920b6d9ae8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 511
        },
        {
            "id": "e97d4ad8-afee-4ba5-b7a4-1ad3985c600e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 537
        },
        {
            "id": "ad1f259e-caef-46de-a7f3-d51f1bb1abd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 538
        },
        {
            "id": "6bc9bf3a-da2c-4789-86ce-db8b7bd4d83d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 902
        },
        {
            "id": "85927744-7cea-4fb4-b138-b5a4160da4ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 913
        },
        {
            "id": "1aeeacd8-fa31-4860-a953-f5788f4fe3fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 916
        },
        {
            "id": "f9d42ab8-9bca-4f6a-8b8f-850a2a3afec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 923
        },
        {
            "id": "ea815f2f-30e3-4dfb-b3e3-4eab96d30671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 940
        },
        {
            "id": "c47ea195-4288-4566-9932-ff00960823d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 941
        },
        {
            "id": "65f50de1-9ece-4c71-8c43-79ed0247c2fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 942
        },
        {
            "id": "82f03782-4f8c-41bf-a5ee-72d3bb57821b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 945
        },
        {
            "id": "65bfe920-575e-487f-be59-f3372a3f7676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 948
        },
        {
            "id": "94dc9f02-63b7-46d1-8064-53e3a4a0990d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 949
        },
        {
            "id": "4c3a7ba2-cc75-46a1-9b3d-46a5e4994864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 950
        },
        {
            "id": "37280fe6-189f-4a6b-bb07-5303c085f01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 951
        },
        {
            "id": "8def8b18-58c7-489f-a597-f87aa4b4041d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 954
        },
        {
            "id": "d4583775-8341-4630-bc0b-0ef38d886bfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 956
        },
        {
            "id": "071444b4-e82a-46cd-8f89-4abcbd376fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 959
        },
        {
            "id": "58078d7f-8bd8-49c5-8afc-0523d9741aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 961
        },
        {
            "id": "954f2ed2-eddd-4a70-a355-57cd5212da83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 962
        },
        {
            "id": "cefd3e65-28b1-42f3-b7b4-9ab56a9ef70b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 963
        },
        {
            "id": "29f4bfab-6d8f-4213-ac5f-95a79a5e2521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 966
        },
        {
            "id": "5d5eaa2a-ed9c-45c2-8d6a-25d561da4cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 972
        },
        {
            "id": "9138eeea-af17-4e9b-b2e0-a341e7699ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1033
        },
        {
            "id": "74760659-3130-4d01-b743-ff1a426f0477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1040
        },
        {
            "id": "4bd84e5b-32f6-403f-9270-1540dacf6a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1044
        },
        {
            "id": "5358db87-ba32-42e8-87fa-7dd8e5543d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1051
        },
        {
            "id": "ef6176b9-0546-4b8f-9b93-70de482574c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1072
        },
        {
            "id": "f1044944-091f-474b-8ab3-c617ecc188fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1076
        },
        {
            "id": "eccbbbdd-5eea-4d56-93eb-21d3d6681785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1077
        },
        {
            "id": "ccfe4320-bc39-40b2-bab7-27c05d6d7b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1083
        },
        {
            "id": "24d1d0a0-391a-4de0-b3ec-abf8f85cfcc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1086
        },
        {
            "id": "f161dfe1-9574-4130-a0f5-fbe7dd8aa2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1089
        },
        {
            "id": "54955fce-e22e-4254-845e-d9569fac62c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1092
        },
        {
            "id": "80e06dc1-3fa4-4e33-be81-f6629cc2c1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1104
        },
        {
            "id": "1e6c63ea-8d25-46aa-819b-5080571ea56e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1105
        },
        {
            "id": "b0e63f13-d626-4a2e-a868-e55ecd0e8647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1108
        },
        {
            "id": "dbdad223-f718-431b-92e5-ce0e9372eafb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1109
        },
        {
            "id": "498eddc3-770a-402e-b0c3-84a2ecb694cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1113
        },
        {
            "id": "b3d292e0-f879-4929-a45d-c33e2758d71f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1126
        },
        {
            "id": "8da32f99-3e27-44fc-8ef7-1f3e8f2bab1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1127
        },
        {
            "id": "ce9fc8c2-689d-4949-921e-4f729121b037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1139
        },
        {
            "id": "ab271be5-093c-4916-8184-5f457fc25398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1145
        },
        {
            "id": "a3ebcdca-467a-4ae9-b07a-e0702fcf537b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1147
        },
        {
            "id": "ae1d77f4-e695-4be2-b0c7-c6407da5ae4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1149
        },
        {
            "id": "bcae8b6f-714c-408f-a7a4-a1f62d937341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1153
        },
        {
            "id": "ba3a2add-666b-400b-93e4-a5ac62515bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1193
        },
        {
            "id": "1b6cf23e-3b11-4826-ac82-2bb7422bae13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1195
        },
        {
            "id": "cbf39d60-a5ee-47e0-b801-3d79918479d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1221
        },
        {
            "id": "8dd39e33-79a7-4827-8322-85d89bf18318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1222
        },
        {
            "id": "efc52c5c-0a26-4bc7-9902-c09250ec777a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1232
        },
        {
            "id": "e7b2d54b-265c-41e0-b631-9a29ad009daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1233
        },
        {
            "id": "ecdd5407-82eb-4ba7-a573-7332415e2b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1234
        },
        {
            "id": "055f4644-6064-4799-a0ce-afd7b1d6767b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1235
        },
        {
            "id": "a1971374-a08f-42e7-9919-03fee6da5f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1236
        },
        {
            "id": "1e8a200a-beda-435c-b7d6-4ee79bc64af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1237
        },
        {
            "id": "9742bf43-83be-4e68-8b62-e6135e446553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1239
        },
        {
            "id": "28f07d49-13ae-4fd2-aa78-cfa6d2617c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1241
        },
        {
            "id": "5c998b92-cc7e-4fbd-8a11-56dbbf257bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1243
        },
        {
            "id": "74e39ff1-6677-464f-8a95-b09d587d2dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1255
        },
        {
            "id": "edf16825-d622-4d68-b624-77192a0e40f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1257
        },
        {
            "id": "a383205f-5053-43f8-af56-8823a0500e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1259
        },
        {
            "id": "5eb2efd3-ad37-4c7e-9ff6-569626d36ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1280
        },
        {
            "id": "93115d0c-869c-4eaf-9ce2-0771c0787ef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1281
        },
        {
            "id": "7244b85c-5c79-4aa1-928c-914a375232c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1282
        },
        {
            "id": "f6dddb7e-e778-4378-b210-46559b01291c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1283
        },
        {
            "id": "e71a4bc6-7b5f-4f96-854f-320580b17b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1288
        },
        {
            "id": "00caabf3-8d8f-4dee-abf6-a056c69926d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1289
        },
        {
            "id": "7c4e36f1-7e41-4375-bd06-8497775e0bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1293
        },
        {
            "id": "95ec1c54-9ef9-42e2-a3fb-d6972765229c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1297
        },
        {
            "id": "4cdaf77e-0da5-4455-8880-11f912a86f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1298
        },
        {
            "id": "22ca6242-c61a-41a0-a801-786466012efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1299
        },
        {
            "id": "c1f10037-2eed-4d3d-a28c-c2f20cddb336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7680
        },
        {
            "id": "534449fc-22ce-4b06-92ef-012b63506b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7681
        },
        {
            "id": "41b5b252-228a-4af3-a4ba-c57004517431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7743
        },
        {
            "id": "d910ae4d-5f92-44c0-8f11-b6792bb76d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7808
        },
        {
            "id": "fe175d3f-bd1f-461c-9a43-8fb023d782ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7810
        },
        {
            "id": "566b5dc0-4296-4895-ac25-a7a4a0c2bab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7812
        },
        {
            "id": "5ed0f6e3-4e16-4a09-a41e-fdb960120f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7840
        },
        {
            "id": "6bc8aebb-d764-4896-8f7e-93d112e1ad12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7841
        },
        {
            "id": "04d7bd25-c366-4077-bda5-70ea6ff9b5ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7842
        },
        {
            "id": "73a2bd74-a3df-4088-8aeb-6ba0db10bad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7843
        },
        {
            "id": "f1ff550a-a2ae-483f-9277-b556165d04aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7844
        },
        {
            "id": "aec7a1ec-11f6-412b-a4d5-4b3342d04eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7845
        },
        {
            "id": "c3a7c0f3-3274-4b38-aee9-9862b8a8372c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7846
        },
        {
            "id": "175fddde-cd8b-4c0b-bf58-f75bc1dcfa94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7848
        },
        {
            "id": "4c7f842b-ee17-4dfb-a76c-e965a868fd98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7849
        },
        {
            "id": "00d11268-b77f-48ee-b402-2796ced86b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7850
        },
        {
            "id": "41b6c9de-124d-43c9-895c-69d0d3a7a3e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7851
        },
        {
            "id": "e3ed4a8b-a184-4e9b-9828-c6771dd96271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7852
        },
        {
            "id": "c50ca0d1-95a2-4273-81c7-0606074e7b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7853
        },
        {
            "id": "deb10a7e-f533-4a76-9c90-260abe6427de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7854
        },
        {
            "id": "6125c6c0-fb39-4f15-af07-469c708f3d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7855
        },
        {
            "id": "3623a1c7-562e-4a18-a32d-cbe9c6a13cd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7856
        },
        {
            "id": "db2eb1fb-9116-430d-9933-0fb3a16c87fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7857
        },
        {
            "id": "93b9ed47-4ba8-446a-b2aa-7f4076b86c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7858
        },
        {
            "id": "bcc2aaf8-db1a-4d0d-b44a-1e54dde11a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7859
        },
        {
            "id": "6b746d61-63a9-4cbc-ac58-180146e99574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7860
        },
        {
            "id": "162654c3-f9d9-49c3-b97d-b6a748459f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7861
        },
        {
            "id": "a7e38e35-e83d-4ee1-bd00-d6a4d39804d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7862
        },
        {
            "id": "8e116b0e-c48a-48ce-80f5-e18d9453ff14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7863
        },
        {
            "id": "2f7a7a57-a86c-4a37-96b4-54cb777297c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7865
        },
        {
            "id": "f42d1b7b-b5b7-40b0-9ae1-145a3cf26cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7867
        },
        {
            "id": "f6a412ed-d21d-42ec-9405-d447f2c87956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7869
        },
        {
            "id": "aa6f4cf5-233a-4bc6-b835-be0528f216dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7871
        },
        {
            "id": "c8cff8c9-e824-4864-b8e3-79d929d66f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7875
        },
        {
            "id": "b3611408-c3ad-432c-a47e-fd7704952853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7877
        },
        {
            "id": "c9554a2e-d2d8-43ec-ace9-aea2b63347e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7879
        },
        {
            "id": "74cbba31-a49f-43c2-b5ec-7d46791745a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7885
        },
        {
            "id": "109f68bd-2080-4d31-8ee5-648ea01c0a71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7887
        },
        {
            "id": "6faecdf0-09fb-4098-858c-e99a255dc2aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7889
        },
        {
            "id": "b2592114-098a-4496-a379-c9dc56d51d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7893
        },
        {
            "id": "0ab4f86e-e2b5-4338-a651-e42e22e64901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7895
        },
        {
            "id": "4e0a724a-63be-4f00-b101-7d2a644cc02c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7897
        },
        {
            "id": "068f31bc-58a8-4142-bfa7-c829a79d6927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7899
        },
        {
            "id": "e4ddbf0b-dfcc-4f29-a4b3-c436e32063d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7901
        },
        {
            "id": "3693c73a-bdf7-401c-acfb-e14380ebd8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7903
        },
        {
            "id": "5472f732-864f-4d04-9793-4c8a75f84c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7905
        },
        {
            "id": "3dd9abd7-247b-400a-abd9-ea79c3720ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7907
        },
        {
            "id": "adb7ff56-37a9-4045-8f22-461fa86d2d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7909
        },
        {
            "id": "5db71a43-4598-4676-93ba-2210e7a19af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7911
        },
        {
            "id": "17671a38-6a3f-456f-8fee-cf873a6ba170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7913
        },
        {
            "id": "972db5a0-08be-48f8-9b5f-2c976d55ae23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7915
        },
        {
            "id": "f6fea8ba-d7c0-40c2-bd72-9760da7985fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7917
        },
        {
            "id": "b5d17482-e193-43dc-8513-49a72b91d3a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7919
        },
        {
            "id": "817330af-64e9-45f6-8f4e-f898903dce81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7921
        },
        {
            "id": "8932ca1a-83de-425d-b71c-07ad8bc27852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 65
        },
        {
            "id": "dbff360d-5276-43ab-a1e1-44846abcca91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 84
        },
        {
            "id": "bc9c39e9-b748-4d89-b6dd-5b7fdffe821e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 86
        },
        {
            "id": "957ca344-e954-48e8-9c94-f79493023ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 87
        },
        {
            "id": "356f9b8c-d148-434e-8d56-0e092a7809d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 97
        },
        {
            "id": "453916f3-64b7-4fc9-b989-afde1b16be62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 99
        },
        {
            "id": "35c185f6-d6b2-44c4-917c-71d54f89c1ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 100
        },
        {
            "id": "56e4a2a4-0c34-4516-aec8-3486e35201b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 101
        },
        {
            "id": "55aa76ca-356b-4b80-8620-fc3f1edac820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "027a3a36-38fe-4e15-a7ff-35712d55ce4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 109
        },
        {
            "id": "e00eb881-4c9e-4e2d-8e46-e6d332b1de8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 110
        },
        {
            "id": "60567a9c-e23b-4bd5-83f9-d5c36f9b5926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 111
        },
        {
            "id": "a7d202af-adb8-4559-9529-518650e00b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 112
        },
        {
            "id": "e18a4788-7e7d-4e75-bb41-589e9661089b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 113
        },
        {
            "id": "304a7476-e167-4e07-9b62-2527f52f4d1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 114
        },
        {
            "id": "71db071f-bede-4884-9944-6f573797b01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 115
        },
        {
            "id": "fd129587-c8fb-4046-a5dc-2bc4a8bd65e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 117
        },
        {
            "id": "482e6bcf-dca1-4f22-80c9-7601b60ff931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 192
        },
        {
            "id": "e004aa44-28d1-45f0-891f-f642a6b42433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 193
        },
        {
            "id": "225a4e1a-cf8c-4d72-83de-e3a9f8bafb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 194
        },
        {
            "id": "7b4bd2db-8bf2-4af3-87a0-815c3e2af5a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 195
        },
        {
            "id": "61376881-3eac-4d54-932d-be36b4fec9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 196
        },
        {
            "id": "95535bd5-6c3e-47d7-9778-e784ddf0055e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 197
        },
        {
            "id": "779c1f39-6142-4ac2-912f-37a308097f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 224
        },
        {
            "id": "7d192e81-6ea2-402e-9135-f922741ec597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 225
        },
        {
            "id": "f92a6d78-e40a-4659-bb27-7a04d5e47138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 226
        },
        {
            "id": "d3f1c22a-c04f-4186-ace8-add6b69d44c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 227
        },
        {
            "id": "dc4b2835-ec7c-486c-bc59-30684c7a2c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 228
        },
        {
            "id": "35348d24-07ce-4b67-a5a6-5bbe715f88bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 229
        },
        {
            "id": "561d4357-327b-4a30-bff8-77b4c8218d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 230
        },
        {
            "id": "23a81d2e-5522-4337-b86f-4c731144de47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 231
        },
        {
            "id": "084fab55-e6d1-4440-9594-8f49473ded43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 232
        },
        {
            "id": "580eb36c-0e0c-450e-be49-fdc41fc863ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 233
        },
        {
            "id": "aa21b851-c799-4bca-baf7-f9eedb4a332d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 234
        },
        {
            "id": "f3652be5-66d6-4807-b71b-129410535993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 235
        },
        {
            "id": "289c954f-f80b-4ec8-8f57-8058a40f0254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 242
        },
        {
            "id": "8e0bc4af-308b-4b43-951a-bd52e2b530ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 243
        },
        {
            "id": "ba6da311-6bca-4aff-9586-733fdf15637e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 244
        },
        {
            "id": "3c46359e-2404-4c48-a5aa-767207ab24ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 245
        },
        {
            "id": "74487ba0-8db8-4f86-9997-0d2f832b9f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 246
        },
        {
            "id": "a4717ba5-4a52-4a0c-a81a-2612a7865c1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 248
        },
        {
            "id": "7b1c3c35-dc30-4e07-94fa-6aaedd3669d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 249
        },
        {
            "id": "dad4c360-8214-493f-a40b-e5cad5bfa222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 250
        },
        {
            "id": "90b776a5-6d97-4527-9fbf-a356fb469cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 251
        },
        {
            "id": "4a58d26f-8add-40a6-82a4-9dfc44eceeb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 252
        },
        {
            "id": "c2fa4e8d-e532-43f1-b3f1-a39e427779c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 256
        },
        {
            "id": "c4f7d0b3-fee1-4597-99cd-2eddb34d4f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 257
        },
        {
            "id": "f933fa5b-671b-4377-a980-fe44257e8fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 258
        },
        {
            "id": "2ab20412-6e58-4120-b865-8e8b3898b4c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 259
        },
        {
            "id": "b7cedfd5-5e76-4785-9906-f78daae53388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 260
        },
        {
            "id": "18b9d824-d541-4373-9075-855d4a09bb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 261
        },
        {
            "id": "6c0a5f31-8444-4c96-879d-163ff155ae36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 263
        },
        {
            "id": "6655ffa3-d8f2-48c8-a168-75842b5a601f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 265
        },
        {
            "id": "5b9f5d5e-4f8a-4467-b1e7-67b082d6e959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 267
        },
        {
            "id": "0184c86d-bf96-4d03-a6d7-c70f6e0ff1a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 269
        },
        {
            "id": "3adf3a93-74ee-41a9-a9bb-52af25e757c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 271
        },
        {
            "id": "31fcfd32-d80f-4b00-9572-882bf4d5610a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 273
        },
        {
            "id": "622df2b5-6bfd-4ca0-8acd-b3f658b31f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 275
        },
        {
            "id": "df045ab3-c8ea-4dc9-a840-4cd6f688e586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 277
        },
        {
            "id": "b65e3fe0-db5e-4316-b494-68bf857814d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 279
        },
        {
            "id": "4f19aafc-a303-4c15-86b4-436f6a29e025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 281
        },
        {
            "id": "50b2ecf5-b8c3-4b5f-a07b-90caff63c35d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 283
        },
        {
            "id": "f83a1aa9-9eed-4bd7-98c8-a9ff7125a998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 285
        },
        {
            "id": "286c31d9-b4e8-4527-8b87-5046652094d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 287
        },
        {
            "id": "1d66e8d7-d6e9-4e02-aa17-325b1da5b638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 289
        },
        {
            "id": "ba7c7bb2-e898-4f77-a3cd-05cc838c82bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 291
        },
        {
            "id": "1840fb8a-b056-4a12-ae82-d5f90bde2b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 312
        },
        {
            "id": "818e6b50-1341-4865-9cd0-e52076fab129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 324
        },
        {
            "id": "23824302-62ad-4609-b2d9-a349df19eaeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 326
        },
        {
            "id": "023feed4-c4ec-4e9c-b0c0-be52dc604f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 331
        },
        {
            "id": "b750bf69-f5b1-4b7b-9a4c-d5df932d0526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 333
        },
        {
            "id": "250615f4-1242-43cc-b13d-7aa2837fe48e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 335
        },
        {
            "id": "5c70b2e6-d05a-47dd-ab15-f5dc168c8ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 337
        },
        {
            "id": "0cd5b171-adbb-4209-9152-6404b008e2aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 339
        },
        {
            "id": "464bae37-94a1-4043-b617-0053101bef07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 341
        },
        {
            "id": "14b63c0d-22fd-473b-9683-11e4c16c1871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 343
        },
        {
            "id": "d1afd00c-4406-4146-aaf7-d64bf1a67ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 347
        },
        {
            "id": "5b1920e4-fd77-475e-855f-85a5cc5fbcb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 351
        },
        {
            "id": "c106d79f-c1ee-45e9-a841-d4c9c1989b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 354
        },
        {
            "id": "7b73ae8f-adbf-4544-9cfc-899376e09ad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 356
        },
        {
            "id": "36202390-33aa-4afb-8cc0-0e530b23aa26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 361
        },
        {
            "id": "0e0fb1ea-436b-4046-8c62-71035a85ac3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 363
        },
        {
            "id": "a13ba795-31c6-414b-aa92-02dfaebea6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 365
        },
        {
            "id": "61615dbb-ded8-46b5-a60f-65b317be0075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 367
        },
        {
            "id": "4c99fe09-546d-4c9c-b7b3-ba349f3e19bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 369
        },
        {
            "id": "c752b8c8-12c7-4d56-b84a-919d1ba9a2b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 371
        },
        {
            "id": "1d02b175-d073-4094-b5fa-299d2580cd09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 372
        },
        {
            "id": "fd11ae80-d8a1-4636-9eb3-12849aa95bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 417
        },
        {
            "id": "70039e23-6349-4946-b649-fefd6e29d299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 432
        },
        {
            "id": "d01fe29a-0e64-46f1-a323-8791a0de46ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 506
        },
        {
            "id": "467aa34a-b7c3-46d6-a6c5-00e2cf155acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 507
        },
        {
            "id": "3bb88554-8a66-4e47-8e30-32e096d51328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 509
        },
        {
            "id": "3a1d12a6-d574-42b7-aa85-dd08f8e09274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 511
        },
        {
            "id": "b5e31c64-f8be-4233-94da-ff69d39989ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 537
        },
        {
            "id": "8320051b-7d22-4c76-8ca8-6e43aeff54e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 538
        },
        {
            "id": "f3808ac3-2176-4c9f-85cd-e34e3ac3cb1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 902
        },
        {
            "id": "aeaf6f29-d6a5-45e8-ada6-196d6c036483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 913
        },
        {
            "id": "d3a5bee9-0c3b-48dc-b13d-78ef59e5ecde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 916
        },
        {
            "id": "61e34d68-02ed-4e2b-9bb5-d8aa5aa9f299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 923
        },
        {
            "id": "3d0eb492-8220-479d-9da5-b7e96bf97720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 940
        },
        {
            "id": "7e2a34a6-10c4-4fdf-8132-c572eac6eb94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 941
        },
        {
            "id": "55bfe60c-f9d5-4b88-bc10-2e7eebe4c8a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 942
        },
        {
            "id": "ae12c9c5-7fa5-46b6-8091-25fa87b3a5a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 945
        },
        {
            "id": "ed7e8083-478e-4631-95cb-6ac9195e3351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 948
        },
        {
            "id": "4f23009d-6839-43ed-862c-789a1d90d83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 949
        },
        {
            "id": "be7a7c19-94f2-4c78-b6ba-4b0622334c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 950
        },
        {
            "id": "41525b8a-2431-4641-a2f5-e50b60c15200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 951
        },
        {
            "id": "d1ffe1a4-dd34-4d88-8d79-320a5072f250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 954
        },
        {
            "id": "92fd49f6-db94-4aa5-97cf-49124c1a771f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 956
        },
        {
            "id": "38e3f992-c576-4848-8776-643abbb0eaa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 959
        },
        {
            "id": "780daa96-079c-4615-b5cd-0ca9f3d4756e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 961
        },
        {
            "id": "68dfa091-6924-4852-83c5-16b78b138f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 962
        },
        {
            "id": "80c8380f-d37e-4dff-a446-a6c6f9b61796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 963
        },
        {
            "id": "3cfcf609-d241-4383-ab78-ec87abdb9f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 966
        },
        {
            "id": "ffec8995-4527-4dcb-a412-3aec7221edf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 972
        },
        {
            "id": "3340a511-c125-46b9-be03-15c5d8ea24b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1033
        },
        {
            "id": "360a10a4-f874-45eb-8311-2065213ad960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1040
        },
        {
            "id": "1b987a17-de14-4b58-84c2-c5478e5e2639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1044
        },
        {
            "id": "aac9c0cb-322f-4638-9b68-343f03f5fa83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1051
        },
        {
            "id": "818a01d5-ff36-46ef-9892-8a7a60ed1404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1072
        },
        {
            "id": "4397cd13-a039-47ae-93b6-273a8b45eab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1076
        },
        {
            "id": "43b2e8de-ee70-49d2-a282-2e7470500db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1077
        },
        {
            "id": "c3bf4d7d-c92d-4429-a6bc-71d0970f4a37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1083
        },
        {
            "id": "059ff1c5-0f20-45bd-84fa-dd3d975da016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1086
        },
        {
            "id": "aef38ffa-b9fc-4278-b5a4-cb3808a08a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1089
        },
        {
            "id": "a668668a-0284-4c4d-99f7-32dc901b86c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1092
        },
        {
            "id": "2fe2b95d-bc1a-4fc2-ab0a-8ffe13f8429a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1104
        },
        {
            "id": "5a45708d-69ba-4302-a9c0-902d7cf79e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1105
        },
        {
            "id": "f7f42050-e0ef-422d-9952-8c345211d3fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1108
        },
        {
            "id": "88d5ebad-9af0-4151-ab0f-7f2975291b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1109
        },
        {
            "id": "d072c665-abbf-4e8d-af94-495b969c6669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1113
        },
        {
            "id": "346ab6f3-b139-466f-bfed-e034f75ce419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1126
        },
        {
            "id": "4ba56ed5-da97-4d20-ac51-97e760556e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1127
        },
        {
            "id": "56be9211-c64d-4c9f-b09a-69ae287de2e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1139
        },
        {
            "id": "dd7f8138-026c-4767-8769-fcecbe238e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1145
        },
        {
            "id": "2e9f3cc1-56e6-441c-8251-66192ee14fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1147
        },
        {
            "id": "a478abf1-077b-46a7-a7b4-acd3b76d4e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1149
        },
        {
            "id": "1348c97e-bffd-4536-841c-10f09d4d2076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1153
        },
        {
            "id": "3a589556-f376-47b4-bd4a-95f3b5cefdca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1193
        },
        {
            "id": "155bc7e0-207d-46d6-8f67-6dd524014a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1195
        },
        {
            "id": "4ee491e9-272c-47af-920b-3da43a41c4f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1221
        },
        {
            "id": "adbd25cb-585c-4868-9bd4-e2cad383479a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1222
        },
        {
            "id": "22e08184-8d70-44dc-b0d4-72e8f55184be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1232
        },
        {
            "id": "6f9924ac-998e-4970-91db-634658e60fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1233
        },
        {
            "id": "dd44d030-fd37-4f04-a62f-2d0986594889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1234
        },
        {
            "id": "15059008-f24a-41f8-bb36-e04f1271ceb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1235
        },
        {
            "id": "3dca963e-0cd4-4f5f-8623-9bfbab7ad140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1236
        },
        {
            "id": "82e14264-9c22-4e75-8266-806b589b4768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1237
        },
        {
            "id": "796ace7d-d2a1-4d32-a03a-f21a0e6c7673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1239
        },
        {
            "id": "32c8541f-0ff3-4fd1-b033-c375586d94d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1241
        },
        {
            "id": "561e5002-4820-44f7-9d1d-446df2a3535a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1243
        },
        {
            "id": "2861c615-1ec4-499d-8f8d-83eac5dd8109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1255
        },
        {
            "id": "8599acdf-1735-4e3b-901c-9a3849d12bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1257
        },
        {
            "id": "4d901603-faa6-4d03-a3e0-9acc087f0f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1259
        },
        {
            "id": "dfca5b1e-f96c-463d-9f57-83dc34da8394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1280
        },
        {
            "id": "1d7c7b8b-68e3-42dd-95dd-93fae9e7d2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1281
        },
        {
            "id": "b590e448-d944-4222-8ea7-fed0290a2090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1282
        },
        {
            "id": "a3b11c66-cd2d-468f-83ef-190d11aeddbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1283
        },
        {
            "id": "949223a9-0f2a-486b-aa33-2dbc4b9a62d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1288
        },
        {
            "id": "b2d29a8c-80af-4289-bea9-555a6f2d1982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1289
        },
        {
            "id": "262b4b57-57b6-4443-b7f6-7a2a2a9ca35e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1293
        },
        {
            "id": "216d8af4-ec8f-4b90-be0f-64992a8e0e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1297
        },
        {
            "id": "23344864-a39c-45d9-ad41-fef9b1b6ff6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1298
        },
        {
            "id": "c712192a-d863-459f-ba31-0272884fd111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1299
        },
        {
            "id": "d54bfc24-78b7-40d3-a048-397a45c02d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7680
        },
        {
            "id": "a09c829e-aba3-4d42-9a95-014807d16323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7681
        },
        {
            "id": "27746d5d-c5af-48a7-a6b6-c7d1fb4c085a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7743
        },
        {
            "id": "347b204d-ce95-4245-920e-0ac236ee08b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7808
        },
        {
            "id": "dd329759-cdd7-49fb-a2f2-e2fe436133a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7810
        },
        {
            "id": "39b70640-e968-49cf-a011-810f34abe9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7812
        },
        {
            "id": "5427ae05-1c2c-4fe1-a401-3de416fcc09b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7840
        },
        {
            "id": "307f1893-f6d9-4e69-9676-144cc231aedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7841
        },
        {
            "id": "8a04f081-595d-44d7-b4b3-b8412ebada5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7842
        },
        {
            "id": "8e481f3f-1223-4bd6-8adb-c76f4e61686b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7843
        },
        {
            "id": "1d8b11ab-e0e0-4dbd-b379-3a62423609d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7844
        },
        {
            "id": "63e3c3f1-5f9d-4d61-867d-9ae00ca9c31a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7845
        },
        {
            "id": "3f0a066b-0d6f-4787-a13c-f1748ad74974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7846
        },
        {
            "id": "fa3d82ab-146d-427f-b6f7-0d78def8b9fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7848
        },
        {
            "id": "37a6f97e-260f-4f03-903a-5e6592776dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7849
        },
        {
            "id": "a63153d7-e418-4eec-8149-9148b56ca16f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7850
        },
        {
            "id": "04e521f1-c8d1-45a9-8cb2-f36e15c59227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7851
        },
        {
            "id": "dd264df6-0e47-4ee0-81a3-7901aba10106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7852
        },
        {
            "id": "75acc153-02ef-4d97-bf1e-08f39d982b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7853
        },
        {
            "id": "a84851ff-58fe-437f-bd45-a7111e1d9d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7854
        },
        {
            "id": "1f9e22ab-7368-48cc-b99d-31a04e724037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7855
        },
        {
            "id": "75687a1b-901a-453e-af14-6e458d6c3050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7856
        },
        {
            "id": "d0f50c4d-e3bd-4e94-b6da-a4230bc89832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7857
        },
        {
            "id": "a2098199-0b67-4234-bd20-46f8056c1441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7858
        },
        {
            "id": "a8c42e59-7186-4a56-86f6-7ddb2d3b29b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7859
        },
        {
            "id": "d4d3a594-c19c-4630-bf79-a08e3aabc8f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7860
        },
        {
            "id": "045b0333-ea1e-4154-bd9e-51d2936ca878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7861
        },
        {
            "id": "328cff98-c2cd-4e08-8d86-316b0ba21ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7862
        },
        {
            "id": "6b5d7234-dbaa-44dd-8db4-3b7a90790421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7863
        },
        {
            "id": "b1ecf156-f6ac-4e48-ad69-42fe969383cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7865
        },
        {
            "id": "f4317347-73a6-4371-91c1-e39e7c6f12f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7867
        },
        {
            "id": "10f5f436-d213-4013-9c69-bbc890f80aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7869
        },
        {
            "id": "1b1ea462-c8fa-4af1-bb31-6ef8e507bc3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7871
        },
        {
            "id": "2b1cd4da-524c-42a3-bc7b-d733266091ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7875
        },
        {
            "id": "fd8bb49b-1c00-46c7-8129-45e72496c2b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7877
        },
        {
            "id": "32526eeb-f4af-4ff6-b995-3463c141386e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7879
        },
        {
            "id": "782a1602-e5a9-4973-b3a3-6ade2a246578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7885
        },
        {
            "id": "13dd7192-4608-4cd0-b165-08c32ef3d027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7887
        },
        {
            "id": "5edd6d48-46c6-47c4-9351-7ffeef90490e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7889
        },
        {
            "id": "429a86aa-f50f-4fff-8089-45bb746410bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7893
        },
        {
            "id": "1b9ffbad-0608-4ca9-aeee-701fee778ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7895
        },
        {
            "id": "26329054-de5b-4a06-a850-d56196fcd6f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7897
        },
        {
            "id": "f60d7ba2-7d75-452e-9387-2f6b2ca84f17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7899
        },
        {
            "id": "27507704-592f-43a2-b444-ed088bd46317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7901
        },
        {
            "id": "574d8941-5f56-496f-acc0-ecd931fee8e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7903
        },
        {
            "id": "f5c6367c-9121-4737-9a2d-d8332c2a3131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7905
        },
        {
            "id": "2fb1f648-1d54-4d8c-bcbe-bead724da445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7907
        },
        {
            "id": "fa671577-32ba-40ce-9003-d6b764c14e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7909
        },
        {
            "id": "70c4b5b3-d331-4af4-b12a-237c9b2a737b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7911
        },
        {
            "id": "4f5a3f35-2e50-49e4-8c4e-9a9b62742732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7913
        },
        {
            "id": "fef65ded-cd7f-49c8-a19f-547ca00dfffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7915
        },
        {
            "id": "2e59bf9f-9bb8-4277-b666-246e2c0d01d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7917
        },
        {
            "id": "4bd8f624-2d80-4b36-a217-a6037d32f48a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7919
        },
        {
            "id": "47d5cd96-81c6-42a4-bda2-97a21416c83c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7921
        },
        {
            "id": "a37d629a-684d-4c21-bb93-28e44db117b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 40,
            "second": 74
        },
        {
            "id": "17040193-f8f9-46b0-a00e-94a2d004eddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 67
        },
        {
            "id": "24670902-b3a7-4596-9bf8-d59db50e8f9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 71
        },
        {
            "id": "38b163fb-4a54-4410-8dc2-ed3979d6a82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 79
        },
        {
            "id": "855b00b4-feae-4399-b01a-4ff481140ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 81
        },
        {
            "id": "d9d08c39-5b0a-434b-985d-eed8e4c7d5d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 84
        },
        {
            "id": "9139782b-627f-4d42-8145-21afea8d63fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 85
        },
        {
            "id": "0200fe41-a4b0-4328-b557-c1e53884214b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 86
        },
        {
            "id": "0727a81e-c2c4-49b1-838f-5f7c5f79810b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "d724cbf1-547c-4020-b6fb-92a6dce98a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 89
        },
        {
            "id": "f9d3d250-04fd-421d-9dd3-d52db711cc1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 199
        },
        {
            "id": "e4d09f15-03f8-4485-8f04-271b61ace8ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 210
        },
        {
            "id": "caee3b88-af78-4c24-80c2-2e3e5d402c7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 211
        },
        {
            "id": "f99c7ffa-707d-4e57-b931-78d66f5ad68a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 212
        },
        {
            "id": "7df05cce-fc70-439f-8913-dd05ec482882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 213
        },
        {
            "id": "89dba823-5bf6-4572-9ad7-509acc18ab92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 214
        },
        {
            "id": "7796e41c-f035-4bdb-87d9-0705ba9aa4fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 216
        },
        {
            "id": "90e39f99-e3b9-4f39-95e6-8ef85049cd5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 217
        },
        {
            "id": "ab83c2f3-2efa-41a0-90e8-7eb1593c023d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 218
        },
        {
            "id": "88a3bfae-f819-4cff-b655-5acb515911ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 219
        },
        {
            "id": "3bf54339-0c53-44d3-8034-2652ffdeee87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 220
        },
        {
            "id": "cf3ce8bf-bc1f-4eb0-bfca-e1dcff7dc5f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 221
        },
        {
            "id": "e9a3b2c3-40d9-4e33-a09b-93296864593a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 262
        },
        {
            "id": "f6b6fcfb-87d6-4288-b729-ad61edabfea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 264
        },
        {
            "id": "51f0ea45-c65f-481c-af8f-85459a86eb28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 266
        },
        {
            "id": "54aa8d16-c355-4a60-b13b-ab06d146c652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 268
        },
        {
            "id": "6cfd2dec-3fcb-4a03-989e-6d8714501f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 284
        },
        {
            "id": "572ad7a3-c9e7-47f0-b2ee-022bc942dfdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 286
        },
        {
            "id": "5a295312-5d2b-492f-a58d-3721c64917fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 288
        },
        {
            "id": "a6cd7106-aeb1-4e5a-9804-6350124041b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 290
        },
        {
            "id": "53f71945-a467-4ccf-889f-33d503d2fa4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 332
        },
        {
            "id": "e5ea4e2c-f5e7-4709-9002-3703e4dd01aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 334
        },
        {
            "id": "d042c659-7d88-477b-bdc1-40a2280cf477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 336
        },
        {
            "id": "d2a05214-b5f5-484a-ae1f-6ce88dea8939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 338
        },
        {
            "id": "6ca512de-79ce-4866-8708-a5eb3cbb2162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 354
        },
        {
            "id": "622472c5-9b26-4e13-86b6-52a521760157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 356
        },
        {
            "id": "6bd43c6b-832e-4344-805d-61fd47295d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 360
        },
        {
            "id": "625e7b3a-5b87-4dbb-96ba-464767f2cc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 362
        },
        {
            "id": "22bae722-a580-4b55-aac1-f40aa975b56d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 364
        },
        {
            "id": "cd240816-d176-4365-a35c-2ef323e812a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 366
        },
        {
            "id": "be5c1088-84ea-41a3-81ac-0bd70d3f1945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 368
        },
        {
            "id": "0b4b006d-d7e8-468c-bd8e-e5fcd84d5962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 370
        },
        {
            "id": "7f380212-62d3-4cc5-9eff-29d8b42ed39c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 372
        },
        {
            "id": "55b119ed-1ae1-4da3-8b22-dafadac49614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 374
        },
        {
            "id": "cdf36ff5-fabb-4580-8ef5-daf96553bb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 376
        },
        {
            "id": "0e3206a0-24ea-4372-9bd4-9e753344834f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 416
        },
        {
            "id": "f2c14e53-d114-4988-9598-306820da7e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 431
        },
        {
            "id": "fc874b72-b1b3-4ab0-89c5-af7c55eb8866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 510
        },
        {
            "id": "620e3039-df1a-49c7-b6bc-6de65d675900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 538
        },
        {
            "id": "934c32ba-f585-4630-b894-f438d6cee357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 920
        },
        {
            "id": "9d5851d5-91fa-435a-b715-181406c770f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 927
        },
        {
            "id": "747de22a-1378-4462-88c9-f56bf6c18849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 932
        },
        {
            "id": "d3d93094-63e9-4d60-b75e-ee34464ee989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 933
        },
        {
            "id": "5c396ca7-3d21-4f7a-a7f3-15d0d119a49a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 934
        },
        {
            "id": "b292c8da-fef0-42dd-b8d4-6d9a70bb1329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 936
        },
        {
            "id": "ae8c0c93-f2ad-452e-bcd6-ab2e9110b2a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 939
        },
        {
            "id": "a0c9bc5f-c175-43e0-a738-78e1f5b31696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 952
        },
        {
            "id": "1c0a198b-cc57-4e2c-a536-e97a440fc05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 978
        },
        {
            "id": "14716f91-3e00-4dba-83ef-fd29677f7b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1026
        },
        {
            "id": "ffa4676e-dfa2-4cb8-938d-17fe91752112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1028
        },
        {
            "id": "2708a567-feda-4efd-a17f-125c88ae27be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1035
        },
        {
            "id": "694f00f4-e8cb-441f-b7b5-625f5e25916a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1054
        },
        {
            "id": "80fd2f73-0564-4bfa-abbe-4f94d4f4c2c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1057
        },
        {
            "id": "49a63703-d483-413a-94c9-432136ba38c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1058
        },
        {
            "id": "fc0a4571-a099-4a80-b533-d16b0066c2b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1060
        },
        {
            "id": "0c15ed31-c5b5-486a-b77e-b03ae7f1eaf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1063
        },
        {
            "id": "4287ea88-003e-4296-bc8a-3aa42957dfcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1066
        },
        {
            "id": "ace46b06-4150-4757-8093-e85b0084c7dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1090
        },
        {
            "id": "e0754a5d-9c9b-4010-affd-0c5d6add582b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1095
        },
        {
            "id": "e8cdfa48-84f1-49ac-9cbe-3ab002f97c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1098
        },
        {
            "id": "10375888-ea40-4c86-a118-bd3aa3eb50ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1120
        },
        {
            "id": "9e874299-c27a-4402-8dd3-c11ec708eb47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1136
        },
        {
            "id": "bf248233-67a5-4859-940a-6ce391551110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1138
        },
        {
            "id": "c4876e0b-87bc-4069-bc7e-178dc8efbecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1140
        },
        {
            "id": "335a3052-e75b-4dd1-8796-4bfea70e4ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1142
        },
        {
            "id": "b9c4089c-a5e3-4c23-b39e-05426619a288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1144
        },
        {
            "id": "a91819f3-2aa5-43a3-b1ba-0b48e91a49d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1146
        },
        {
            "id": "b31d3b1b-6537-4b29-93a3-83158ce05e2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1148
        },
        {
            "id": "faa94515-7084-4f94-bc77-e9d05e265eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1150
        },
        {
            "id": "779b0c31-45b9-4e3f-9924-f217a870add0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1152
        },
        {
            "id": "8c214b15-13ef-4271-bff1-de75980d59d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1184
        },
        {
            "id": "6cc08c3d-3bc2-45ff-8f49-a686e8431154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1185
        },
        {
            "id": "9eeb820f-1080-4377-a89e-c0d633ed649d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1192
        },
        {
            "id": "a29c7ab2-b3ba-42cd-a610-72b138e8787f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1194
        },
        {
            "id": "5b5bbb9b-89ca-4605-8f40-a496a6aee515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1196
        },
        {
            "id": "3156d73d-236d-4aff-840f-fb894245d6f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1197
        },
        {
            "id": "a3f1590c-72eb-45eb-96ba-daa2a570ed3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1198
        },
        {
            "id": "0f4a574a-39ce-4866-ac87-6f41665fb6cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1200
        },
        {
            "id": "e076575f-b18b-4e6e-8f38-0de2d5dd1947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1204
        },
        {
            "id": "d0a420de-5fad-44ac-bba2-9fbe3429113f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1205
        },
        {
            "id": "9d2dc15a-ea5f-4ec6-849f-c3b946215daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1206
        },
        {
            "id": "3562150e-d7b0-4b3c-baf2-09abbfce128a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1207
        },
        {
            "id": "83270107-e1f4-47b7-b7ef-9db15fd4b5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1208
        },
        {
            "id": "c7781c19-6587-4a03-b718-9713e6c50247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1209
        },
        {
            "id": "4c1abdcd-997a-4b91-8f34-301688a6f042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1212
        },
        {
            "id": "dac50fe2-2a26-4b52-a75d-3c0a849e2ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1214
        },
        {
            "id": "17e8090c-b31b-4d71-a751-5b0ed5ddfdcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1227
        },
        {
            "id": "a4c87342-336d-4e06-87e6-7befe78e5177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1228
        },
        {
            "id": "0808e9cd-9932-42c5-8648-178e33bd0691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1254
        },
        {
            "id": "92ae6451-fb5d-4d27-bd50-bc3fb8169380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1256
        },
        {
            "id": "e21efb07-344f-4aa8-b14c-218c6f10edd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1258
        },
        {
            "id": "0fe963c6-d06c-43db-b64a-dd19d78bbdf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1268
        },
        {
            "id": "10333f41-dbfb-4202-970e-3d0ece8238e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1269
        },
        {
            "id": "0878fdd3-d68e-497d-81c2-05a3dabcff54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1284
        },
        {
            "id": "19c1dd43-e543-4565-a3ce-ff0729c79b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1285
        },
        {
            "id": "37e5f4a5-ad51-4935-8c4d-5fb325bf485b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1286
        },
        {
            "id": "e29c3c4c-f54b-4357-a49e-8371333066b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1287
        },
        {
            "id": "53ddf46d-891b-4d8d-81c2-1ab690b85889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1292
        },
        {
            "id": "578153d0-1588-41c3-913f-3c1f17ccc9c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1294
        },
        {
            "id": "47799cbd-c95d-45bd-9ca9-3a95a61e0ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1295
        },
        {
            "id": "119a3dbd-cc2f-4c76-a1c1-8ab982d83057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7808
        },
        {
            "id": "3cbd5441-eb4b-42b3-8a74-5ccd85650ed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7810
        },
        {
            "id": "b177b3e1-c14d-427a-b696-c4eea2e25e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7812
        },
        {
            "id": "6ac61cd7-dc5d-405c-8299-a5a5ebd098b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7884
        },
        {
            "id": "5d5545f8-dd0d-4731-bb04-afa2711eea3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7886
        },
        {
            "id": "ab26617e-41ea-43b0-8723-b0dc0a58cec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7888
        },
        {
            "id": "2a868ff1-4aa5-4d7e-9b9e-07fd2b91d498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7890
        },
        {
            "id": "74ae0ee9-4d23-492f-8e4c-98bace1e97ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7892
        },
        {
            "id": "18b43695-c79c-48e2-b6ec-866e2d7928dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7894
        },
        {
            "id": "83617412-bc2c-4582-a688-19d0d7d73270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7896
        },
        {
            "id": "2a3dde10-9397-4728-bbbd-13b3922270d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7898
        },
        {
            "id": "e60b51c1-0683-450f-9428-465fa6369aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7900
        },
        {
            "id": "8b001062-753e-482d-8f91-bf4d784e45fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7902
        },
        {
            "id": "4dbb5fe2-aedb-4370-a4a8-a2d1da090a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7904
        },
        {
            "id": "0e2b456e-2bbc-4243-8b61-b61e3a15044e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7906
        },
        {
            "id": "e25b5c13-7025-461c-9abe-69de5b846c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7908
        },
        {
            "id": "a38e56e5-0f14-41e4-be33-38382ff7156f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7910
        },
        {
            "id": "b32a17fa-cc8e-49e1-ba5f-554ef12bf65c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7912
        },
        {
            "id": "06a6d789-9afc-4f07-a9a4-3f6fb5ef7ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7914
        },
        {
            "id": "082868c1-4c99-4735-bb43-05b53ac19864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7916
        },
        {
            "id": "c10b2937-1ef2-4c04-adf8-686ed70b1505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7918
        },
        {
            "id": "8d945892-22c1-4f4a-8478-af9fb6b3a20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7920
        },
        {
            "id": "1eb5479e-a2a0-4904-8f4e-94b4147c9250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7922
        },
        {
            "id": "dd8aeec1-ed0a-45e3-8e87-d0af86d214d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7924
        },
        {
            "id": "c84a1922-4673-45bb-ae92-c796885a2d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7926
        },
        {
            "id": "5fa3bb6c-7a27-492e-9d7f-13471fc1e6cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7928
        },
        {
            "id": "be3ea33e-4a1c-483a-9b92-b0e246ee66c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "b16080b6-5619-49b8-a9ef-867700ada7ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 354
        },
        {
            "id": "ca1dd776-130b-4953-8373-e39f71345daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 356
        },
        {
            "id": "46d7b74d-96a2-4232-8cf4-c3105cb615ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 538
        },
        {
            "id": "6fd57f25-1e4c-4c14-aae2-4772d1173560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 932
        },
        {
            "id": "53f22848-25ee-4299-ae43-c7740652ad97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1026
        },
        {
            "id": "77c9e28c-2949-484a-bcd5-2526915cfffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1035
        },
        {
            "id": "a94654f9-12ba-4b08-808d-cd4c265baaaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1058
        },
        {
            "id": "e24e2c86-2102-4f7d-94df-707efd659c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1066
        },
        {
            "id": "bab61e23-05ef-464c-b8fd-f90ef8dd2434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1090
        },
        {
            "id": "59471fe9-fa64-49e3-871f-5512b9e7020e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1098
        },
        {
            "id": "6ca54c30-a867-4b2e-8b6c-74d6dde68673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1184
        },
        {
            "id": "3a206605-d598-40cd-b5ad-c2ef16f67c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1185
        },
        {
            "id": "280bb19f-4dd1-4306-ab98-02be58e9a664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1196
        },
        {
            "id": "e550db91-a633-43a7-9190-c9a739e6275a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1197
        },
        {
            "id": "23685293-8108-4ec3-8ce5-f45a79a119a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1204
        },
        {
            "id": "4afeff65-ea74-4574-a63d-c685fde1ce54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1205
        },
        {
            "id": "4f9a8a31-9180-45e8-856b-8511114dc5c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1294
        },
        {
            "id": "9397cbd3-5997-4ffc-82df-cf0c67fdc9fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1295
        },
        {
            "id": "cc48e55e-ebc4-4b32-8f82-105b79bd7c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 67
        },
        {
            "id": "e497d471-7e1b-4e52-828a-1be40d323a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 71
        },
        {
            "id": "8cb3bf88-589c-47e8-b7d5-b8fe1a3e9790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 79
        },
        {
            "id": "e588c77a-a1c2-424a-8ba4-bfb7180cc9ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 81
        },
        {
            "id": "48da9495-b282-41ee-97be-205794e2f38b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 84
        },
        {
            "id": "ca41991b-a8c6-4e03-b9d0-23070871ebad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 85
        },
        {
            "id": "f52f88ac-0447-458d-9958-2aced2e36244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 86
        },
        {
            "id": "f39fe268-77ea-429d-8887-7e02a8b739b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 87
        },
        {
            "id": "6b94eb52-a91c-4fa2-a199-8fcf2d6ca273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 89
        },
        {
            "id": "73ca79a7-7541-4605-ab80-cee94733b8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 199
        },
        {
            "id": "f1a6afd3-5641-4d5c-8d01-05312f9d0689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 210
        },
        {
            "id": "56edb4b6-a537-48b5-84a2-58944b4ee10c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 211
        },
        {
            "id": "9a3136a6-5eaa-4a86-b4fa-29914bebc73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 212
        },
        {
            "id": "b1595ce1-4d1e-49dc-b339-1d48e514bc79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 213
        },
        {
            "id": "2aabd95d-e4ea-42b8-89d4-78fd1f593574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 214
        },
        {
            "id": "1e81f873-fed6-48f8-9ab8-4ade8c2dd662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 216
        },
        {
            "id": "ca5cafa8-73d1-486f-bcee-8e3a24ffb7c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 217
        },
        {
            "id": "c86b77f5-a04b-4623-ab2b-2a33c3ddbdcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 218
        },
        {
            "id": "90875642-c1e6-49d5-90ac-e80e70f6742c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 219
        },
        {
            "id": "487128b2-e3da-4564-b335-bc7bf1314b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 220
        },
        {
            "id": "0549e2a3-8f7c-41a0-b3d6-f3fdd665884a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 221
        },
        {
            "id": "fb9e0c91-50b1-46ef-9f68-bf586350802a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 262
        },
        {
            "id": "4806057d-21f9-4d3e-8c63-650148ada13e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 264
        },
        {
            "id": "bb107f84-e985-400e-9b07-563c73b09327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 266
        },
        {
            "id": "45e5166c-6353-45e6-951c-d1a333849cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 268
        },
        {
            "id": "c97d8fba-ece3-4498-8aa7-4a749f9b1e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 284
        },
        {
            "id": "7a77e2b1-cad4-47d3-976e-08c6ae6c0b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 286
        },
        {
            "id": "5c6704f6-1f68-46ac-8ee5-b833b23db5d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 288
        },
        {
            "id": "c70c6917-49d4-421a-badd-77bc3caff177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 290
        },
        {
            "id": "cd221334-6732-48c5-8d91-31e53005829a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 332
        },
        {
            "id": "20d1e63f-2206-4a63-98da-8b5da361d555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 334
        },
        {
            "id": "28c2badc-b794-4762-8940-0c53d72a4739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 336
        },
        {
            "id": "e16def1a-3694-4710-8f76-8c249155eb9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 338
        },
        {
            "id": "603b5b1c-018b-474a-a734-f973dae51818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 354
        },
        {
            "id": "d6d082ef-99c1-4515-8c00-87266d9a94d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 356
        },
        {
            "id": "79dfb788-e2bb-4ed5-9da7-9c15f7d400a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 360
        },
        {
            "id": "22d3e186-ea69-4510-a3bb-a2643328f3ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 362
        },
        {
            "id": "734bf6eb-7083-4e82-a58a-38926c6193f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 364
        },
        {
            "id": "7c004d88-c94f-49a0-b9bd-969ea5f982d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 366
        },
        {
            "id": "2a358df8-bd7e-4c8b-8b97-3c99b8be48b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 368
        },
        {
            "id": "b7069055-d848-4af6-894b-a89ad419568a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 370
        },
        {
            "id": "a687e1ba-1755-4fb7-bf42-c670100af116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 372
        },
        {
            "id": "81e0692c-dde1-482e-9b6c-359fe136bb48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 374
        },
        {
            "id": "857de52c-b59c-4e24-ac8e-991d6d22c744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 376
        },
        {
            "id": "71d243f9-ffbf-4d0c-938e-1ecf599f66f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 416
        },
        {
            "id": "72ac5f79-54e0-4b29-971c-49c73f45273b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 431
        },
        {
            "id": "6cd71328-1183-44b0-bb45-c7253e3e2a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 510
        },
        {
            "id": "ba302cb2-fcac-4cc9-994b-1f49d7f4f5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 538
        },
        {
            "id": "a13ad4a2-1756-4d3b-bae5-68c412ae19b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 920
        },
        {
            "id": "121b9f6c-7eb2-40da-bd22-6806f285215b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 927
        },
        {
            "id": "74fa213c-00de-4156-b4aa-133710ebc939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 932
        },
        {
            "id": "7a1bbab8-6817-4b60-b940-42a487af9cf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 933
        },
        {
            "id": "a5ec95bd-5437-4700-8c11-2a9ca8c33916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 934
        },
        {
            "id": "bb24f836-faf2-460c-ac9a-136ebc039758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 936
        },
        {
            "id": "2aff1e26-c144-41ac-aba5-51542a059522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 939
        },
        {
            "id": "2f39da3a-c775-4917-bc7c-acfd3ed2e0ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 952
        },
        {
            "id": "27069267-e69d-4425-976d-cb848455189c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 978
        },
        {
            "id": "8a3fcf56-aaf4-443b-806d-7e853e810f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1026
        },
        {
            "id": "fd7c845a-e6a9-499d-80ce-62a3a67aef5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1028
        },
        {
            "id": "1b29630f-cd88-4266-9f94-18c817df7c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1035
        },
        {
            "id": "bafd6f9b-c26d-479f-9a1e-b85d4993d103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1054
        },
        {
            "id": "6a27aa9f-1d16-46a2-bcbc-58ad92f99b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1057
        },
        {
            "id": "f95ff6a0-e838-430d-8774-44478ceecf4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1058
        },
        {
            "id": "bf577653-a7f7-4b6d-b491-a0c87778583d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1060
        },
        {
            "id": "dfe1ccbc-95b4-4af2-a85b-b0c1fb829a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1063
        },
        {
            "id": "9e87bdd2-97e4-4bc7-baf7-3cf50bb8462d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1066
        },
        {
            "id": "32652817-b73d-4f74-bab5-0e5cdc920212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1090
        },
        {
            "id": "07bdb5b1-a55c-444f-a8c9-c2b9199a4b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1095
        },
        {
            "id": "250c7083-8a04-4f41-9345-3462409b6fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1098
        },
        {
            "id": "dd12d750-78c4-4b7c-aa7d-a58c5ccb5aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1120
        },
        {
            "id": "c8bcbc56-0cfa-406c-826d-53a387481904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1136
        },
        {
            "id": "ddee8705-b51c-4753-8d9d-dc2da3106af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1138
        },
        {
            "id": "bae038d2-111c-458b-a28e-d7529e8313a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1140
        },
        {
            "id": "efd971c8-1046-4416-a6aa-7e0086fca48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1142
        },
        {
            "id": "a311a5af-d8ca-4f59-80ee-f61074ca1a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1144
        },
        {
            "id": "c479ef39-4619-4566-bba2-fcc03652e874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1146
        },
        {
            "id": "3fd71ee6-f27b-4480-b3ca-348c52fac447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1148
        },
        {
            "id": "d7d048a9-6bdc-4fa3-ae9a-7dfd498c1049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1150
        },
        {
            "id": "8a0da3b9-8f01-45d9-aa64-97ebd4598fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1152
        },
        {
            "id": "1b3bf4bd-f924-4090-9c38-2aa9001e8167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1184
        },
        {
            "id": "368a4b50-41fe-4ad4-843c-d986a1b46fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1185
        },
        {
            "id": "03f84fb3-cff3-4970-a140-383ff617767d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1192
        },
        {
            "id": "8084f60a-6838-420e-8d9f-7c790645bbe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1194
        },
        {
            "id": "0e95ef66-f564-4967-89c8-89af414681fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1196
        },
        {
            "id": "b3f1d22f-ff94-47d8-a22b-747d350e95be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1197
        },
        {
            "id": "938647ea-e1df-4312-871c-1370c3ae0b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1198
        },
        {
            "id": "8b358922-2ef8-432a-a6ad-7061ea23e737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1200
        },
        {
            "id": "0e0bfdfe-76d9-4365-8917-5b0bbd46fd7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1204
        },
        {
            "id": "aaafc88a-f438-4c8a-9c62-8d92813d9de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1205
        },
        {
            "id": "f930649c-659e-4e9a-bdbc-82f73251fd77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1206
        },
        {
            "id": "82d34582-a11b-4ab3-8c08-94721cdd0db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1207
        },
        {
            "id": "4ac3d672-6253-4084-a28f-aac860377371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1208
        },
        {
            "id": "9420c6ea-3daf-45f2-bc30-bb40f0b9a784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1209
        },
        {
            "id": "869dba26-69ab-4e72-abcf-67363c013515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1212
        },
        {
            "id": "09cc7aca-b566-420c-ac8f-d067841fd1d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1214
        },
        {
            "id": "aa5a0ac6-fda2-495e-811c-db0b6febb048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1227
        },
        {
            "id": "bebfa76b-7f12-4910-a651-f89ebe3769cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1228
        },
        {
            "id": "7f369629-6e89-48d2-8726-d1d7d4677c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1254
        },
        {
            "id": "b89ed30f-20b4-419c-b5d5-98cf0d7e87f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1256
        },
        {
            "id": "bcfbc599-cebf-4ff2-8dcc-178e5f0431be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1258
        },
        {
            "id": "6a27b962-9354-4b9d-a511-3ac4372d584f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1268
        },
        {
            "id": "7519a88c-c180-48fb-8923-b36904335ad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1269
        },
        {
            "id": "b1652e15-eb0d-4518-9b0d-3f125db70cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1284
        },
        {
            "id": "c89cd00c-e7c2-4648-8ffa-fc5e59cf36a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1285
        },
        {
            "id": "01c9f549-a200-448e-b1d3-179832c81b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1286
        },
        {
            "id": "edea0a79-a1f5-40bb-a780-f3fa7bd69a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1287
        },
        {
            "id": "e3e5f992-48a8-46e9-b58f-4d91782a2f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1292
        },
        {
            "id": "7f299f7b-caf3-42f4-b9eb-eb5c388bf550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1294
        },
        {
            "id": "f21fbd4c-de91-488e-9e5b-6437fefb7c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1295
        },
        {
            "id": "d39dec73-0670-4abf-99c8-a864844f3402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7808
        },
        {
            "id": "90e2136a-c91e-42fb-b213-c208fe5a7361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7810
        },
        {
            "id": "4db9062c-7836-4e2d-9264-cf4cfcf2ff1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7812
        },
        {
            "id": "e9bb5551-7c2b-456a-b595-378bc4e9f9e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7884
        },
        {
            "id": "ffeafa9b-1bba-44ed-889b-0cdd3361631c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7886
        },
        {
            "id": "e30e08dd-dc67-4abd-9464-f2215afb5573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7888
        },
        {
            "id": "b9c683f5-a78d-4949-8f28-b2737aebde65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7890
        },
        {
            "id": "2a33c69e-4d27-4a0a-aaf7-47213739a617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7892
        },
        {
            "id": "9ba7255a-e9d2-4b02-b14a-1a05dbb24a93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7894
        },
        {
            "id": "7c8ddd0c-0c84-4230-bb04-7e037c1a039e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7896
        },
        {
            "id": "d10833d4-2a01-4f38-8a0c-32ea998c99be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7898
        },
        {
            "id": "f6c14644-1535-4041-8fe7-e8ee50d1904c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7900
        },
        {
            "id": "9e3a73a1-f2c0-4679-bb23-06cb038bb4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7902
        },
        {
            "id": "c8b21728-63ed-47a4-85a4-6a8bde405119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7904
        },
        {
            "id": "875d3444-ffa8-4dee-b088-3cf32ec4a793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7906
        },
        {
            "id": "083b1a76-aa66-49f2-aa10-37821091d0b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7908
        },
        {
            "id": "bc930ce5-9f53-493b-a005-0ed961241f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7910
        },
        {
            "id": "1c3e1580-9b49-41f4-97cf-01b96e981749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7912
        },
        {
            "id": "40af7a10-2c9a-4f3b-a19b-e528b1adecfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7914
        },
        {
            "id": "b766186c-e99d-41e5-831e-efbfb6dc6784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7916
        },
        {
            "id": "b6304041-9f1b-4c4a-bfeb-c4342a5047e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7918
        },
        {
            "id": "e124261c-490f-4e77-851e-b4c9f18fc84a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7920
        },
        {
            "id": "c44a507f-14c7-4f84-92ca-09e35e96fb5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7922
        },
        {
            "id": "c3e4a86a-63b0-4b26-a8e0-10a422b6a94f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7924
        },
        {
            "id": "5a805a8a-c5b4-477a-9ff0-58286229625f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7926
        },
        {
            "id": "ced26b0d-60e7-4a41-81bc-b0b00ca6e8c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7928
        },
        {
            "id": "e9737d64-5865-40bc-86bb-a56e00ff93b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 34
        },
        {
            "id": "633a6f5b-c03b-45f4-ac3b-65ea02d9eda3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 39
        },
        {
            "id": "1e299cb8-d90e-40b8-8431-60a54a135e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "93bbe99e-aedf-4e14-a8e9-862fdb49e26e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "ae534eb5-6dc9-46e7-8af7-eda21964e0e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 65,
            "second": 74
        },
        {
            "id": "97a5b38a-3364-42fd-b510-8956ff6b6b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "a4e2d417-8e1f-48fc-b759-63ddf0f63038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "9e93d54b-d6c9-4714-b0e1-fc6dca84e48b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "51c84e04-d96f-4eb7-8bf5-d6f6b526c2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "09bfecfb-14e8-4b1d-a299-87edb6f7b7bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "07f3b421-9757-4c2f-9995-889408513c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "efe064e0-de71-453d-8948-ad7865383594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 199
        },
        {
            "id": "81b385a9-f2c9-44db-a48b-2af59e893b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 210
        },
        {
            "id": "f6fa581c-b369-4041-95a4-15156990514f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 211
        },
        {
            "id": "6b148341-1f56-4dff-8612-302bb7f27603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 212
        },
        {
            "id": "af624695-51bb-46ec-94ec-30fa52d2f68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 213
        },
        {
            "id": "a69cd6a0-6276-4749-ad9e-a20b9c8d41ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 214
        },
        {
            "id": "b24e33ec-7bd9-4e82-8f5a-f67163a8cdba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 216
        },
        {
            "id": "11695a56-acf4-4162-9a46-248c2e5697d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "185fda4c-e5cb-46dd-a986-3f44790967f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 262
        },
        {
            "id": "e773b6b7-08d3-4e76-bec4-6b19c6a7fbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 264
        },
        {
            "id": "f945ebc9-b970-40a5-94ea-ec835ba2b1b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 266
        },
        {
            "id": "bef6e72b-cd49-407a-b8bc-d17d0064b76f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 268
        },
        {
            "id": "24e80f68-8450-4f1a-818d-df052eed2276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 284
        },
        {
            "id": "a0f3184b-8a71-4336-a045-f82ea875fe43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 286
        },
        {
            "id": "208b2b82-295e-4d12-affb-1cc683d6b8f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 288
        },
        {
            "id": "e21a58c4-c167-4f26-85ed-2b2dad8fd4ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 290
        },
        {
            "id": "1737db00-4232-4eec-b1a8-b388ac97f679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 332
        },
        {
            "id": "9fb3c43f-9488-4de0-9e5e-9661a072902e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 334
        },
        {
            "id": "c072d430-4db5-4f99-8660-a1b56653aab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 336
        },
        {
            "id": "6e2546f5-c999-4eb7-bfb7-bb8e1e7bfd52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 338
        },
        {
            "id": "643d9824-aeef-41fb-b1f2-ca12042926b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 354
        },
        {
            "id": "b2db8c61-a188-4806-ba76-fb30f9ea8a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 356
        },
        {
            "id": "1d186a32-b5a8-4030-a0c3-21f39812589c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "b208bf9c-c13b-4c5c-8455-8e7be90b0a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 374
        },
        {
            "id": "6db8ef2d-821d-4a13-be20-c69614151f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "bd918834-bfa3-4187-af41-7bd7ae3962f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 416
        },
        {
            "id": "4f81b84c-f8ff-490c-bb21-d32e2b1cbe45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 510
        },
        {
            "id": "5b1b0e05-2448-42ac-bd8a-ab9a78e1c0fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 538
        },
        {
            "id": "4571d61d-c654-41ea-a0a4-2dbe7bdc8ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "3c0c2796-5642-45e3-9ecc-04b31c16a76d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "3757423d-95d1-4786-879e-2b0f21b64e59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "8b0bd345-ec6b-45e1-be93-66bd1cd1b6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7884
        },
        {
            "id": "ab063b66-1251-44c4-abdf-6948e2b3b4e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7886
        },
        {
            "id": "30d55a87-ab92-4407-8efd-e41bb3b6cc21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7888
        },
        {
            "id": "57278c6d-7cd6-4471-85a4-57b9cd9e69c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7890
        },
        {
            "id": "3ba0b7c3-2d17-4db5-9db7-7e19302a4018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7892
        },
        {
            "id": "9a2d198b-b829-45b8-9c75-4943291305be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7894
        },
        {
            "id": "04f0a7ae-f4c0-4d9c-ab79-a921959768e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7896
        },
        {
            "id": "99d47b3c-ea6a-4baa-9283-86a618fed3c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7898
        },
        {
            "id": "2f88c95a-3389-41ea-b2e6-18248a5abcc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7900
        },
        {
            "id": "7f11a219-b20d-4a97-ab4d-2304d1c60c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7902
        },
        {
            "id": "bc465776-d852-445b-ad2e-1e2a1484b606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7904
        },
        {
            "id": "28f32157-f0cd-4bf2-85d1-b4e11cfb1d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7906
        },
        {
            "id": "3e5164fc-3e90-473c-a402-9a16f4623818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7922
        },
        {
            "id": "2eb43674-db3d-43d9-8c04-77d223d15507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7924
        },
        {
            "id": "5ebbe7e8-21a5-4902-be49-fcb047caf2ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7926
        },
        {
            "id": "96e2af6e-970b-4c5c-92ce-f949c2c774e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7928
        },
        {
            "id": "aefc65b4-122b-4359-8381-0848ab750d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "1f78b94f-7685-4a4c-b63a-7b0de6d79526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8221
        },
        {
            "id": "9833e5a5-13f4-4af9-a918-db2fc8d57d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 44
        },
        {
            "id": "be428637-b8c2-4588-b5dc-7bbd4aa5e3e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 46
        },
        {
            "id": "6ac3b349-1156-4f09-9d44-5fde1afc2730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "0573091d-6ae9-4e16-8302-b2805a96a45b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "9fe0008f-9463-4f33-b580-81aef33b0b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "711eaf1e-5198-4347-954f-686ece775cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 192
        },
        {
            "id": "3268ebc6-5c43-4f17-bbfe-066a993c729c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 193
        },
        {
            "id": "488d3a31-4525-438c-aaf7-8fc70eb12d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 194
        },
        {
            "id": "ecfd7be0-31ad-4619-971d-94d04e031808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 195
        },
        {
            "id": "7d4a111f-e229-4ed3-b941-2e0af985842e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 196
        },
        {
            "id": "a06519ec-4424-4e37-bf99-3ed5ba228fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 197
        },
        {
            "id": "cd7cf37b-468d-4999-b644-1e95943d0d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 256
        },
        {
            "id": "a34dbe21-298a-42f7-a448-446786119f4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 258
        },
        {
            "id": "7eef5be9-f069-4c87-b751-724cb7ddc6d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 260
        },
        {
            "id": "c6ead49a-f4cd-474d-b5b4-cc74c9f2f5d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 354
        },
        {
            "id": "d64b3e67-22c6-4145-98eb-dc775fbe83da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "02b632b9-216b-4cde-8ef0-65a5704cc02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 506
        },
        {
            "id": "16d88f07-c2fc-4698-96f6-6ca37654a410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 538
        },
        {
            "id": "54481a6b-df92-4142-9d16-515904a0d5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7680
        },
        {
            "id": "7b3dfceb-c6a9-4c79-9a50-5ab0f50d5cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7840
        },
        {
            "id": "1f1b5d94-c11b-4f48-a98e-afbc3d9fa158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7842
        },
        {
            "id": "00a8975d-caa3-44e6-84bb-358c9fe26037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7844
        },
        {
            "id": "7a4fe18c-827e-4ecb-a1bc-eabe1db63762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7846
        },
        {
            "id": "e8a41205-13f2-476a-b86d-0bb9c0110159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7848
        },
        {
            "id": "67812a3d-c793-4f45-9b12-e1559e7cc5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7850
        },
        {
            "id": "aeb00ddf-534a-408a-b658-9a804e643630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7852
        },
        {
            "id": "7cb684a5-6128-4052-acda-f5cef4c30021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7854
        },
        {
            "id": "b0d7706f-e12c-4c6d-98e1-919d818555d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7856
        },
        {
            "id": "20ad3f34-78b0-45c6-b245-8f7f90f9ae4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7858
        },
        {
            "id": "aa9813bd-5fef-42af-9993-c7f17809b188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7860
        },
        {
            "id": "bfbdeca3-80a1-4efc-91da-e89c321ae277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7862
        },
        {
            "id": "70fb84ab-9c3c-4297-a352-49e9cf871177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8218
        },
        {
            "id": "e008e69d-a9f9-4337-bef6-4e0a6e70e72b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8222
        },
        {
            "id": "efc85b22-a517-4e1e-a289-f41eb50239eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "56bf20d8-3ce3-4f08-b7c0-1b7d367fef8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "a67dc1e3-576c-4af1-a799-c48421c54541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "c57c2e93-b110-4c44-b68c-969aba90f1c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "eb4fd6a3-6f83-4b95-8a98-717e7be949a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 199
        },
        {
            "id": "6f103377-6ee6-4bd9-a87c-d70464872bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 210
        },
        {
            "id": "963e23b6-1051-41e7-8927-9ad82c69d5af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 211
        },
        {
            "id": "d5d2e95e-2ade-41aa-83e7-54005ef793c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 212
        },
        {
            "id": "be9df5d4-1150-4c54-b366-f8a74cfad9e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 213
        },
        {
            "id": "99ec5c40-245b-4d31-b60a-7a4e1e229b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 214
        },
        {
            "id": "147427ce-6515-48b5-9b2c-7ac1a475e4d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 216
        },
        {
            "id": "cea96f08-18aa-4886-9503-f6f1e77d4f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 262
        },
        {
            "id": "8b658c6b-67f6-4799-9d85-7403e397c439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 264
        },
        {
            "id": "070362d8-5d4a-49b6-83b9-0921c91f8c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 266
        },
        {
            "id": "c2840852-c76d-4022-a306-548d02d488b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 268
        },
        {
            "id": "ed10d066-c985-44ad-bdc0-9c381dd5c0da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 284
        },
        {
            "id": "c888a6a5-ee5a-4f09-a6ba-841800ce1ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 286
        },
        {
            "id": "32ae6b5d-5dae-45c6-9dcb-b210dd4279fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 288
        },
        {
            "id": "3094d58c-b9b0-4c64-95dd-f6b9c591c88b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 290
        },
        {
            "id": "62b85c0f-7377-4fa4-a7f7-e8892d8057be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 332
        },
        {
            "id": "68100d24-07d9-44ec-a1e9-adb05099b4a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 334
        },
        {
            "id": "4a7dfdb3-b574-4eb6-b35f-32425473c11b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 336
        },
        {
            "id": "603657b1-c06a-4be0-b214-dee26587c398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 338
        },
        {
            "id": "b326541d-f5a7-477d-8e9f-1984c4e2cc55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 416
        },
        {
            "id": "207dcdef-5fcf-4c61-87ea-187b3a298736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 510
        },
        {
            "id": "358bf304-967a-40e0-88f2-bb20900de5d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7884
        },
        {
            "id": "42ddaee5-b33a-435c-bc25-d4408d1f9121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7886
        },
        {
            "id": "2ff2464a-7c76-40dc-81b4-10858d316d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7888
        },
        {
            "id": "689655e2-1b64-40f2-9c83-636105d42dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7890
        },
        {
            "id": "f9457903-ba5e-4be4-88d1-8b43afbbc48c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7892
        },
        {
            "id": "2fda5348-54f7-42ed-a615-9c60f9f568a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7894
        },
        {
            "id": "4fcda594-371e-4e34-98ce-0e7e1e53a805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7896
        },
        {
            "id": "ce796859-1efe-4d78-b900-6db2db5f70d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7898
        },
        {
            "id": "ca5990c5-441c-48fd-97c7-86fd9db550ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7900
        },
        {
            "id": "19e84fbe-8774-4388-af03-dfe2324a58e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7902
        },
        {
            "id": "c7b24836-3a1e-4704-89b9-58d95310c456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7904
        },
        {
            "id": "532857ea-65c8-4cb5-8646-d6714a174aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7906
        },
        {
            "id": "74754b0f-4a9b-45a4-b58b-f0dc18ab8c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "4352a928-4fa4-4e48-bc7b-3fddddb011e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "fb278d38-8d47-40d3-b6f1-1e6ab40b22b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "0fbb0c32-3940-4da1-8326-f198a0cfbb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "a2370c45-3488-4d18-b166-dd973782a08c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "f7b521af-4e5b-4d05-89a0-1a46d30f392c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "d5ddd93e-ad9d-4b12-8aab-65bf9f65d4e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "834dd816-68fa-44c6-b75b-78050f71944c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "94e43025-2b7d-49de-9706-f31deb9e6ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "00bce164-16d2-4c4e-afd9-afa6ed8c6966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "6308281c-fc95-4991-84e2-ed6b96a2edc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "bd8aeb7d-5e8a-4e6d-ab98-e369de9a0993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "d8e78890-151e-425d-9526-60416fadfbf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "4b4041c8-21ca-43ba-b1b0-06a24a8e17e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "96b1e042-60f3-47cf-b9c7-200d3283abda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 354
        },
        {
            "id": "35f9e753-4086-483a-b6fd-bdfb272e1e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 356
        },
        {
            "id": "38807d91-b468-4ecc-87dd-2676c134e948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "35d5936b-eac6-4df9-a5de-b9c58fbd01c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 538
        },
        {
            "id": "1fb4ea76-67e5-4825-9eaa-635974d426d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7680
        },
        {
            "id": "d9c04d58-eff9-4abb-8379-972c93663265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "9ca0ef5d-14a9-4328-896c-be2141f3b419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "f3583b54-a3b1-47e6-a8b5-72d69695fbde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "2dd7821b-61f2-4fac-b6c7-0629cd5dae0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "b15a0a7b-11fd-4479-ae33-c05a1dcce6d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "972e1612-9c82-4fae-9039-a19fd7b203a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "b2e0e062-9aaa-4a20-bef8-54bf392846e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "2511f91c-0d3d-4e5f-a2aa-807ba8ab30d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "e9d8c856-f364-4ebf-8e66-ee874b48b2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "2eb95b51-eb15-4870-81b9-61d79759992d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "8640bd89-1d7c-4c87-b0d0-c8880767cc97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "08b982b0-a0ef-4e0b-92de-8a3ee83da3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "781e01c8-7a7e-4aa0-9166-ab96a57089b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8218
        },
        {
            "id": "14be2eab-652f-46a9-a30a-5c32e713346a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8222
        },
        {
            "id": "a41a83ed-62dc-4483-9537-2cea2fef2bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 69,
            "second": 74
        },
        {
            "id": "b5a1a9bf-78bd-4a14-9e36-4d4de5963525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "743d56fd-b02e-4b0a-a689-14c622b4c6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "b6aa03f3-e72b-4a1d-bf24-6d3180f27eea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 63
        },
        {
            "id": "d508fe60-2f2f-4d62-902c-805e7d7d39ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "a0d4e771-a8e9-4c70-b05a-fa280972c898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "e4b0bad0-f0d6-46cc-9734-d914a0a95054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "42108a91-0677-4fe6-b643-9c63a3737449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "f4364e54-6f09-41d4-b5b3-b9dafa1f6f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "e3190815-07cd-4291-9ae3-bdd93786649d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "88fd726a-315d-4a25-9a8d-21db3ddee6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "42181930-d8f1-47ac-8df4-c1c9a202fa93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "bb8fab72-12c5-4de5-ba31-a00ac98b9a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "f03cecbd-fb43-4070-9291-2263d855edb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "73a6dde2-c7ff-4c4f-a3bc-0d92147d72da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "b8177b84-d536-4e8f-81ad-273a55ca4b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7680
        },
        {
            "id": "ce69a9d7-ed45-4cd1-950f-d07232c4b254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "e0946276-8768-4d7b-beaa-41715c2e6a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "2329baff-48c4-4ec2-a38b-bccf4414fe99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "e1851ba1-676f-4943-8785-fd1a7d62c059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "f11223e4-b875-423b-908d-b3254b934caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "76c21555-2222-4ac0-887d-f162ad7313e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "5ad8f6c0-1534-4513-9532-2ba930e13485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "0c3a5fd2-a39f-4c0b-8496-e21a6a897344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "3476afee-9da5-4176-8a46-ff714ace1c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "2b64e3ac-b522-48b7-8fc5-a9269c982933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "0230f0e4-c503-4079-8fda-4659ea18de74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "7c156aab-b549-4d1d-85e2-ce388d4008d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "0b6b5dff-19b3-48c7-8dac-5487984194dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8218
        },
        {
            "id": "e88c2809-2380-47dc-924a-782026f1c2f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8222
        },
        {
            "id": "3a13da2e-befe-4ea5-ba06-603cd89af758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "03b83506-065c-498e-a2e4-dd3985244e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "3e1505a5-65e0-4d09-8dc7-9cbfcac52e18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "b41243a4-43ac-48d1-829c-246160ea23e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "13a7c735-101a-4042-8069-15c103ee1118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "ea2a7af0-7ca2-4331-b072-7c89134458ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "63e8f934-b629-47ee-ae19-4c6cd62077c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "27b86243-1e02-463e-9bca-a0fbd8af89c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "1b54f963-836a-412d-99fa-538a722cc894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "97e8becf-a394-4934-8ffb-719c255237bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "07c42e83-488f-476d-b027-1c3883b5de7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "ae44d4c0-adf0-4498-9c05-c96d00ed045d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "f2859522-688a-414b-bea4-2ce36aebe5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "7b581291-aefa-46bd-ad81-fd61ef510210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "ba8bf526-44aa-4eb1-977c-79f78ebb6964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "890ef89c-790c-49da-b324-82b8f7bb1ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "16fa1409-2b9a-44d1-a42a-bced1e912470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "2b3c7fbf-622a-4d30-8692-4345e4afa64d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "1e5ee90b-e095-4ba1-8271-220467dc19c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "7b64e8c2-ccc0-4594-b595-8489f9d4d7cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "1008a451-bfca-4cae-94bf-4fabc2b65b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "557ca22f-dd7c-45d2-8116-82214ef6e75b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "fee31610-7146-4a4e-9fbf-8e0329546050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "c61d92ea-ade5-440c-8cf5-209c74a124f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "2ac0790e-1fc7-4cff-9c07-8c956d71e22b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "07e7b28f-57a6-46c6-80aa-28e7ab91b24f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "1e892128-c076-4d4f-a542-732a7e83b6aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "0055958f-9feb-409d-a0df-547a907f6401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "48956115-419a-4fb8-aa16-f70969788f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "e3b43860-2db6-42bd-99f9-bec0b784183f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "09cdf086-b51c-41b9-98a4-3324fa5624a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "455af795-4f45-4efa-bc30-5f35d2319101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "a54ed9ea-1df7-4a17-9081-16e04738682e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "1ad64bcf-ec3a-425c-8e3c-84929e59bbb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "aaa53ed1-c4d0-440d-92d3-8cd76831a977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "3e610636-1ab7-4dfb-8eba-b5b6aedb3cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "3f4c6141-2375-49f8-9113-1b3965e6157f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "c21593cb-a18d-4960-83ee-0403e81a0172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "83376a8c-b20f-4573-9bed-3b0a4cac9613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "629216e6-3b35-4509-a437-642a34a23157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "285ec125-6ac5-42a9-9282-f774bfda0f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "a761a5b1-87ac-4d3d-ae4e-373d41563f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "9e470a6e-2e83-45cd-8e0f-2b442ea9f4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "45b05135-2619-4a46-a2bd-06444bc35e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "4f2feab0-39cf-417f-98b6-0c98d380a2d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "346b5078-0d38-4333-8197-3c4402f1c66c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "ad12e31c-219d-4717-9fc3-f302016586c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "89c00f6f-8d14-4c6a-a327-fa550743bb17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "c99ba7fd-ca5f-4b06-9402-b670c2fdd9ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "be1d91db-c411-411c-987c-a60583e247c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "61fa22a4-2c25-4a26-b461-d5b7ec6b95e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "1aeeaa3b-2368-4355-97dc-524b903e62ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "f6fb19a0-62cd-4f29-86da-cd05e577154c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "47b3c8ef-e9d4-4203-93d2-1a1b10f578cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "e733dd03-4156-47fb-9e67-4417eda96f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "1eb4cde5-a7a4-4b59-a0b4-bc5b8c85dc65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "6172fcbe-f0df-4b98-8208-ae98b68ae4a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "94eb59c7-7d7f-4679-bfb8-f0020f19bce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 266
        },
        {
            "id": "85034a19-b353-4fef-b69f-021e7ab19562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "58ca8445-55e4-4a99-9c8e-d0409ed2e997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "6293b6ec-83d8-4a7f-8fcb-dd2827d4cda0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "39b4777a-a953-48aa-9e7c-370e5d0fc2c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "c42a592d-30a3-41f8-b036-fd883ea5d7e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "6a7a7daf-7176-4de3-a591-acbe4ada7fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "c6cd976f-4ef1-496b-9202-44f318adfe3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "77894585-92b1-46f3-a475-93c7c9799704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "133e8075-597c-49cd-adcd-3ad80c093aab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "7bb8852c-f43a-4599-b04d-7211c2e06a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "4490d18d-5e8b-4cab-b347-5bd3644ee2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "75593d0e-04c1-46fb-ab46-fd69ed66fe52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "8fa1a745-0425-42f8-a54f-a11d636be417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "6f1e04db-bdab-48f2-8f12-c4e133f7f971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "15d1fa6f-863d-461f-ad8a-ed3c8f85cb59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "19ba7515-47b9-4ecf-b080-0da4140b4417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 510
        },
        {
            "id": "5100cd5f-9ff3-4103-8f7b-ad8d78a02ca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "97474edf-4fd3-434a-a9d7-bbc1c633b655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "a93665e4-4fbe-4f56-a99a-7b19d79d171c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "23f79db6-18c9-4a05-8f56-67955f73ec65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "1f53b5ad-c40f-4140-b33f-90e82df3daad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "06adb10d-3910-4d6b-9e1f-b10c0891fb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "fc4b3726-6030-4d03-b734-356eec11b143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "bf8635f5-81b7-4432-b2ae-3222f40e6bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "0d970a23-72fc-45f5-b631-157e952d0fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "71573881-f3e7-4920-b8ba-33466118ba71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "af09da92-f4f3-4da2-a018-30f1769fe6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "3b5203a9-e9d2-44e8-978d-12ee3db521bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "c735fcb1-6d9b-4017-a475-c5aaf565241b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "78058c08-330c-4594-af58-788e229fd743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "e20f7132-7883-43cb-a522-9b54fb61bf62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "be66b9d1-66d2-4163-b310-d99e63eea49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "17243948-b776-4de1-8f4f-dbc0b40640fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "cc28d9dd-744e-41a8-8782-0b76e082bb23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "f7fdd9ca-8913-4acd-b12d-b10038aa2638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "b4917431-137e-4053-953f-0edb2b22a166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "937e025a-6a77-45f3-9b20-10048106c601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "2cef9525-c138-4f23-8cf8-a096c1fc81cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8221
        },
        {
            "id": "ee17cbe5-5cc3-4cde-9086-c300edac24a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "847af7a4-88f8-4614-a4a2-40e1331de7fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "f1d0af1e-8c83-4744-9805-29ffe6f5f7c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "9b0a9425-d49c-49fd-bab6-a6b40c77e1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "29dc5d0c-6411-457f-bee1-6971477a56eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "9a7e46b9-66ef-4dc5-9d2e-0bccd7948366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 192
        },
        {
            "id": "75627f38-d2c6-4300-a926-336648f89c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 193
        },
        {
            "id": "b3ab15d9-8e0d-4586-8fc6-065f51af26a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 194
        },
        {
            "id": "0bc748e9-607b-45d7-bde2-5d611de792eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 195
        },
        {
            "id": "05a5d130-1662-4678-a4ee-10a5766a6c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 196
        },
        {
            "id": "0d14f891-39c8-478e-9db5-bf87a432b436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 197
        },
        {
            "id": "08140d0b-be7d-466b-9096-fc69b5c34898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 256
        },
        {
            "id": "7b99c2d1-e815-4e43-9517-931240e0badc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 258
        },
        {
            "id": "d03d6df2-0804-4871-966c-fe79d6be05f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 260
        },
        {
            "id": "884db603-acbc-4216-95d0-ba134242aa8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 354
        },
        {
            "id": "c8502af8-a2fe-4ce9-8d6c-faeee1311c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "3c9a4402-5c44-4aad-81e8-056923499c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 506
        },
        {
            "id": "460a9a55-142c-46b3-9cc5-4fa869b2d3b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 538
        },
        {
            "id": "7d245d9b-fee6-4a94-9d32-b46c1aa96386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7680
        },
        {
            "id": "ad300917-f27f-4d7b-8727-01b8015f81d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7840
        },
        {
            "id": "2da72715-d656-4248-af2f-7b88ba1a3264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7842
        },
        {
            "id": "6b603bec-ef9a-4011-8a30-a4f598cb3beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7844
        },
        {
            "id": "54809679-affd-4057-8fb3-96d28bf1aa88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7846
        },
        {
            "id": "4b9fb5ec-ce8a-4e53-bca6-662dc0d7699d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7848
        },
        {
            "id": "bc41921b-0b00-4698-bcce-b11e7e722366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7850
        },
        {
            "id": "05c6a14d-ad8e-4f27-983d-6ac752b21c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7852
        },
        {
            "id": "be20cdde-0c23-414e-a32f-f1b775e35288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7854
        },
        {
            "id": "e80ad9b6-41b8-46ea-bd3f-ac0e885dbf2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7856
        },
        {
            "id": "54109c7d-d882-40a6-92ed-22dbad9b4a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7858
        },
        {
            "id": "088b01fc-66d3-43df-9195-355a8577f5dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7860
        },
        {
            "id": "a453089f-69ba-486a-85c9-14e8b878aa65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7862
        },
        {
            "id": "c0819bfd-828e-4dad-8730-a3678b04b1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8218
        },
        {
            "id": "003ab735-2989-482c-a71f-4232e370c709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8222
        },
        {
            "id": "961822c7-3f5c-4727-aa9b-5ed801f9aa91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "708bac50-be30-4a97-9a55-b3d93e9bb7c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "3d8fbd4b-5cb9-440e-ba6d-ad81dbc8b29b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "ab97f60a-ddc4-4337-b4b2-71cd89b0ebe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "381bdf6d-6549-4de1-b313-d23634a130b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "c2c0651c-a7ab-4171-938e-2c32a093e7be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "73c2afa5-a199-4e9f-a813-09be5a752021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "35272579-1a2b-4115-8920-cc2bd95be6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "351fa1c8-54f3-42da-aa0a-dcbd37870ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "e0c5fe52-6eca-4565-84a1-297f333021b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "2d7059f4-2351-498f-9ada-8231bff8940f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "d6319e9a-074a-49ef-aaa4-38213c4f21ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "f71b795e-0180-4ebf-abdb-479ce5877303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "e03b6428-4b5d-4ca5-a9e6-b41703dbfe27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 506
        },
        {
            "id": "4a73ebda-1861-4ad2-9c73-59001848da8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7680
        },
        {
            "id": "0d705b0e-480a-4d9f-a3ed-60fb86874ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7840
        },
        {
            "id": "bd9137e2-e0a1-42e5-95b0-adfc84b92382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7842
        },
        {
            "id": "fc7e441e-d0c5-4e77-a2e4-e602a8052e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7844
        },
        {
            "id": "d5b0d382-b146-42ee-b855-996fc1e5cacd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7846
        },
        {
            "id": "249daa7c-685b-4ad8-82a3-7b1c083c5e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7848
        },
        {
            "id": "fe320a7f-63c9-47b5-962b-65ead02f4910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7850
        },
        {
            "id": "0a9d74d6-b783-4885-ad29-8349d1388d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7852
        },
        {
            "id": "464f703a-be98-4328-9607-083fff297ca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7854
        },
        {
            "id": "3c030eb6-26b4-4d3b-a97f-74023fcdf94a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7856
        },
        {
            "id": "dffd887e-03bc-42fd-8ec0-512692d6901a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7858
        },
        {
            "id": "82bbd4ea-1962-4acb-bae7-0de379c014b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7860
        },
        {
            "id": "ec1fc1d8-5310-41c6-99d1-c2b530bac5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7862
        },
        {
            "id": "f27c9561-a58a-45a5-bcc0-8558e5728766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8218
        },
        {
            "id": "4b6a5650-eb51-42b2-947f-940076fde819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8222
        },
        {
            "id": "468fb91e-5195-447e-bd7d-e43d7f2ffeba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "5a4df060-4e2e-486c-9b62-e21ae1126517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "37808f34-86c8-4427-90d4-84e90e06f32f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "2e9aa855-a6b1-4e81-9462-538797976ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "47851277-8357-4b26-926b-516e80514c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "7848545f-f935-423b-b81c-d3d298429efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 192
        },
        {
            "id": "a363cb41-b131-4dea-8102-a5342b617364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 193
        },
        {
            "id": "50ffac3a-cc69-4215-a054-ae4105f00f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 194
        },
        {
            "id": "12378196-a316-4ec0-9436-2f5853adc408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 195
        },
        {
            "id": "2ec773c4-c7d4-4915-ac6d-fd51b40a4d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 196
        },
        {
            "id": "63ebf0b5-3c68-475a-9b2c-57df63edf795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 197
        },
        {
            "id": "1c03e647-5b04-4907-8840-1c4c16c69d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 256
        },
        {
            "id": "f5579ef6-be98-4a4e-a2fe-f07304806dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 258
        },
        {
            "id": "4cb853d5-2a29-4e34-9efd-a6c68b424c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 260
        },
        {
            "id": "1e46581a-3873-4e8d-a2bb-f865259834d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 354
        },
        {
            "id": "b2216062-1a5c-422f-890c-2a8929760b1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 356
        },
        {
            "id": "22a11b89-259c-4b8f-9d32-25d00ec837dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 506
        },
        {
            "id": "439cefcf-bf7c-4e1c-8a27-d5f5d6cd3698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 538
        },
        {
            "id": "0340f010-cc5b-4a8b-b60f-4d1804b1ef7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7680
        },
        {
            "id": "94bdb16e-96c7-4b38-8f60-86da26f8ed06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7840
        },
        {
            "id": "0dbafe6d-1860-45dd-bc78-0aaaa6d37c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7842
        },
        {
            "id": "92552f5d-9259-4526-9697-716c2304a7d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7844
        },
        {
            "id": "00e17953-73cd-4326-86d1-e2e72e3b4281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7846
        },
        {
            "id": "c45ad62b-544b-4966-8b67-8f24f9833656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7848
        },
        {
            "id": "a2b6fb1b-7078-4e7f-9962-922c7bf6c1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7850
        },
        {
            "id": "c407f6ba-7ce4-4994-b306-8f5123a5f704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7852
        },
        {
            "id": "8a5211b0-d080-4490-8ac5-56d7e9f5893c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7854
        },
        {
            "id": "abbf953c-12d5-4762-ac2a-59e36f6e972c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7856
        },
        {
            "id": "3d06679e-e09a-4d63-8f1e-2fc7db83182f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7858
        },
        {
            "id": "0fe77d23-b80a-472b-98b9-62f01c775513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7860
        },
        {
            "id": "562a13e5-5b5a-4d2d-ab42-ccdf42cca97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7862
        },
        {
            "id": "444a2ab2-e272-47aa-bde6-64122d6578e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8218
        },
        {
            "id": "da9b049c-9644-4510-8de8-f9cb178b532f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8222
        },
        {
            "id": "3f74ddc2-90cb-4887-bceb-f2decd1a04a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "201c42e2-6581-4c6d-9e1a-aba474a68294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "96ee1d18-da9a-42ba-b588-42a9b9cc7246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "e70c9c8c-0c22-4428-906c-cd2ac9c7e32e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 63
        },
        {
            "id": "69b41a88-6cfd-4e89-9a1a-7f21b9df31a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "6683da8e-8d67-4614-97ee-7b29ec132800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "67cbb552-bd18-46b0-ad3d-7d3a6f576936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "2b44c738-b9ad-426c-9ecf-3bf89199f392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "84607e1d-4640-4e71-a2f5-d71c96a0418a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "543769ae-c357-432b-994b-58c8bd876016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "4080db2c-7ef6-4412-80c8-ea5955f6f5ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "1df1220f-00d5-43aa-b29b-90813ac8392b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "e1a9d0a7-7c21-4e60-9191-e55595ceec7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 100
        },
        {
            "id": "4b27fb7e-afb4-4e84-9c91-fd32b29b1441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "80aad080-0fb9-459e-ae88-1e2b6acfa0dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 103
        },
        {
            "id": "0c242336-2beb-4b55-bbc1-fd75c21da3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "5da79d2f-4017-4bb3-a5d6-e99566af2c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "44247e04-47f5-4797-98bd-880d7350f631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "f5f1b3e4-a87b-4bfc-bbab-30515b2018d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "b2833c37-9b70-4ec0-83e1-5e3aa05cf506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 113
        },
        {
            "id": "044dc612-eacd-487c-963e-332cbbcb8c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "c3d1b7e8-bb86-4857-a10a-542ccdfeae13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "a7de47d6-048b-494e-8f47-f1ee86d882c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "b1d0dd5b-3bc1-40e3-b373-fdbaba17253f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "3f9882fd-1998-4024-a00d-3e4860f2d441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "a511eb18-6869-4a97-af2c-bab9669f87cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "1fde16fd-4f1d-4a43-8e2c-a3e7519bac56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "e550a307-0efc-40c4-8256-eba297bf11a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "317792e2-75cb-44fb-8740-e760d5ed209a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 192
        },
        {
            "id": "724c49e6-1992-48d6-8740-5771421728ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 193
        },
        {
            "id": "f3470705-7d54-4cd8-a44a-0befee1b8557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 194
        },
        {
            "id": "d70c2e2c-1b9c-4f75-97cb-b120ef7777b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 195
        },
        {
            "id": "3bb398b6-5558-412f-bfb4-561b0da30242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 196
        },
        {
            "id": "57f56fc0-1b1c-4780-9a41-48f9131aa732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 197
        },
        {
            "id": "9e5078ef-37b9-42ef-917b-823fd212a678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 199
        },
        {
            "id": "be14e507-da59-41bf-bd85-e73c55c0ab5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 210
        },
        {
            "id": "591b02fe-d52e-4e8b-8894-236d17d270b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 211
        },
        {
            "id": "e2381ece-6a14-486c-a050-cb4adb5ee36f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 212
        },
        {
            "id": "b5e63ad5-3848-4954-ae6f-69d742ac3014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 213
        },
        {
            "id": "92451057-a87a-4af0-bf22-8031066ab3eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "97aaf6a9-2029-4892-a24b-58bee489c106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 216
        },
        {
            "id": "5b9c17fc-6558-4092-bc67-109694bbaf90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 224
        },
        {
            "id": "271d1026-34e3-49a5-8be7-f1fa0f19d8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 225
        },
        {
            "id": "30afe8a9-f4e5-41b7-be16-6f32993969ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 226
        },
        {
            "id": "b3e7b876-4388-4ba8-bb3c-35c25f8fe56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 227
        },
        {
            "id": "3795dba3-493a-4ebd-8032-62a624180946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 228
        },
        {
            "id": "fa34f6df-90c7-4dd3-8123-00ec2b66ddff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 229
        },
        {
            "id": "859be817-ee03-412b-a608-07795c6e69dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 230
        },
        {
            "id": "6ff48e30-dc85-4d4c-bb88-984fd4b075af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 231
        },
        {
            "id": "f034eff1-fdc6-403d-a3a2-df4ed44a40b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 232
        },
        {
            "id": "d77d7c6c-9bd8-4222-b872-07546c0019df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 233
        },
        {
            "id": "81a02905-a5b7-4b9e-8d11-bae1b8561bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 234
        },
        {
            "id": "479a6c5b-62bf-4b2d-9a44-f315ab2ce8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 235
        },
        {
            "id": "8f567fea-0927-42a8-b85d-88012d503860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 242
        },
        {
            "id": "ed6812f7-e0d7-4d8d-825f-22602326556c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 243
        },
        {
            "id": "ea94ac77-6409-4561-bc55-db4110fdbfaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 244
        },
        {
            "id": "4924aa0e-8fe3-4849-95d2-d8f1b2bcfb5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 245
        },
        {
            "id": "47a351b2-a11f-49f3-baa9-43a146fb561a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 246
        },
        {
            "id": "5bc2d736-0ad7-4f49-a6e3-7ade6a0f9a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 248
        },
        {
            "id": "b2743baa-a6cb-4fa6-82a7-7ad10836e3be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "cda3308e-e23f-4b8a-8822-ac6489b33eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "dbd326dd-0af8-47c7-9309-88e82c9a43f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 251
        },
        {
            "id": "bb8b123a-4ab4-42e9-93af-c75bc622126e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "8094f9a0-1692-4884-a38a-a945e83b23af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "7f7746d5-15b3-409f-95a0-2c175af1f276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 256
        },
        {
            "id": "be6bf872-f123-4e4a-95cc-a45aa15bfcae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 257
        },
        {
            "id": "f1b652b9-96ad-496f-8aef-932f231438ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 258
        },
        {
            "id": "9b46af93-ff20-4d6c-bbb3-daf286bd0128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 259
        },
        {
            "id": "f5291003-c823-4fa8-a2e0-ccf5ba7fc60e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 260
        },
        {
            "id": "07cd19ea-0f68-4871-9fae-e7965c31b5ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 261
        },
        {
            "id": "043a6c37-0adb-4abd-b583-9fb9179a8c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 262
        },
        {
            "id": "024dc4cc-ea6a-4951-9af3-71a95623495a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 263
        },
        {
            "id": "b111070f-b07b-47b9-9c03-6306cbe6a81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 264
        },
        {
            "id": "0b7b6729-3420-484c-9bf3-5f3339e6bb42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 265
        },
        {
            "id": "9d529f73-ec19-4334-8780-c768939e2cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 266
        },
        {
            "id": "14d11e65-c382-4847-bd25-6ff8ada42606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 267
        },
        {
            "id": "4015a9c0-251d-41f0-a9d9-53938837a835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 268
        },
        {
            "id": "527b83e0-8171-410f-95db-52b01565f36e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 269
        },
        {
            "id": "8a54212f-a14b-4ecb-9e38-e4dd6094c0a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 271
        },
        {
            "id": "208f0295-d36f-400e-aa15-2373624d0881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 273
        },
        {
            "id": "16744d63-9c2f-4273-8a6e-0e11e040fecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 275
        },
        {
            "id": "ef536a86-8c80-4ace-9e9b-80fbccd5b1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 277
        },
        {
            "id": "196db9c2-a6e2-4b2a-a777-c8d10631236c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 279
        },
        {
            "id": "325d6794-e2fc-469f-a0f6-cea152876206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 281
        },
        {
            "id": "027b2af1-b48e-4a88-abee-9b7e3d6a7dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 283
        },
        {
            "id": "e8262e90-1bdc-4618-9361-48c877611f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 284
        },
        {
            "id": "e9e0ee55-9854-4b66-8cfd-1e20b4036798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 285
        },
        {
            "id": "7a1d5ad9-d31e-453e-a3b2-8e376424ab60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 286
        },
        {
            "id": "c43bc818-6b4f-408a-8389-6c5a796cae8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 287
        },
        {
            "id": "54b59df0-acb5-49d1-84b6-1a4c99ae3dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 288
        },
        {
            "id": "3dc524c7-24d4-4b7f-92a0-5bb0cad556ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 289
        },
        {
            "id": "2c99cf44-44ed-4fe2-a7f9-461e3ba8c9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 290
        },
        {
            "id": "9a947145-b1a8-4f1b-aa0e-8501cd3e45ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 291
        },
        {
            "id": "0c5db2fa-9540-40e8-80b1-4299595b618a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 312
        },
        {
            "id": "948c0207-63cd-43a1-9aad-77679f926fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 324
        },
        {
            "id": "88f33441-fe1b-4fa3-b767-2629bc0417d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 326
        },
        {
            "id": "69497b0b-4d81-498c-8597-a80b7f0da05e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 331
        },
        {
            "id": "09341666-6ba9-4e26-84f0-6a178488c11b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 332
        },
        {
            "id": "fe3be136-79df-4f1e-954d-2b3f5b785ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 333
        },
        {
            "id": "c0642daa-d2d9-48bd-acc0-31df99d30ecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 334
        },
        {
            "id": "56a3e644-43db-401a-a2e3-6b7777b62d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 335
        },
        {
            "id": "f4396a57-372d-48f1-86dc-e348c322d1ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 336
        },
        {
            "id": "d219c93b-3358-4e0f-97da-138731d93186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 337
        },
        {
            "id": "30ac7ad1-14dd-4d44-a3dd-e8c2cb3c94d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 338
        },
        {
            "id": "007ef638-6f8f-4ca6-beb7-f21af262f714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 339
        },
        {
            "id": "0da9e352-004b-45a0-9679-3120db0ace70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 341
        },
        {
            "id": "a1086e6f-06db-488f-ade1-756b9832170f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 343
        },
        {
            "id": "1bd273da-6458-424d-998d-0806d11a1af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 347
        },
        {
            "id": "47bdf61d-3521-4d45-b001-9a223fb6fcae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 351
        },
        {
            "id": "45951e21-2ebe-4681-83df-435fc473d71a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 354
        },
        {
            "id": "9e619cc4-c0c4-4dba-a875-91afa8d3c31b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 356
        },
        {
            "id": "76534916-82a8-4b4b-89be-52226aab53a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 361
        },
        {
            "id": "97d8eebb-f251-4edc-9495-beb3485fde29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 363
        },
        {
            "id": "7ea895ca-56ce-4838-b230-0f4aabda4872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 365
        },
        {
            "id": "2709cfa9-0a6c-42e0-908d-dd76640b4c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 367
        },
        {
            "id": "1efd1f71-c004-4453-b5f9-6cd9fa52e1f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 369
        },
        {
            "id": "211be41b-1d81-4f5c-ab62-087f63be215d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 371
        },
        {
            "id": "e89042d0-d93a-4fe6-b4c4-9895498336ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "b599b0ba-db2a-4f0c-9128-c42c98a4297d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 378
        },
        {
            "id": "448567f9-314f-4399-a93b-95d56e798755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 380
        },
        {
            "id": "26c61701-1f46-43f8-b9e8-ed6da6067033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "cc2e0a81-bfe2-479c-bb9d-fc037f143104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 416
        },
        {
            "id": "2b7fbd23-7abd-4aac-8866-a9f257894a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 417
        },
        {
            "id": "2d3b58bc-676b-4ff0-b90e-3c0b98c1cf71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 432
        },
        {
            "id": "7c1482c3-058d-4488-b03b-34b75db442fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 506
        },
        {
            "id": "f321f3d8-344a-44b6-a9cd-d3d23b173751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 507
        },
        {
            "id": "43117e69-2568-4555-914a-c0037ed9da76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 509
        },
        {
            "id": "611fdc50-a681-4930-9315-de75d5259948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 510
        },
        {
            "id": "91c75efa-08de-4629-910a-72ba907a3b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 511
        },
        {
            "id": "aac12267-470e-46e8-a76c-baf79c5711b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 537
        },
        {
            "id": "3fe6867b-6199-4a93-a032-69e74e5671c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 538
        },
        {
            "id": "d5a55911-2622-496c-8972-ccf9cee0f421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7680
        },
        {
            "id": "eb1a3555-31fe-4027-af59-ec43d6841a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7681
        },
        {
            "id": "dc1bdde8-50b5-438a-9e1d-1f20a017a073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7743
        },
        {
            "id": "a2e16ad6-adae-4c21-a4cd-efae827e8b8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "aedfb336-8c5a-4026-b6f3-47dc456d94c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "a123b0b5-2183-402a-87db-986743d0c0de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7840
        },
        {
            "id": "de30b28a-e5d4-4ece-b60b-ca6162fab8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7841
        },
        {
            "id": "f70b90ac-e233-405a-8d8f-c22eb6f96f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7842
        },
        {
            "id": "38c39343-d50c-411c-bbd5-33ca8e225c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7843
        },
        {
            "id": "f573baf0-db0d-433d-a9ff-534f01604979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7844
        },
        {
            "id": "fe51c3c6-4e59-4041-81eb-b4f16ccfcca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7845
        },
        {
            "id": "278eb5d2-757b-4a91-a2a6-a8f0e5f85f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7846
        },
        {
            "id": "23df5fd9-7e07-4860-8802-037a466ed347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7848
        },
        {
            "id": "dff7bea5-dcfb-4725-b64f-24a35736ef2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7849
        },
        {
            "id": "a90b6e08-c112-4205-b606-15721434e2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7850
        },
        {
            "id": "5fa5ac65-bbc8-45b5-a970-c5cacf6850db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7851
        },
        {
            "id": "d79445ae-cf4d-442a-8748-ca794b2b4cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7852
        },
        {
            "id": "f5702a46-394a-4e43-9c94-555df651c0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7853
        },
        {
            "id": "1e3d22aa-a393-4841-88ce-59ceae4c967b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7854
        },
        {
            "id": "6a38ca7f-4bba-4aa0-b57e-7f17a6da5769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7855
        },
        {
            "id": "092ff6b4-6914-43f2-b08b-c496eb13de89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7856
        },
        {
            "id": "a419d8ce-d9b4-49fc-89ae-877df0ad966a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7857
        },
        {
            "id": "95dd727a-0ab9-4102-aad9-5cfada148ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7858
        },
        {
            "id": "272509e6-6aa9-4802-a7c3-e5d36581b387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7859
        },
        {
            "id": "fc1d4ec1-c178-407f-b46c-666f6c0347e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7860
        },
        {
            "id": "487a5e07-3c0c-4ee6-abb9-983a9dd42007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7861
        },
        {
            "id": "0bc49ff8-ce0f-4964-860e-5cafa9deef20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7862
        },
        {
            "id": "70de4fa9-315e-4d10-8e36-b7ce4f7711e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7863
        },
        {
            "id": "3d6e56de-4931-47fe-8455-5b24663fc477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7865
        },
        {
            "id": "243e66e6-ebfb-4dc1-be99-6d40fd3a1435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7867
        },
        {
            "id": "d99f7b78-0121-4bef-b232-04a2f96b4e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7869
        },
        {
            "id": "b7eecdcf-b23f-410e-b40d-5305fdc0a7d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7871
        },
        {
            "id": "ccf43419-c316-4739-ba74-dbcbcf17fa2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7875
        },
        {
            "id": "e3b03e19-be4b-4c26-8cb1-571514cbbd96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7877
        },
        {
            "id": "f9ccba25-85eb-4bcf-8cc7-431564e6c646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7879
        },
        {
            "id": "9d840dcd-6072-4800-ae17-547c4c3844fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7884
        },
        {
            "id": "45655e71-efcb-4546-9bc7-ee4c6d8a1d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7885
        },
        {
            "id": "3152268c-c192-4055-af74-a6c782b5a60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7886
        },
        {
            "id": "df9c0f6e-4e81-4cb0-a030-c7c6d0adc079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7887
        },
        {
            "id": "64d8cbda-f3c9-46e1-bff2-2757d4c8710b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7888
        },
        {
            "id": "05334f16-c12b-41c9-83f3-fb0e3dc982bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7889
        },
        {
            "id": "b2aa112a-a93b-4d09-b422-6bf137c4f4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7890
        },
        {
            "id": "f53cbba2-2185-471f-a2d6-5ed8aca40830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7892
        },
        {
            "id": "a9a77487-0066-4f60-82ae-0527a939418a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7893
        },
        {
            "id": "536cd36f-3391-405b-8c3b-9b0db4af27b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7894
        },
        {
            "id": "838b6de9-e153-47cb-b479-1cdbb2038714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7895
        },
        {
            "id": "04a9505e-c141-48b5-a20b-19d5529f9d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7896
        },
        {
            "id": "461b8dd6-ea78-4e6e-821b-eecd78d32692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7897
        },
        {
            "id": "9af81677-4f0d-45d3-a256-1d69816a6e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7898
        },
        {
            "id": "e58b5ef3-6f5d-4d1b-8ff9-447c204b38ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7899
        },
        {
            "id": "1bbe8a53-ffa0-4f47-89dc-ba575405c685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7900
        },
        {
            "id": "7da03882-0a0c-43cc-bd4b-8ad18a55d90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7901
        },
        {
            "id": "5547a859-baba-467e-b193-b392925a72a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7902
        },
        {
            "id": "b462c1df-22fd-467f-b880-ab0b705ed6bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7903
        },
        {
            "id": "2289bbdf-583a-4751-b766-0828e759af35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7904
        },
        {
            "id": "fb0d057a-6b06-4a42-a360-40182d91d70b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7905
        },
        {
            "id": "e1d8c1e8-2990-4d60-a8cf-3374264e26a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7906
        },
        {
            "id": "69c83531-ab90-4ef1-849e-fa72651039ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7907
        },
        {
            "id": "bbc6d3be-6def-4a90-9317-39dac80cdc24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7909
        },
        {
            "id": "4630eee6-c973-4dbf-836a-efae5f8d4780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7911
        },
        {
            "id": "439bdb3d-aeb6-4cd5-a883-f602885b6275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7913
        },
        {
            "id": "57df2e1d-6d9f-4189-b6bc-2e9d5b15532b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7915
        },
        {
            "id": "7aa416a5-d29b-49ec-8b23-429d99ff3e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7917
        },
        {
            "id": "f7af371c-6b5b-4d44-8151-480b94d1b5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7919
        },
        {
            "id": "fbc0b949-3d82-491b-8f8c-aef33d2f6dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7921
        },
        {
            "id": "d4dea54f-0807-43a7-84cd-b63e0994dda5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7925
        },
        {
            "id": "a1a74e1f-2e38-4ab4-9b97-f8f0e95395dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "1a002351-7e18-4ae4-8699-f26e28a86642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "c01a80a5-0edc-4ed3-92aa-3e8250a69238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8213
        },
        {
            "id": "7a893900-1b19-4b58-a11c-6992135f6cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8218
        },
        {
            "id": "5a367b6f-5a14-4937-83e8-7b4bc9e332a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8222
        },
        {
            "id": "967e4f3e-8bd9-43a9-9264-3ac1a5b74390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "b5d65618-72d7-4ef4-910d-cedfe91ef1f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "2d9010fb-870d-4e1d-8905-04448b7795ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8218
        },
        {
            "id": "c5f277d8-c1a4-4429-8f93-a1d223316196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8222
        },
        {
            "id": "9142bfad-b56f-444d-8d78-a4771e7a9b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "fcec77d0-ef6f-4b08-93b7-601994bb3856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "e239038c-f6f6-41ea-a8c9-fddb6893ed30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 63
        },
        {
            "id": "8ec053df-f66c-4679-8231-07dd347698fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "3c048a24-7c9d-437e-9b00-5eeeb73d6739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "8a0a2f11-3162-421a-aa87-ec4dffb5c8e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "3604312e-dc2d-4acb-bdc5-76520c745316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "09504164-8b98-421e-938d-7dca57ab31a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "c0bceaf8-25f1-4eb0-8630-2470ca1a8267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "9f38f358-2432-4baf-abfc-695694e73f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "1e8d284e-45d4-4af0-afb3-e4987905f855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 192
        },
        {
            "id": "535fdc0b-3639-4526-bd9b-e9ddbf0a359b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 193
        },
        {
            "id": "e584b6b2-34f5-4096-8fcf-e3babed29a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 194
        },
        {
            "id": "171a64da-104e-48bb-b71a-6da5d7aa8a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 195
        },
        {
            "id": "04ad410d-cdf4-4959-9807-7c888d03b338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "b29a0a46-8566-4b94-a722-ee53bab5f5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "c1534add-5cd2-4dea-a094-ccb70e01c727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "d333005a-986d-4462-af21-fa8db240aa20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 225
        },
        {
            "id": "ac1470f5-cb45-46c8-a31b-c2523aaed296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 226
        },
        {
            "id": "d37a9320-3772-467e-9ff4-7d7c3f208033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 227
        },
        {
            "id": "17a27d92-7157-4b44-9adb-b9801b009682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 228
        },
        {
            "id": "685fcb8e-f3e6-4a6d-bc0c-431b25f55e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "55a78121-9f56-430f-80d7-94de06872c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "b271af78-79e1-48ac-ade2-fb31ace2fec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "32b95a44-456d-432e-a9ff-3d6a9386ddcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "66ae7a28-5275-4bf9-a258-0128590e9873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "f71a8cd8-e4eb-4514-afcb-54ef6c97eb76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "768ab158-5897-4b5a-ae1f-e56438fd4f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "33e5ebe4-6301-4283-99b7-1a01f39a4538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "ee839467-6f71-4e22-b09c-6f47398f00c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "8fa6365f-6cfd-46c2-b837-50727572e14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "25221288-618c-4821-ad52-5822c5105723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "50f45f65-40b6-4496-a069-dbf65fbd4171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "0a325f1c-7939-4897-a270-ff4bfef7e543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "72305c72-a774-465b-bc91-23a8b5704e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "bd04076c-dec7-4b28-82f2-24da9b6e6e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 257
        },
        {
            "id": "42af9eac-4f4d-4081-9fb6-0095298e9820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "898aeb5e-c566-48f0-904c-21d9e95f1914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 259
        },
        {
            "id": "e8d4be15-2bbf-4479-8fa1-996ddf22a4c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "67ef8d03-1539-4c93-aecc-f54c8d06f670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 261
        },
        {
            "id": "48c7b9a2-7d9d-4e9a-b602-91efd56fa4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "2ee0d561-26ac-4518-858f-1eb77dba9bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "db32fea4-9229-4515-acbe-b9f32e5a025e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "a90160be-445c-4bf5-b4e7-97e9658a8afd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "d73003d5-f821-49ff-a7ae-f9ec7af2496a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 271
        },
        {
            "id": "f146892d-ec65-4b10-970e-384bdf9ed8ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 273
        },
        {
            "id": "75dfe590-0ce0-4cf6-b872-cbae7c8e210e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "ec667c2e-ec0c-44f7-9987-f6a97330b372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "73789af9-b335-47aa-b135-fa762fcd6b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "626d1e07-7807-4864-8409-78213d751133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "d4da5396-98d3-4180-b783-8295019b8015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "c5a87e7d-7318-4bf5-b328-d94c9fc415cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "7168828c-7fac-4458-8972-79d558f7537d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "0cf87948-81fe-4819-ae03-e94ddb9a9618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "903bae07-fef4-4e36-828a-abfbd78529d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "b52818e1-2c92-42ba-93dc-3dd837e89dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 417
        },
        {
            "id": "ee6339c2-05ad-4d9d-9ed2-052bc50c9839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 506
        },
        {
            "id": "ddaf6075-a93f-43cc-b40a-783b3b1cf13c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 507
        },
        {
            "id": "92bb3d03-a573-4e51-8762-0b87531e8827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 509
        },
        {
            "id": "1ac3a72d-bd47-46f4-afae-f15757d62d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "2aff2356-83d5-4799-b61a-2c619d9b80cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7680
        },
        {
            "id": "f3913f86-d9a2-4467-80cf-8ee89c532b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7681
        },
        {
            "id": "2be4d9a6-3c3e-4553-87b6-0f4987a8909b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7840
        },
        {
            "id": "025c9aad-0fa7-411b-96ad-f7161438ca6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7841
        },
        {
            "id": "89a23c03-6d74-4ea9-bfe7-a701ee330bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7842
        },
        {
            "id": "ebd3dcb8-3bb0-4b20-8d6d-2573c13cf8ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7843
        },
        {
            "id": "5effc439-f7fb-416f-a532-4faa53e95df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7844
        },
        {
            "id": "837f37ab-8454-436d-ae62-f23f9690346b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7845
        },
        {
            "id": "82e34171-1cf1-4320-9a3c-3a1da50c58b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7846
        },
        {
            "id": "2e8a5e37-e757-46eb-a35e-f21dd647476f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7848
        },
        {
            "id": "651d5d76-abcc-47f9-bda6-aacfc803a098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7849
        },
        {
            "id": "599b8dc0-44b2-46ef-abc5-a724453680a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7850
        },
        {
            "id": "58a6d09a-6eb8-4f4f-87f6-c36b40df65d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7851
        },
        {
            "id": "3cc19c7e-a3f1-4988-a9cf-227847b3eb85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7852
        },
        {
            "id": "fc32ba05-8def-40a5-9f9f-915c9a742975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7853
        },
        {
            "id": "4ef8eae0-51ea-47cb-beee-77fea2a2b2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7854
        },
        {
            "id": "9b1c0292-61a7-4de4-84fd-8e3a6170ed07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7855
        },
        {
            "id": "84f63469-362c-4de3-8dd2-f1c633e0b52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7856
        },
        {
            "id": "9fbc5fa8-a0a7-40e5-8964-50e6dead6d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7857
        },
        {
            "id": "11475815-a06e-4c94-9240-dd1985a72fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7858
        },
        {
            "id": "80e20525-e0ab-4cc2-b5d8-aaa8cee6a62c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7859
        },
        {
            "id": "9b1ec38b-6184-4c42-aabc-f35d9203c5c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7860
        },
        {
            "id": "e04dbce2-423d-4629-9817-5eef750896f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7861
        },
        {
            "id": "14fd2d47-649f-4fb5-8e9a-46e3cd38c257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7862
        },
        {
            "id": "6c0e8268-35dd-4d66-aa8b-5ea70dbcc416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7863
        },
        {
            "id": "cbf54dc6-3eea-42bf-8a41-cda2b76f208f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7865
        },
        {
            "id": "9ff061fa-68e3-455c-8993-c00d62620532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "0e23484a-49c4-4cbc-93dc-014c52e489e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "bbb2459c-c8ed-408b-9e14-7acb164c5096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "447940eb-cc36-476e-82b1-ea96fe88e064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "4525ddd5-8401-454e-8892-bc0b9f574b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "2b5bc967-2be3-417f-837c-ef4d3bacedec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "fda9dd9e-bdf3-44ce-95b7-a0ac6b1fe1e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7885
        },
        {
            "id": "6806502b-11c7-4613-9120-c29716c68d47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7887
        },
        {
            "id": "bc6d5e31-c357-45fc-9c3b-4a1dabedac40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7889
        },
        {
            "id": "7eda79f6-6dd2-433d-9b57-c892b8a35b96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7893
        },
        {
            "id": "6843a546-630c-4f64-880c-301c9da9ed63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7895
        },
        {
            "id": "7a8322f4-5eef-4004-acfa-a7dd4114e223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7897
        },
        {
            "id": "6a5c467f-ccb4-4741-ad66-dbe2df9a4613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7899
        },
        {
            "id": "b5e11657-9193-4723-9598-55e71dcf2454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7901
        },
        {
            "id": "2806a50a-fdc0-4a04-9c9d-89f475e91301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7903
        },
        {
            "id": "4a143bca-7740-4463-b322-68f84ea43053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7905
        },
        {
            "id": "f849d4d7-70ea-4cf0-ba49-82172536b07e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7907
        },
        {
            "id": "47b23078-fec3-446c-a146-5ae6b7be2f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "47a42fd0-538b-41fe-a533-75c5413220d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "86d58485-3e3c-4a57-a964-4cf3a9848b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "429fcbdd-89e5-4c14-a8ac-141fe04679f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "509ca5d4-3fd3-4e6c-a18f-1eea9e1e367d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 63
        },
        {
            "id": "1ca69968-80eb-45fd-870e-6c6352195681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "12cd7617-40fe-48b7-be3e-92f5c784ce5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "b7959ec3-925c-46ca-b710-a921519855de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "4e33c87e-1e99-4610-913b-734bb745b2ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "3d08cc4e-822a-4588-98ed-becf116341d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "8d08fb5b-e74a-4697-90a8-549c40a80c5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "04e3203d-c36a-4d42-8907-9a74f5706629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "061de998-eee5-4b37-aaed-cb8ab1a895a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "03cc3fcb-2257-49f0-b11e-683402c90508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "9df898d9-7cc3-4a5d-ac63-09a997107f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "e3fb49fc-1e76-4e64-83ed-7185455f06eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "75b011fe-21b5-4bdb-a519-2c15da9123eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "2ad3171d-35b5-4487-9718-44d2ca6cc757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "dd243737-0fa9-4a2f-b770-18bc185764a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "8de1dbcd-e431-421d-9679-ca56b324aa40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "9123b1a2-5864-4db0-a61d-9ad00559bfe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "e728f682-0bf5-4490-b0f7-b0e275f89a09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "05e0b658-caed-4e81-b35c-27005d9244c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "651bef4d-5452-4306-b2e9-eec5b301b764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "c766d524-8b8b-42a7-a7e6-1d8aed4ac4af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "af22bbbc-1887-40e3-998d-8d68dfd50dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "99dbfb12-90af-4498-a987-d34effff4923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "3cf3d440-164f-41ef-804b-def5ece0e72f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "4308a85f-3fc1-447d-bf93-59c94a5ae04e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "c74d3bf8-e9b0-4f17-a834-a19097b35dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "0e39c911-8318-473d-be73-ccdfd1ecd727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "10fe02b7-dba1-4667-bd3e-e2efe093d904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "dfa70b9f-6ce0-4163-9d5d-2111fb03a69a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "d5fc5fad-fae6-44b3-90da-de0034ce01b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "e172fab6-83a7-493a-96c7-fed99fdeebe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "193b6213-0a4e-406e-80f4-763b720524ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "b1ad15a5-5899-47d6-82f7-23bf6c246752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "f729c6f5-d161-43eb-9618-5145fe6f853d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "35c0c84c-21e7-48b5-b0fd-bacf6cab61b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "8dd99bb1-8160-4e1b-ae97-5ab98593dd90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "029af316-23ea-4dce-870e-5b24c49f171d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "8ceb3f1f-d1e4-4515-a720-4258af21168f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "d3bec814-d7d7-4b19-b98d-f7fb367ee7b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "82dccff3-5268-479c-afea-b5bbf0c7cb0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "446fe082-3bea-46ae-a56d-3e18b4da0b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "65cf5ad9-de15-4d21-a4f0-a0e9b54ea13b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "501bf2b6-f552-4b7e-928b-c6ce6bb1cab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "5a4e4b59-4616-42fe-9cff-f7227eeaac2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "45c6e578-07fd-4fd7-9f01-1ac8eeb5eb20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "8c56fa4b-5c56-44da-97f4-1650c400635b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "f7c973a8-84af-4f48-babf-4e3901e7e279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "2836b5b5-2493-4f04-a472-f1b7a2adc090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "bd2ae7b0-8304-4c1d-b39d-65c059da87ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "d10565ed-7535-45c7-a39b-623aed3b88fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "66fd4f2c-7686-4538-a7e1-41ad041e97e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "5c2dc29a-52cf-454e-bc64-0db6d027e492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "48d36a1b-419e-4455-ba33-bf146add1825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "f6a121dd-8f6d-41fd-ba07-94ae9187acfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 417
        },
        {
            "id": "dcf5ba58-e0e7-4fc3-9194-a320cae6cff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 506
        },
        {
            "id": "202f112f-09af-43d7-8dd5-55c200c62faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "34592556-d7f7-4f8e-bc9d-51682130a9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "154778ea-5e2e-4a6b-ba97-eb2ed89f0673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "31a0dfd0-a51e-45be-87cb-207c6b8f3e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7680
        },
        {
            "id": "12a28d1d-1091-4f27-9b78-e6850f15d2d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7681
        },
        {
            "id": "dde57af1-c64a-4c14-8224-8c959e26ea3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7840
        },
        {
            "id": "8bb26d79-f52f-47f3-991a-b7e35d31f22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7841
        },
        {
            "id": "4feaf45a-80e9-4b7a-9cb2-3c42a9356b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7842
        },
        {
            "id": "e61d8f98-61c6-460e-a222-9a60ecd7e719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7843
        },
        {
            "id": "49f50b0d-6f6c-45dd-96eb-24a9b5d66190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7844
        },
        {
            "id": "41a43339-9b80-47fd-9da4-4258611ed743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7845
        },
        {
            "id": "29333f8c-9c1a-46f3-a9d9-9b96ecf9f1e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7846
        },
        {
            "id": "222b24b5-a877-4263-ae2d-c7ff674887a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7848
        },
        {
            "id": "4a5a79ae-a671-45ac-9d1b-529f6088e8ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7849
        },
        {
            "id": "9fb88df8-16dd-408c-aea7-3bf5357068aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7850
        },
        {
            "id": "bb237f97-90cf-48c4-b40d-74e14f67570d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7851
        },
        {
            "id": "182cddbd-9e35-4020-a3ed-2087bb3fbca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7852
        },
        {
            "id": "bdcb470f-7fae-4534-a64e-5c0ba121a9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7853
        },
        {
            "id": "f43d6957-97fa-454b-b277-b720e47bb747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7854
        },
        {
            "id": "18c1f113-1826-4949-9d9c-e820796be132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7855
        },
        {
            "id": "3a739a7b-68bf-40a6-b4ed-dad0bbed6898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7856
        },
        {
            "id": "0d232f65-6a86-489b-a932-7e5ebe95623c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7857
        },
        {
            "id": "26b3735d-22c8-4c3e-be87-8f15843f146f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7858
        },
        {
            "id": "d80e6e01-2822-4be9-9ea3-135df8cf0d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7859
        },
        {
            "id": "1dc97ed6-eb09-42bc-bdf8-0ce6cbc270e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7860
        },
        {
            "id": "3c57461d-1156-4932-8462-6446883744c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7861
        },
        {
            "id": "54eb3303-7c3f-4766-aeb5-ed74123f0e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7862
        },
        {
            "id": "dd4d0052-4329-4d8f-a78c-96b141504dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7863
        },
        {
            "id": "6da731c7-febd-4b2a-bc9c-4d116fb788b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7865
        },
        {
            "id": "22389712-8e79-40f2-b2f8-b3c50cedf3f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "64cfbc4a-d512-4f07-ad31-15cb7eccb15a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "880b727a-c26a-4030-aee6-47bede6b0dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "5d48718a-5549-483f-8478-baf2b04a3e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "2d7da0ff-8bf0-4768-ada1-23e166259684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "3441bb91-680b-4b20-a136-e3e4174aa254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "a7a8d458-3926-41b0-9713-08c6408288cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7885
        },
        {
            "id": "ea4be913-1c62-4486-824a-104bda0b214d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7887
        },
        {
            "id": "ada10fb4-619c-455d-9216-009d9d9768ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7889
        },
        {
            "id": "5a1c7515-9ce4-473c-9767-8a64ce1ce166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7893
        },
        {
            "id": "78a7d491-6d40-47f9-a83b-ec5a2645e9d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7895
        },
        {
            "id": "cf3ced46-fd00-48d9-bf1e-ecfa6e83ddd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7897
        },
        {
            "id": "7a85063b-8a49-437f-b627-60ce4b2cd209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7899
        },
        {
            "id": "f1699d4d-2c40-4d7f-ab5e-8a4c91aa1292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7901
        },
        {
            "id": "b0050544-d030-4338-8fe5-c37e644bcb41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7903
        },
        {
            "id": "f4170c50-8782-4395-8ee1-7f7e0cfc5e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7905
        },
        {
            "id": "efa75bba-f034-4baa-b8bd-22bd1c545b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7907
        },
        {
            "id": "4b308513-00ef-4a31-a4ce-e21b34021c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8218
        },
        {
            "id": "558c615a-0166-49e4-a3d7-2cea1d3d079c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8222
        },
        {
            "id": "62b48597-da89-460f-bdb9-4aa8a4b8a7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "95d31660-2590-4236-9b7b-5318982e01de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "b61398e0-dbaf-4b8e-ab0f-f3ad0f88c0b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "6e4f598e-43e9-4370-a9c5-cb36b29602df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "bc3ec12b-9f06-4020-ac28-99e092703e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 199
        },
        {
            "id": "262d9830-9e6e-4577-a7a3-2b89acd16e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 210
        },
        {
            "id": "cb22db57-881e-46d6-b46f-02422c9f3cc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 211
        },
        {
            "id": "84102b50-e11a-4f93-97b4-fc376d1b0646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 212
        },
        {
            "id": "8cd91b9c-070f-448d-8bbd-23116f62324c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 213
        },
        {
            "id": "a1706f28-136d-4ac3-b5a0-7934f398faf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "e72400b1-1c0a-4955-88ae-5bb1b5eb1190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 216
        },
        {
            "id": "0e2b57b7-10d6-435b-93d5-8577f934e534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 262
        },
        {
            "id": "ff1b83c8-8536-4a89-897e-3614cf340ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 264
        },
        {
            "id": "73cfbca3-1e23-44ed-8a78-a1a8532d44d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 266
        },
        {
            "id": "bc35cdd8-21fc-4b3b-ba50-714ceebeb9f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 268
        },
        {
            "id": "7a0bd29b-610c-4fec-85a6-3aa33c1d8caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 284
        },
        {
            "id": "6851b5c9-74f0-4bad-9c40-ac256ff65660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 286
        },
        {
            "id": "6a227514-b6b7-4e62-8ff3-e8bae850a712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 288
        },
        {
            "id": "613d4f1d-88de-4395-9e91-23291b63e706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 290
        },
        {
            "id": "fa19d966-0384-4b4f-8c0f-c616f7b5f7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 332
        },
        {
            "id": "95adaba2-ed90-4807-b3eb-2493c1d854e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 334
        },
        {
            "id": "9314a6d0-0e2b-4361-9819-43a0ff32608e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 336
        },
        {
            "id": "83237d4e-483a-4fa5-9051-76b7cf1225fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 338
        },
        {
            "id": "b6954381-395a-411b-9167-968adc3ba295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 416
        },
        {
            "id": "e63df447-ef6c-4078-ab01-f06181624a03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 510
        },
        {
            "id": "a934de56-cd31-4b21-992f-70c1759219e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7884
        },
        {
            "id": "79ba9c70-d7cc-4860-bf7e-2efc668f8a96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7886
        },
        {
            "id": "6e5aede0-77a4-4313-a3b4-98555383a790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7888
        },
        {
            "id": "5228da29-a84e-4e6a-a499-3f0ab55c5134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7890
        },
        {
            "id": "d59bee11-b25d-4a23-be92-ef104ccfe7a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7892
        },
        {
            "id": "d8936dad-a729-47aa-a5a7-700b5d595a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7894
        },
        {
            "id": "15d6bf4a-0d98-4c14-9433-4332c96fae2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7896
        },
        {
            "id": "1d822769-2fea-4029-9619-4ede1d5c5770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7898
        },
        {
            "id": "d0675be0-e3e4-4634-bf88-65f02a71695d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7900
        },
        {
            "id": "02e1de1c-b4dc-420a-b598-15bfb9c0c71c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7902
        },
        {
            "id": "81ef259e-ac70-4dc8-81ad-3d925ce569f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7904
        },
        {
            "id": "ac20c4cd-1d36-4751-ab17-010473c66021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7906
        },
        {
            "id": "2137b679-2c46-484a-8fac-4d36e3249f9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "129c9648-4c32-42fb-872e-efae667e59f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "18dc508a-dfc6-4be8-90fe-1e2525a77975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 63
        },
        {
            "id": "c4a0083f-61df-44c0-ab32-1cf12d1117f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "b48e0fe8-2d7f-4308-bb8e-d704a873b0e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "c30db64e-0236-41cc-a2b9-74aac6a431ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "426170c5-29ab-48d9-ae08-3d242b90fb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "646c7c60-2103-45cf-a454-527e22c37ed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "3cb99a55-bf6e-4315-8f86-c36621961c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "bf2be4f7-2114-4e0b-aa06-793646754617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "58bc4172-9e5d-41c6-a77d-639c0e32aeec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 100
        },
        {
            "id": "64f32406-2e68-4f10-8539-4ce89c895843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "ff4ea1d9-2e12-4a0b-9e8c-851ab85454db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "60bcaab2-88e1-4bd2-a403-597d6463193a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "3db458ae-93c4-4352-9625-d07beb82449b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "86aab00f-8aa0-4b9d-ae43-27b568f2c90f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "2df0fa6d-3207-4d48-be32-8dd2f52f068d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "94a71b46-ac7c-4169-9aa4-c6232d3e93b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "ca437b93-e0fb-4e71-a946-7864fc1d39d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "271c233f-7729-49f1-b17a-9b94b09d9a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "4578e64c-d286-4c8e-8a67-70428c818d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "717bf1ee-33a4-423c-8ac0-8127805c2989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "9906ee45-1c76-4cc1-ab77-12892878150a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "d1557831-5e74-4395-9725-cde9381d9b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "9ebdefd3-2e70-4ff1-9a10-5f1f618dcd62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "32b7e6f1-fd10-443e-8961-c71598d63203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "a806fb09-835d-45e6-8a2c-9b50645a7acd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "618ca29c-fb2d-4050-a8a2-35d7714523d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "ddb44846-77f3-4a28-b2b1-2ca298e93746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "22cef418-11db-47ad-baab-48c974adedeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "9872aa3b-8e3d-47bd-a6fe-22272da4b8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "ed89dfed-0753-4d0a-beef-bac99b75c012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "de8d6981-187b-4b08-9105-c1c43f104933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "a32bd055-aedc-4d1c-b858-1ba7fc0dbf45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "dbee34a6-d3ca-4ecf-aa28-cdda0725e645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "5cd91bac-8d6e-410f-ba31-b17d83957ebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 224
        },
        {
            "id": "74183cc9-ee40-4565-8f38-67b395580fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "af13a67e-7f45-4202-bd0c-0fe24379eef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 226
        },
        {
            "id": "0235e0f2-6675-4830-87c3-fe314630f0f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "15119a4e-3cd7-427c-bfa2-158aff52fc5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "72516799-08f6-452f-a5d0-a55655088566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 229
        },
        {
            "id": "372c833f-9a51-497a-a0e5-41833d06abfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "d2497fb0-2e56-46e8-be5b-d9a12b4137f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 231
        },
        {
            "id": "39ffac8f-df1c-41c8-bf0b-9fff75999ba8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 232
        },
        {
            "id": "11dffc53-87ab-4c18-88f3-e1e20e01f898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 233
        },
        {
            "id": "de9016bf-7a29-4c03-910b-367e5bb63899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 234
        },
        {
            "id": "ffd67f56-c586-4048-a942-5abc283e26c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 235
        },
        {
            "id": "cea83e7c-c16c-4f80-a3be-8b6fbe75b352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 242
        },
        {
            "id": "ae16f962-5c58-49cb-803f-0b4e57c41700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 243
        },
        {
            "id": "3547cbd0-63fa-4e57-9f61-1b18ba99b508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 244
        },
        {
            "id": "50a13764-2400-4d83-9016-b0136a1e94ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 245
        },
        {
            "id": "35cf41dd-0ce9-4d38-9327-4920d6790438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 246
        },
        {
            "id": "249a506d-4dba-4649-afe0-ef8a5d37a593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 248
        },
        {
            "id": "8f583e04-21e6-45ae-8ca9-432a8d39a9ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "da072e5f-6a27-4611-91ae-026e3ecc7a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "39557b1a-bca0-4eb6-ab8b-7f67dbe8c45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "062b9b5c-6cb3-4a5f-aa22-feb67da0dc38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "b529b8ed-ce63-45a6-8274-8203bcf0485b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 256
        },
        {
            "id": "6c726846-fd6b-46ed-94bf-10a2439773a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 257
        },
        {
            "id": "ccfd1367-b718-4ee1-bf52-b234798c999b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 258
        },
        {
            "id": "7683bf33-12a9-47f7-b7d2-56924ab77bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 259
        },
        {
            "id": "55bd45cf-512d-47c2-9cbe-dfea234a63eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 260
        },
        {
            "id": "87bb8e93-831d-45a2-b31a-602583eeeebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "27180177-5632-4581-80d1-d1a3d0b54647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 262
        },
        {
            "id": "baf908cd-ace5-457a-840e-f9d11137a65f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 263
        },
        {
            "id": "4031db9e-b6c8-4a16-8a21-89c52d2e5456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 264
        },
        {
            "id": "e6aa99a5-1a33-4035-ac7b-6bd944d06118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 265
        },
        {
            "id": "fe42104f-d3d6-42b8-8e6d-d0a32f07b250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 266
        },
        {
            "id": "072ff701-2df4-438b-8fe4-63fa48a961f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 267
        },
        {
            "id": "6f606238-6eeb-4775-87a5-c9771df4f2c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 268
        },
        {
            "id": "50bb3e07-fd83-4fcb-9d9a-5cd3d6212963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 269
        },
        {
            "id": "28961c5d-5a3a-47d5-9161-45c2f19e7a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 271
        },
        {
            "id": "7b309c67-a068-4801-b6c8-617e8ef0d9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 273
        },
        {
            "id": "02143b34-62f7-4d42-af5e-3a7fbd573160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 275
        },
        {
            "id": "4abe6eea-20e5-492e-91fe-b744fecf11bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 277
        },
        {
            "id": "975ddc3e-e8d5-4176-bb7f-7f2f74ef82c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 279
        },
        {
            "id": "4e381202-7372-4fc8-ab32-fe5288bb35db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 281
        },
        {
            "id": "1a67f9ea-f0d5-4464-b88c-0b342ef8558f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 283
        },
        {
            "id": "2545c179-6a8c-4afc-92bd-4d220be770f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 284
        },
        {
            "id": "d2120472-ddf5-4ba1-9745-a0f391f3adef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "8cbca0c5-27fe-408a-b08a-ffe6c501ae9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 286
        },
        {
            "id": "29691447-1e26-4399-b5e3-1af9a8a3fcd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "5923dbad-7104-402c-8f30-8c7038a5b1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 288
        },
        {
            "id": "3fb1cd08-e3f3-4617-b39b-61b9b2871deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "3ba5c449-2930-4933-9583-e898f30b2944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 290
        },
        {
            "id": "4aa102f8-5a31-4f08-acc5-ef7565af2f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 291
        },
        {
            "id": "dac9f994-1894-4178-9c1e-a24ab4bad311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 312
        },
        {
            "id": "813e3bab-30aa-42bb-9816-3378aa2ffc16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 324
        },
        {
            "id": "ee383fe9-20fb-4da3-86eb-9cc8147956a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 326
        },
        {
            "id": "0f59f0a5-14cd-46d1-915f-e4eaea82645f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 331
        },
        {
            "id": "75ec3e78-46b4-489b-8c28-dce9f567d291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 332
        },
        {
            "id": "5216e82f-4a0f-4830-be45-992dcff28dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 333
        },
        {
            "id": "cb25b9ac-45f9-4537-92d3-3d54b52aa33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 334
        },
        {
            "id": "792b773c-0900-49d2-8720-d7cee2a2668e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 335
        },
        {
            "id": "de9f3fff-dfcf-4c46-8a5c-d047f17e71b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 336
        },
        {
            "id": "eb9c4f89-8cf6-44ef-ac79-35cb41b62a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 337
        },
        {
            "id": "52e43090-ffd4-40cd-bce6-113d89ea9901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "e9b077b6-1e2b-4680-800d-26988dc8b1d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 339
        },
        {
            "id": "7dd9ec17-2132-4ca3-b6f0-d285f0ff3d4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 341
        },
        {
            "id": "98f5c654-75a5-4579-b32b-4d7e93b08c89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 343
        },
        {
            "id": "3070c3d1-ec35-4ec0-a4fb-5374c04c5dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 347
        },
        {
            "id": "5a430e38-f187-4ec2-91f0-83e8611f9f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 351
        },
        {
            "id": "d15d5dab-1a83-431d-8751-1ddad77554b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "e711f0cf-f677-45af-91f9-c08a7d0023cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "098a8f4d-d8b4-4004-a7d8-69b8d4a016c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "63b62df3-fc6c-4ecf-bb6c-180092cdc463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "916c0333-e532-4a10-8f22-d354cee94a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 369
        },
        {
            "id": "467f5f5e-6c7b-4693-ad98-6cd3c9bd5f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 371
        },
        {
            "id": "3e9e7b30-d542-4dfb-a94e-a673a68bd192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 378
        },
        {
            "id": "4ef85b78-d792-433a-800a-a64421a35eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 380
        },
        {
            "id": "c827d821-5ecb-4d5e-9c35-6f2e74550f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 382
        },
        {
            "id": "41ab8915-8735-4703-ac84-ef9fbd562a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 416
        },
        {
            "id": "1228f2c4-9982-425f-a51d-532c6d60a301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 417
        },
        {
            "id": "8032232a-a97b-4e75-9138-186d35d4583d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 432
        },
        {
            "id": "881cd608-7881-404e-96cb-9916c391dc6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 506
        },
        {
            "id": "8eadc7c1-dd62-4294-9b7e-9cbc276e40eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 507
        },
        {
            "id": "c3ac04f7-912e-44fa-91d4-7c390258badc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "6a309619-780d-47c4-9ad4-3477d2ef75b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 510
        },
        {
            "id": "2acbe8e9-3109-491c-aa03-b9196f36283b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 511
        },
        {
            "id": "337cd992-40cc-49a5-a900-90d71148f810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 537
        },
        {
            "id": "0b01479e-da82-43a2-a497-d5354dcdd2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7680
        },
        {
            "id": "9247944a-b19d-4295-a414-04e6e3b22249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7681
        },
        {
            "id": "2ecf0741-8850-4800-a378-503fc62c5ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7743
        },
        {
            "id": "c4e5e79b-75dd-4eab-a960-b9e37ddcdcd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7840
        },
        {
            "id": "3090d7d9-8aad-4a36-8edf-c7e69551172d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7841
        },
        {
            "id": "2b5eb0ad-75d9-4d85-8437-b7135a9ef98c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7842
        },
        {
            "id": "926f558c-9f86-4196-b7fc-f9d32e76e17e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7843
        },
        {
            "id": "eb0f5784-42d7-4dee-a041-8909ba351ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7844
        },
        {
            "id": "4151c2c6-5b22-4399-9aeb-402a705ca6b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7845
        },
        {
            "id": "24297a22-8fb5-4e15-b174-aa914d174c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7846
        },
        {
            "id": "40b5a8e5-540d-4b4c-ad8c-18d9fe232f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7848
        },
        {
            "id": "1d2bd914-e536-4d52-96a0-e34d18d335a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7849
        },
        {
            "id": "4a780744-8682-4e8a-9076-2e71382a4e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7850
        },
        {
            "id": "32957505-9f21-4793-b0c2-9d1ebbe62112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7851
        },
        {
            "id": "4a9cdbe9-e4f7-40a4-ae7a-cf7cbaf73a93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7852
        },
        {
            "id": "442c3512-9347-4879-a36b-9d3934e5e930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7853
        },
        {
            "id": "d1c92e93-0556-4ee0-aafd-7c5ba8cb7a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7854
        },
        {
            "id": "99f20cb5-a2c2-43b2-bd57-881db209aec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7855
        },
        {
            "id": "35355612-a1e3-469b-a810-027fd43acd51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7856
        },
        {
            "id": "ae92d1ef-c3f8-4a16-90a7-df3e5b385a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7857
        },
        {
            "id": "0b871aeb-3c53-422f-a4b1-5d163f5bfdb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7858
        },
        {
            "id": "bc8fd296-5752-4494-b959-5d5f35ad76af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7859
        },
        {
            "id": "db3ac8ef-37ce-420f-ad74-ac97d8c6d688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7860
        },
        {
            "id": "00843f9f-1797-43c4-a9b8-7ef7608fe95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7861
        },
        {
            "id": "5e9f2d83-b605-4400-abe3-85738e2aa472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7862
        },
        {
            "id": "37aa5004-3c09-4292-98a0-215afa01fb6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7863
        },
        {
            "id": "6807f99d-2398-4f9a-b280-f2b2e4a23915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7865
        },
        {
            "id": "34d78c39-3f0d-46d0-b987-974ac9f35ced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7867
        },
        {
            "id": "d6a45f7f-0c36-4f12-a6e4-6879e2de0194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7869
        },
        {
            "id": "b1d22ec4-099d-4e7f-9524-affa6e72e118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7871
        },
        {
            "id": "091d0f8c-e683-4e1d-9ad3-77ffdfabd6a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7875
        },
        {
            "id": "afa04202-e9dc-4aaa-9781-ba43bbbcbcd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7877
        },
        {
            "id": "3a90d17e-a3a5-45f5-b15f-876f37e04867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7879
        },
        {
            "id": "f50b1e58-76c4-4b0d-a639-4db2e49697ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7884
        },
        {
            "id": "0436bc56-93fd-43c0-98a5-249f30501bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7885
        },
        {
            "id": "55fbf548-66de-4524-aee2-b657e06cb2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7886
        },
        {
            "id": "1e14007f-864d-4d1f-9508-c2b1b34f3f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7887
        },
        {
            "id": "76e6cb14-9748-4d65-9123-3139b9e3c4c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7888
        },
        {
            "id": "595cee6a-5f54-48fd-83f4-48267c188767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7889
        },
        {
            "id": "c38510ee-ec52-41b1-a71e-ef01ca18a3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7890
        },
        {
            "id": "ab0e0460-e4fe-4ca9-8abb-fc8c68002018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7892
        },
        {
            "id": "e60e763b-fcf7-4ea0-89cb-00a0412e44ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7893
        },
        {
            "id": "a3b665d5-d5c3-4ba3-add3-ac449abfdd5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7894
        },
        {
            "id": "ba59d88d-f7b4-463c-8b94-6ec2b4a688bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7895
        },
        {
            "id": "1c85e850-1829-438d-88e4-08debfad6728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7896
        },
        {
            "id": "86337fb2-fe9a-4278-b085-d2d4809daa7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7897
        },
        {
            "id": "4d9cf140-e62f-4bb4-a0b3-b6faad140bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7898
        },
        {
            "id": "155fb77d-1b41-4c6e-a92f-d2e1bba75734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7899
        },
        {
            "id": "14c6ec7e-99f0-4303-aff6-8af35eaf64c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7900
        },
        {
            "id": "d5f9de0e-f688-4c71-a039-c05530de3e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7901
        },
        {
            "id": "d55fbcb6-ee3c-4ed6-b214-31add87d68ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7902
        },
        {
            "id": "75f7b1ac-7c89-46ad-b023-491f4a051fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7903
        },
        {
            "id": "86a55ca3-0464-4bfe-b6df-e5869469e332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7904
        },
        {
            "id": "e6bdf737-f339-46b5-8a97-5256bd57b7e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7905
        },
        {
            "id": "6e29dcb5-267f-4010-a89d-4a32a3d27dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7906
        },
        {
            "id": "95f5d9fa-e8ce-4b43-ab8b-4778c9a54af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7907
        },
        {
            "id": "fd168a73-3d76-4b74-b605-63bf4c425aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7909
        },
        {
            "id": "e611260c-d699-414f-970e-cfd57b76686f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7911
        },
        {
            "id": "46d0586a-4e03-4c37-8f00-18e5e423933f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7913
        },
        {
            "id": "39551b6f-7d90-49de-84fd-44bab06d368c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7915
        },
        {
            "id": "e218a8d5-824c-41b8-824f-fdb71b674979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7917
        },
        {
            "id": "edea6fbe-64b5-4ec7-978b-2abe905db3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7919
        },
        {
            "id": "66eec50e-2660-4b7e-aa2b-a2df63072285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7921
        },
        {
            "id": "7b90ab7c-7293-419f-bf4e-ebddb13b93b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8218
        },
        {
            "id": "ef05b7b6-dfd1-4972-b2ed-a45d965000b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8222
        },
        {
            "id": "a7b464dc-066a-4144-8157-6b20b2fa6899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 91,
            "second": 74
        },
        {
            "id": "eadc0512-1a82-4ed9-8b70-329f339619f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "835a68fb-513b-42ba-997a-567be30e1b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "140633a5-608b-478d-b822-af9506d3d81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "53b599d3-42bc-4d12-8042-099269186eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 121
        },
        {
            "id": "f06aaf22-6e82-45b0-893e-0dcd52e664e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 253
        },
        {
            "id": "27373124-8d89-49d3-8f4a-5145b4a5331d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 373
        },
        {
            "id": "f06c6a9a-eea5-47ef-93a7-a88ab7524e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7809
        },
        {
            "id": "a4338e68-d3c1-4f46-88c1-ae7d0954e492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7811
        },
        {
            "id": "ef1cc7fb-3562-42c6-b98c-11d69b00bff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7925
        },
        {
            "id": "abac6d7c-341a-443f-8918-172a1dcc5af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 34
        },
        {
            "id": "8c67d678-80b3-42ef-bef4-e4fa73e0dbda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 39
        },
        {
            "id": "a2cfdb79-92f4-4bfa-a5c9-4c5090fd8731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8217
        },
        {
            "id": "7a8fe15b-49ae-4481-81fc-f9b1676e744d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8221
        },
        {
            "id": "19b67ab5-5a98-4f53-88b6-1de9a3ac47ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "ec005be0-1a8c-4d0b-97ab-99d30c2ceb49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "a73d4654-4dcc-4d4c-9524-67c0d2dd57cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "60c1e7df-dcb0-4cf1-9586-a790ae073bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "0729e531-e257-4c2c-bcaa-42db37c42085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 253
        },
        {
            "id": "6b928485-4421-4d34-9803-3332e671da78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 373
        },
        {
            "id": "4a6a9749-4bf6-47a1-a679-3959c57ffc34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7809
        },
        {
            "id": "b3751321-6f5b-47a3-a75a-f637ef2cf789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7811
        },
        {
            "id": "94f68a00-ec7e-4e26-ae3d-9c8e8066e02b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7925
        },
        {
            "id": "09cbde5b-89f7-47e4-a954-cb3ae0af61d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 34
        },
        {
            "id": "731ad43c-dc95-4054-94b2-fe2d085fab16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 39
        },
        {
            "id": "f6bf3281-204c-4380-9f8e-8f6795972814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8217
        },
        {
            "id": "cbe0549a-54e0-4135-9b39-a43fe2dee587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8221
        },
        {
            "id": "607a4a94-7aaf-4c2f-861a-489e7a9c843a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "64d503ff-9b6a-4fff-a8fc-c3088fd32523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "12d0016a-e52b-4c13-9758-36d42f4eaf4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "b1b2f796-48ef-41d9-b11a-6b1d48558e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "8c7fb1d8-7177-4a8c-b8ae-e518f1969223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "a58096e3-d12d-45fb-8879-1c097ed91be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 224
        },
        {
            "id": "4c0ba3f4-4916-4c5f-8d99-e0a0bb5f9c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 231
        },
        {
            "id": "12679e30-3e2e-468b-b872-027d5d9cd7e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 232
        },
        {
            "id": "12d1aa89-7561-4c4a-a38c-60285e17a8f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 233
        },
        {
            "id": "18f70e3e-d16a-4dac-bf59-4592c810e661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 234
        },
        {
            "id": "cec69097-9b3c-4df5-85e7-f35a022bb682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 235
        },
        {
            "id": "6ad4a7ab-a48b-437f-9a3c-6abbe8c9fa5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 242
        },
        {
            "id": "1b86a13c-5569-42a8-8a98-3ec6b1278ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 243
        },
        {
            "id": "4f701814-8d8d-450e-98a8-94a508ef4535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 244
        },
        {
            "id": "d8ed103d-742e-4a5a-a704-ad51c96e69be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 245
        },
        {
            "id": "d0e1c9be-546b-460b-88b6-9e30ba51abdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 246
        },
        {
            "id": "4c040b77-083a-4194-b279-d81639029085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 248
        },
        {
            "id": "55710b91-46b4-4981-b1ad-9008992bb2ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 263
        },
        {
            "id": "1dbdea22-0f6f-4bfe-9a71-358d679369cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 265
        },
        {
            "id": "f0516309-5c52-4163-9036-d63aff283b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 267
        },
        {
            "id": "50bbaba7-6895-4384-b0e9-95fc9ae86967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 269
        },
        {
            "id": "332f2c82-4fc4-46f0-8d98-895dec4f5b29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 271
        },
        {
            "id": "42b7aaf3-adbf-4ae9-bad7-32386e983dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 273
        },
        {
            "id": "3c6dc6d7-d829-4393-b234-c65323bd6570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 275
        },
        {
            "id": "24fb3a20-ea54-4686-bfe6-e0d69337f9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 277
        },
        {
            "id": "ec5f4534-9f58-47b3-bb20-d2793990c5d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 279
        },
        {
            "id": "3013466f-7020-4723-9326-53e2d1e633a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 281
        },
        {
            "id": "54522b8a-5f8d-4f67-a9af-4c284e170e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 283
        },
        {
            "id": "2d7824de-cb7a-47c9-856b-0c3722c9326e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 333
        },
        {
            "id": "c1875348-5a44-43ff-b3f3-13ced8b187c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 335
        },
        {
            "id": "5fe71d4e-b08d-4015-b8a0-cdbb6c0f02d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 337
        },
        {
            "id": "4a3290f2-4d8f-4aeb-aeba-147f0e699ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 339
        },
        {
            "id": "3d7bc135-97b3-4b78-baf4-8d2bf86dd463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 417
        },
        {
            "id": "9af1b0fc-b71d-4d27-8ad5-805e48476f4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 511
        },
        {
            "id": "2de7a6ea-7277-4621-bed0-084ae0fdbb1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7865
        },
        {
            "id": "df4f9c5a-f31e-441c-a810-c86d3b2bf173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7867
        },
        {
            "id": "04c50a28-75a5-4c96-95f4-c9836bf03d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7869
        },
        {
            "id": "13d4e893-126d-45ff-ba26-76d545c75098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7871
        },
        {
            "id": "b8831c02-b5fc-4960-a46a-3e6a1a2a212b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7875
        },
        {
            "id": "6550d237-76ef-4142-8352-ba7aef31bb50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7877
        },
        {
            "id": "4ebcb370-0625-4840-a7ad-c9b52c978a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7879
        },
        {
            "id": "c5e4ce8f-c947-44f4-b612-aeaa0c74374b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7885
        },
        {
            "id": "32e53cbf-5f2d-4bf1-adb9-d9f35518dacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7887
        },
        {
            "id": "fc5d9f0e-eb41-4d34-bac8-c8826dd3b632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7889
        },
        {
            "id": "796aec2b-0e44-4658-a96c-d50bbd3f1758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7893
        },
        {
            "id": "5fe6e328-a22d-4b71-b417-1d3cc55dab70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7895
        },
        {
            "id": "f2d0448c-4e07-42a8-b587-b51a61fb12c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7897
        },
        {
            "id": "ec723f6f-361f-4757-9d6e-b6888f0067a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7899
        },
        {
            "id": "69a7fc17-acc9-415c-84c5-6404d91eb011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7901
        },
        {
            "id": "df66c9fa-93fa-4f0e-b1d9-e41e45c4d25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7903
        },
        {
            "id": "ef790e5b-fc66-4f14-ad19-8e93bc1bbddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7905
        },
        {
            "id": "34c91f1a-606a-49e2-b988-f5fbced89096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7907
        },
        {
            "id": "59aef1f3-0a21-431d-a176-f567b64300df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "654cc8ca-9e48-4fca-a528-c2ca9419df82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "ab9e9699-efcd-4b52-b72b-039de58077ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "5c128c3c-1801-40a5-8381-b644140d81ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "1fd03157-a03e-4ddb-a9cb-46536b29657e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 253
        },
        {
            "id": "81c20ce3-9a8a-4d9b-83a7-4afccc4be171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 373
        },
        {
            "id": "0373ff48-7b71-4926-8292-7229f5494b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7809
        },
        {
            "id": "a5f5eee1-68b7-4ada-8ec3-4910ab493005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7811
        },
        {
            "id": "89304a52-d330-4d0d-8885-fb4885ef433f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7925
        },
        {
            "id": "1cc291dc-897a-44b7-80ca-76a05763a076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "6a514c3a-9879-441b-b225-73b934c586fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "b39d27ba-6b53-4f6b-977b-7018aa933cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "d09190ba-0e73-4685-9edc-ded856933996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "67ed8617-c54f-4ef7-a90b-d3377dca3f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 253
        },
        {
            "id": "56f342f9-b836-495a-9435-3698d17e513f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 373
        },
        {
            "id": "e3f016d4-5cfc-43f3-9ba4-65c7f2860536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7809
        },
        {
            "id": "acd7c34e-ec33-4d73-a248-2ac586c5d33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7811
        },
        {
            "id": "6c0cee81-2c69-4191-878a-1e7c67d425ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7925
        },
        {
            "id": "8601804f-44e5-4a39-a023-f047e4f3239a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 34
        },
        {
            "id": "077323a2-babc-4a6b-ac36-700513ce47cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 39
        },
        {
            "id": "e0fa12be-e4c9-4e2a-94ed-e5d87c1f5c43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "c896c537-9d04-44c7-a56c-c7ea7699b5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "dcb24a10-b6ac-4f78-8466-4fe51bbbbcf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "efe60f70-c2dc-4a21-a37f-812c145ab56e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 101
        },
        {
            "id": "19ced7f4-2c91-4e8b-9246-90e7dcb1d348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "2f4f1f60-f62d-4a62-baca-13e943fed9ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "27beb568-3049-40f1-8faa-ce1a8ea52df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 224
        },
        {
            "id": "d4b235f0-14bc-4887-8b13-c7a8f4e49ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 225
        },
        {
            "id": "1e2e26e6-76e7-41be-ab58-02261f1f9fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 226
        },
        {
            "id": "2777c56b-2385-454d-b542-1b507fbd5b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 227
        },
        {
            "id": "3738b422-16d3-4130-9444-67ed0bcf3f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 228
        },
        {
            "id": "5d06dac3-965a-4cbc-9eb7-024c11d2bdcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 229
        },
        {
            "id": "928c49b1-2573-40ac-a29a-82f72baa4d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 230
        },
        {
            "id": "7fe557be-68eb-4946-9305-c8d07c1a6e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 231
        },
        {
            "id": "bdbc297c-e717-4d4d-a2ba-1aa8b9757fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 232
        },
        {
            "id": "f20fd16e-5b59-46c3-a801-33b00d2670fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 233
        },
        {
            "id": "c82eddaa-b0d5-4425-9a66-f6b509cd57f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 234
        },
        {
            "id": "7a9cbb44-4bcd-442e-8a12-bbc98138aabe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 235
        },
        {
            "id": "78331657-988e-41df-b05e-84ef1af774c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 242
        },
        {
            "id": "39ed97a1-5119-4321-af13-670589d2f76e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 243
        },
        {
            "id": "0a33088f-49e4-4e02-9d18-e98459a00e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 244
        },
        {
            "id": "95765af4-c948-418d-b643-4c79f407d8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 245
        },
        {
            "id": "db2156c9-d025-4918-9800-71977d6e9388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 246
        },
        {
            "id": "fc30324d-6edd-4319-9518-8f24ee0f4ef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 248
        },
        {
            "id": "856cd60e-4fad-4ae9-974c-0bb46c8e28ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 257
        },
        {
            "id": "d614b945-aa6b-4362-b1e6-338ea5c5ba84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 259
        },
        {
            "id": "e655703c-005c-4c18-8160-7e67d215ef35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 261
        },
        {
            "id": "14f57520-aa54-443c-911b-6193ed81e452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 263
        },
        {
            "id": "8049ba75-8001-4bd1-b447-7a2a781f9c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 265
        },
        {
            "id": "acf740bf-f6e5-40a6-bdee-d2547d5a1f64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 267
        },
        {
            "id": "58dceaad-6759-44e7-a1aa-02ee3144c6cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 269
        },
        {
            "id": "2e8562c0-9836-4581-a7f4-319336e41dfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 271
        },
        {
            "id": "638c4280-6412-4545-ae19-420e28fdaf1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 273
        },
        {
            "id": "165418f2-96e3-49e3-8765-9c266e60b4ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 275
        },
        {
            "id": "09bced11-f3f4-4af7-b300-bb7fbb7dd4ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 277
        },
        {
            "id": "5940fd6b-6369-452c-a3c5-cfb37cbda0bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 279
        },
        {
            "id": "c4b8da08-9dfa-4d35-9448-9cb54d839de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 281
        },
        {
            "id": "67bb4d34-06f7-4c11-82b2-f81ffaa745b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 283
        },
        {
            "id": "0af188d8-63f5-48df-b6eb-144ca1a56751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 333
        },
        {
            "id": "fd6da702-2ce9-455b-a166-25b9b4d9567f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 335
        },
        {
            "id": "926b6c35-49fd-43f5-906d-79b7d490b698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 337
        },
        {
            "id": "e176597b-b59f-4ae0-94fa-f8671c7275d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 339
        },
        {
            "id": "9640163e-d571-4d24-ba6c-5d9169519cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 417
        },
        {
            "id": "b81b552a-b425-4c1b-988d-b2d927363545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 507
        },
        {
            "id": "e0788c01-cc07-4b84-ab82-1d3cc01d5910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 509
        },
        {
            "id": "6b0f2f34-d3bb-4f47-afd7-d82923b3748a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 511
        },
        {
            "id": "7c8b38ed-8c6d-42c3-aff9-e954b87aca83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7681
        },
        {
            "id": "9bcd3574-57d8-4a39-8b8b-e83cb05bba0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7841
        },
        {
            "id": "9a9aa270-7f18-42c1-8f1b-040edf6a0a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7843
        },
        {
            "id": "61beebb2-2c04-424f-8464-ad01b0da6877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7845
        },
        {
            "id": "0d639cba-1034-45f9-b213-90168138bfd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7849
        },
        {
            "id": "cf0b56e7-254f-4adc-9f46-e0566e4745a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7851
        },
        {
            "id": "2a4620eb-4cac-4691-8798-02efccd8265b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7853
        },
        {
            "id": "3d5489bb-8f32-4702-911e-98720af8d580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7855
        },
        {
            "id": "d0981056-a781-4dac-a7e9-bc8a8a12d845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7857
        },
        {
            "id": "3adb40ad-5904-43fc-a6d2-38abf542eb25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7859
        },
        {
            "id": "4c706f18-8edb-4665-b94c-ffa340a9465b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7861
        },
        {
            "id": "9acf3f83-5210-4a4e-a1ee-3811e8697173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7863
        },
        {
            "id": "282a0bca-9c88-4f88-b97c-d62eb19b0a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7865
        },
        {
            "id": "2dd277f7-d40c-4d08-8b62-c44972de6162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7867
        },
        {
            "id": "5e235fc9-73d8-4f7d-9a87-d0a2eb63ebdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7869
        },
        {
            "id": "f0f6a684-7154-4ffa-a009-87831267080c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7871
        },
        {
            "id": "df1692ce-827a-424f-9af0-698da3f167a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7875
        },
        {
            "id": "4e7ca900-7164-439c-abee-2100a22a66f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7877
        },
        {
            "id": "0cf6f655-236d-420c-9473-b92b19c8f328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7879
        },
        {
            "id": "15aeeb1f-e4e0-41c1-a10a-51ab3c8adbb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7885
        },
        {
            "id": "dffe473e-6123-4ef1-9089-56b0e7b5a8fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7887
        },
        {
            "id": "8754c28f-d4db-4676-85a9-5f4e1d7eb167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7889
        },
        {
            "id": "58b73aef-2d1f-4a92-85bc-64956fcf15b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7893
        },
        {
            "id": "fa2b588d-bbed-4e4b-bf8d-cf50fd025b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7895
        },
        {
            "id": "97abfe89-b6b2-446d-a69f-8ef2aadbe0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7897
        },
        {
            "id": "2b5fd63f-cd0b-4dab-a2e4-91b26117fa74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7899
        },
        {
            "id": "441eb3a1-66dc-401b-b20a-d7f764ed86e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7901
        },
        {
            "id": "0286924d-8712-4972-a9fc-545ae79dac7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7903
        },
        {
            "id": "cd4a6b41-4566-44ee-92ee-e85940722caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7905
        },
        {
            "id": "0c0e844d-07ac-4c26-beee-d7e1b5df3653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7907
        },
        {
            "id": "fa9ac311-7797-40db-85a6-bbb44ab4f731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "30555781-e6a4-47c8-ac24-877c439c9d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8221
        },
        {
            "id": "c5473396-4594-4b46-94d2-d832a0e8404b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 34
        },
        {
            "id": "ad82b7d1-2e92-4479-a9a7-6960c27d3d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 39
        },
        {
            "id": "53634d8a-3387-4f89-a112-5f4c4b6d894c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8217
        },
        {
            "id": "e7e74600-3823-40c0-a5c1-7c2e9278c2cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8221
        },
        {
            "id": "f98e9394-fde7-4e6a-80d7-39c89572f43e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 34
        },
        {
            "id": "d6f6139a-8856-4362-87ff-0379b6d6bec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 39
        },
        {
            "id": "fcb31cb4-6cd7-472e-8fc8-7f98029d1bbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "a5021e13-1ac5-4115-b5da-1a6b8aad5558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "339c4555-4827-48a3-92fe-6b179d75a7ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 63
        },
        {
            "id": "f735f39e-1da0-4b08-8b67-fb3d54dc06c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 8217
        },
        {
            "id": "196ba63e-3b6a-4a6c-8b9d-849cd8c73fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8218
        },
        {
            "id": "c2b81f75-9c8a-4e65-a18e-aecc4096f710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 8221
        },
        {
            "id": "d1c61ff8-8824-49b2-9032-b3c34d885be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8222
        },
        {
            "id": "13e390b3-1809-4eaa-a321-9e3a6fae5a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 34
        },
        {
            "id": "5397ef0e-30ae-425d-8b22-df3b4a13aa9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 39
        },
        {
            "id": "2fab0f20-0d6b-4db3-a0aa-3988efe067bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "b8e6eac9-174f-46f4-a7c0-f1a07efafe81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "c310042b-db02-41e0-b30c-960b05479710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 63
        },
        {
            "id": "cf312e6e-46c6-4458-8fbf-575497b06d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 8217
        },
        {
            "id": "00bf2e8b-2385-4dba-97a4-f9d400263827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8218
        },
        {
            "id": "ef71626d-0957-4eb3-a19d-cb80c25b9821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 8221
        },
        {
            "id": "19778f2f-62c4-48e7-b949-dcee6c395a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8222
        },
        {
            "id": "c33c38c6-91de-4376-ae84-2f90bea970c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "9390ef39-03f7-4ac7-a915-2f29ad6467d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "e035faec-72f6-4979-b753-baf0e0ff17cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "fc0445d4-3bf9-4a46-906f-a9867c25377d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "8e948fdd-1c30-4cd3-84e7-d730ee807d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "82c533b2-5ddc-4844-94d7-c02b409d51b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 224
        },
        {
            "id": "daa5f900-9b8c-4c4c-b854-15aeb7c85a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 231
        },
        {
            "id": "368839fc-cd5b-4e74-a991-77005be75c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 232
        },
        {
            "id": "e8c77e09-a540-418d-9c62-7ca6335a962e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 233
        },
        {
            "id": "64b479fe-4d8c-4c03-b406-ad2a629c5bed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 234
        },
        {
            "id": "5e960878-3190-4b21-8a91-7351fd21741b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 235
        },
        {
            "id": "07062324-1004-4562-8752-603dffcf377a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 242
        },
        {
            "id": "f9055884-4074-4af3-bd72-d35b91ee5153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 243
        },
        {
            "id": "63dcb15a-3507-489d-807f-bcaa69e4c909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 244
        },
        {
            "id": "501e5dfb-b655-4cef-84c5-6ae0150b9d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 245
        },
        {
            "id": "9cd79a8a-c4e0-4cae-8528-ad033c63049a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "afe5570a-ffd5-4383-b7b9-6b8688098154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 248
        },
        {
            "id": "f4a8c7ea-dd25-4487-b687-aef445a2266a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 263
        },
        {
            "id": "133fde19-d011-4371-8105-254b0bf6ba72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 265
        },
        {
            "id": "392f521b-36fe-4803-b7ea-1d6d8511ae4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 267
        },
        {
            "id": "a9c1128b-6553-4567-9f4c-8559cd211457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 269
        },
        {
            "id": "de10bba7-274f-4331-bfef-173c4975b7c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 271
        },
        {
            "id": "0f3567f2-8fef-43ce-82bf-dc16f0314423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 273
        },
        {
            "id": "053c1335-f597-4054-8712-6adcef46277e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 275
        },
        {
            "id": "50bd47b6-b0bc-4e61-ab0e-32af4249b78b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 277
        },
        {
            "id": "c36a6731-ee0b-41ad-9d91-d0ce34c206d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 279
        },
        {
            "id": "b76bc1b6-75a6-47a9-ab92-ee1eefc1ec47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 281
        },
        {
            "id": "460b0f83-517f-4ae1-ad17-9a5867bc3ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 283
        },
        {
            "id": "b6585dd8-1c85-45d1-8fec-e06cd1111193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 333
        },
        {
            "id": "041c8ec7-619c-45d8-b1f7-67a992324cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 335
        },
        {
            "id": "8bfe796c-8df3-427f-abe2-15e605653989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 337
        },
        {
            "id": "0c44aec3-ada9-4441-8a05-b38cd595bd36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 339
        },
        {
            "id": "813a724b-f308-4348-9747-9492c092b933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 417
        },
        {
            "id": "2242597d-efd0-4b7a-ae76-969bf4ff62e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 511
        },
        {
            "id": "cccc4b65-52ac-4de9-812c-b0cb8f4a9620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7865
        },
        {
            "id": "2cf1b727-a2bf-4780-a62e-f3964003715c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7867
        },
        {
            "id": "3eac204d-f9ae-4cc2-b341-5efa90642763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7869
        },
        {
            "id": "28b7475d-7ce7-4d75-87da-71de91e48153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7871
        },
        {
            "id": "cc470ddc-708b-4274-aeda-c16a1e6c1326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7875
        },
        {
            "id": "873bd387-2961-43f2-b185-37e061df3254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7877
        },
        {
            "id": "1d783c49-3c2d-40f8-80b4-2821717553ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7879
        },
        {
            "id": "3f61ce17-97ae-4cd0-8c6a-26b7ce958726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7885
        },
        {
            "id": "aec9100d-1aa9-4b7d-a105-15c68816d0f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7887
        },
        {
            "id": "67191589-e210-4840-91f2-a1284d4f652e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7889
        },
        {
            "id": "226675eb-209e-4c4f-aa64-769aba9cb661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7893
        },
        {
            "id": "148fc422-5518-4ccb-87fd-d9957df8f001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7895
        },
        {
            "id": "b3073a85-8882-4084-8fd6-3482192a9f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7897
        },
        {
            "id": "ccb2ab3a-69cb-4f60-99db-31a212f14d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7899
        },
        {
            "id": "902b0927-e2d7-4ec2-9521-436464b655ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7901
        },
        {
            "id": "0dfb2a79-c815-406b-b642-7d8d84fc1934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7903
        },
        {
            "id": "6b85cc86-0411-4772-a02f-be36e352bcd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7905
        },
        {
            "id": "2b9daa9d-7682-4554-b4ee-ba58e7764365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7907
        },
        {
            "id": "0c7cf47d-48e1-4bb3-ba71-f69221d0098c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 34
        },
        {
            "id": "18dcacd9-7d12-44c0-8d89-334c32f0ae0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 39
        },
        {
            "id": "c6acfca6-a0ae-4948-b7ad-1d076d1433af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "df9f852a-a1a3-42fd-8de5-e595c9f6d138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "c84d9e0b-6c91-40ef-ab3b-9fea0e81af52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 63
        },
        {
            "id": "d95d675f-704d-4400-862d-1b67c0b84a56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 8217
        },
        {
            "id": "09f584a0-94ef-4ae0-9f68-c51c2b77f92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8218
        },
        {
            "id": "ef3599ef-0cdd-4623-99fd-d914f60a3dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 8221
        },
        {
            "id": "58ed709c-ad8b-4169-9a2c-ab29b6dd8158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8222
        },
        {
            "id": "0623992f-e631-4770-a668-521cbc8b3c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 123,
            "second": 74
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Extrabold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}