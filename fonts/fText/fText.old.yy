{
    "id": "5a627af8-46d6-49cc-91e4-608622154ffd",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fText",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "MS PGothic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e3cb695e-7845-41d3-ac54-97e110c7457f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "99b95a1c-f4c6-4bf1-86db-8ed1ed77f48b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 18,
                "y": 142
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "634b6857-d68d-461d-bbe6-1cac8cdd88da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f051a0c6-e0f5-4b0d-ac3b-ea74548be833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 233,
                "y": 107
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "de510f61-60d5-4732-8e05-91086dadc222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 218,
                "y": 107
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d0b02a03-2249-417f-8565-fc0350caf428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 201,
                "y": 107
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c995a86d-e723-4fea-811c-62cb1d445fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 183,
                "y": 107
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "fcd638c5-8ada-4ea2-844f-ac5538982632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 4,
                "x": 177,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "bb8a7334-73b1-4b54-8f0a-b0aa335fc689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 7,
                "shift": 16,
                "w": 8,
                "x": 167,
                "y": 107
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "24682b6f-43c6-4499-abba-6790329d1c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 9,
                "x": 156,
                "y": 107
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cb78820f-c9bd-4030-94ea-9802a098d3e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 24,
                "y": 142
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4807f3cb-a623-4adf-bd16-344c76c33e8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 138,
                "y": 107
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fd88b5c0-1a2c-460d-89ee-3ae97c045d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 4,
                "x": 116,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f9bbdc64-b1dc-4d5a-9467-0beb3784cba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 99,
                "y": 107
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ed4ef1bd-2bc2-496b-bcdb-ec409fb65e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 4,
                "x": 93,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "34604ae0-9aa2-4a46-8681-aed7de466ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 75,
                "y": 107
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6c74e601-198d-41e8-b622-9182b3e03a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 59,
                "y": 107
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "dd70116d-ceb9-4c82-bcb3-681e1b38fd13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 3,
                "shift": 16,
                "w": 7,
                "x": 50,
                "y": 107
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fa25edd0-a9cc-4450-bfcf-a5a1f36cd5ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 107
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "19b6ff32-408c-4d8b-97db-9abb0de279be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 107
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "05a37db1-7d3d-4683-bd9b-84f86d470c71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "abb9269d-08d3-42d1-aec9-7dd9c990e733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 122,
                "y": 107
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "901af51c-19a6-4f2b-a202-cbd9933d2d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 42,
                "y": 142
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a59318df-6e0a-4816-94c8-a8d57d74d32c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 58,
                "y": 142
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ae6b58d9-19c7-472c-bbb3-a2a0fd4ace1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 72,
                "y": 142
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8d6ab8f6-46cb-42f4-9043-aca601389f78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 177
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "27a6fbb0-9bd6-4ac5-8275-bf70007d3afb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 140,
                "y": 177
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "59eb9162-26ea-49a1-a6b4-1dc46a2884b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 134,
                "y": 177
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "cfa62ebb-69a1-422e-b949-c196a874bf0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 118,
                "y": 177
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "24da3431-8db5-4cd2-8164-24abe9c92164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 101,
                "y": 177
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "70eed1e8-7c22-4db9-b66f-a3344e9dab3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 85,
                "y": 177
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7f9e9cff-2b03-4183-be55-b95948d19277",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 70,
                "y": 177
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e4bbf4a2-d5b7-4368-8156-f58446dd60aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 52,
                "y": 177
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a3802218-7ffd-4e52-adc8-c25c33145ab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 34,
                "y": 177
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6fa74a7a-73ba-42a4-a5e0-c507df0fbca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 177
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ace74230-8a90-49d1-a065-669552ee58c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 177
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "54d054d4-0298-428e-a6f2-cca3e1133c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 225,
                "y": 142
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f9f97d3a-cedc-4b26-b0f4-3845ba621ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 209,
                "y": 142
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3cc73bf6-384b-4119-af37-5330bc743513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 193,
                "y": 142
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "34a7a751-2299-4bdf-9e6f-737f56c7bbc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 177,
                "y": 142
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f2596c52-f8ed-4c2a-bc9f-1805f87a720e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 161,
                "y": 142
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6e8a47bd-d1f4-4703-bd20-10ec0b0916eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 153,
                "y": 142
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5f917f96-9155-406f-ac0b-a70dbcc58bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 137,
                "y": 142
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "11755684-ab4f-4a5e-a494-c87fe1bfdaff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 120,
                "y": 142
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "308706f7-29ee-4260-858a-83479afc2580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 104,
                "y": 142
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7bfb874a-4ee6-4f45-8f71-558a7b8c2d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 88,
                "y": 142
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "31053c9e-7ee8-4dfd-8ba7-d839ee3deccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 238,
                "y": 72
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "418edc1b-4b7f-4ffa-af50-60b71128f467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 222,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "37ee26e0-1bbe-4b0e-93b3-deb399274625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 206,
                "y": 72
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "abc94202-cd92-4131-9219-8b2ae6554223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 37
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ca6e9a36-3495-4c7c-87b6-75dcfeef5809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 86,
                "y": 37
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ab897225-29f0-4ab8-97ed-8dc8f2c27c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 70,
                "y": 37
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "937afb42-05e3-40df-8258-3c4f5426965e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 54,
                "y": 37
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9ee7bc6c-db62-4156-aa14-aedb954b092b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 38,
                "y": 37
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6fe77be2-68ea-4e39-b2c2-da0bed913bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 20,
                "y": 37
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "409bd641-4327-48fc-a5cd-d955e042d58e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "19d67323-dde9-43ff-9d64-57d5fb355684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ad9a9cbe-ab5b-42f3-b69d-6c202ef71959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ff578cc5-ad7e-4f4c-a5db-aaf3ee9253e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "649a53ec-abbd-41ba-8024-7bbb35a61d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 6,
                "shift": 16,
                "w": 9,
                "x": 103,
                "y": 37
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9a7242e5-bcbe-48cf-8b29-878c6da53951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "21022040-a42a-4ff3-9543-de00eb3bb726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 9,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2cadf276-6e43-45c3-970c-d9699f76e1f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ce1ae811-604f-4d93-a91a-ba26e4435f25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "12d9f15f-4a44-47f3-9543-2428cf5e805f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "27fda180-9d36-4418-b21f-6cb617cc72f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "fb465269-a0e0-4702-8b29-55aa72bc0929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2d0cbdd6-5d85-4d64-80d1-d2ae68465678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "25b9bf83-b3d6-48e1-9fd4-bf87c3dbeb0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7c039de0-7124-42ef-9f12-b5ae5a3ff65b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "acc43a4a-934b-4c5e-9938-5d3d0185bf92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a6c87749-ec34-479a-a44e-059b166d7d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 37
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "67d7f084-aee7-41a6-a9be-d3c14bf36e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 33,
                "y": 72
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4dfe3085-6d03-4722-a92e-2bb024a837b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 146,
                "y": 37
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "17479346-def2-41fc-8659-791740f50a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 179,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cce4030f-cbdd-49c4-8a4e-b93a0f4f832d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 164,
                "y": 72
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c43cd6b2-6158-4f37-b16e-71119ba0949d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 158,
                "y": 72
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e31b6abe-e18d-4375-a0d3-920075132e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 140,
                "y": 72
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "31613721-cc24-44e1-9bb5-60d6ed6c7642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 124,
                "y": 72
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1d5c4fa8-0495-4c2a-a56b-b5929b99003b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 108,
                "y": 72
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "73679aaf-1357-46c1-9c89-ce688024afc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 92,
                "y": 72
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c3aec9b8-e301-46c2-a6cd-a2815540c73f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 76,
                "y": 72
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d63c278d-9f8b-435b-8139-8b0a84d4ef30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 64,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6397227e-1a3b-4299-a9c9-79d471493bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 191,
                "y": 72
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "946126cd-4a69-4d92-9aeb-b84e9af20a27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 48,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4a49e8ff-611d-4e13-ae73-7a39f9f1d096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 18,
                "y": 72
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f2a4e3e4-7f1e-48f4-958c-8e6cd32f9358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f6c35d93-26c8-4e9b-83a0-5d83b94c54e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 223,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e9780c5d-f979-4b8e-bd35-b9275403d65d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 207,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "51181b83-8149-4341-a883-8fee9bc66378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 191,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "fec6a7d5-07f6-468e-a747-84fbe0a7b576",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 176,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4fbe99d7-1217-483f-b4bf-ed884c11b84b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 7,
                "shift": 16,
                "w": 7,
                "x": 167,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9710f3c8-e5b0-4cf5-bc70-5c85a8b23401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 161,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "557c950a-1061-405a-8fd6-4b40c4be4198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 7,
                "x": 152,
                "y": 37
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "fde59940-5069-4011-aadf-f743ed629a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 177
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "43ee4f36-f14c-4f92-8de1-f1b4291b5285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 33,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 178,
                "y": 177
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}