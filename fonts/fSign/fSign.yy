{
    "id": "16b7aa42-0437-4140-9a8e-8f9f6fef9389",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fSign",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Console",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "26c39a17-55df-4ce2-bfcf-eae81389cc8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b2960d5e-94b7-4457-9ff2-aa4e3270a8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 192,
                "y": 40
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "03b88381-05df-4cf5-9b25-1b55f1a56fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 183,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "021d9a29-acfe-422c-be84-42d5ad6f239f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 171,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2e733e10-de77-4cf6-9604-9b974ea3c958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 161,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6eada5ba-e305-40e7-93e7-e66a11781d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a39dcec2-6eb2-4334-955f-ae436c29a218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "38ae2cde-7fc3-4307-af86-4d44b085c1e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 132,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d77843e0-f184-44f3-80f5-00751b01318d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 123,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9d11d257-057a-4263-a595-8d8dde13b53c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 115,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e9317ac7-d188-46ec-8914-89b0ecadf873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 197,
                "y": 40
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "60546804-d429-4679-9a9a-51ff931ec404",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 104,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6955936d-2f01-4781-b7bb-b676924aceaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 90,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d266e984-475b-4e58-a331-c115f78e996e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 81,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "52f9e145-d1ee-464d-a4dc-15af57a8d058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 76,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c78cf97c-92bb-4cec-bcc7-8a53eab7e306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 64,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7c7b8383-2ca4-41cc-a0ed-c53326d86ce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 53,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "69326018-e9b7-49af-b6d2-8a3c0a2fa6f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 42,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c2d667ac-0edb-4733-8874-44c6c237175b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 33,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c68eff77-7609-4cba-bcd6-4c90f72b03b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 23,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d1f1da8c-c913-417a-a5f0-ec78c2f42774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 12,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "230e22ee-0963-48bd-8b11-b4230a05e41b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 95,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2a62c992-3c5e-45ef-a324-5572b5856e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 207,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e1932865-b666-47c9-8a77-5a4b54f0952f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 217,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9ef92b74-461f-4dae-82a7-ba5aa15df735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 227,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "46bee75f-9125-4567-a33f-db73fe822889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 194,
                "y": 59
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "931212f4-c65a-470b-97fb-0ef9ae68606c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 189,
                "y": 59
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c8c5985e-4fe6-4ea7-8b12-8852286779a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 184,
                "y": 59
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "eaf732cf-ab29-4843-98e9-b1bed777e22f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 173,
                "y": 59
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b7e4d334-6b02-41ca-9742-b6d8c43a023e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 162,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d13c8ca4-fb2c-4609-b1f1-60722aaea6ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 151,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cc64f2bc-7c08-4d53-becc-c67a3d3e7055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 141,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "76891618-d4bf-4851-a19e-465668e26f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "67d4d888-e4a4-4f28-af5f-c106ab4f6a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 117,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c0030836-a861-4200-9e80-40da6eedf35b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 107,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d5852705-309d-45ce-b902-1085dd13c1e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 95,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "03d5c6b9-76ae-4458-9b8f-4471960355f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 84,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "60a6d8d8-23de-4937-b81a-4035adc6934b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 74,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "88f2a19d-3424-4635-9dca-133768a79594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 63,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1114216a-335d-412b-bca1-70d01f7c4128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 52,
                "y": 59
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "44cb7fad-f0a0-42db-b807-c5f8f8a7ee4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 59
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4bb51202-8bf3-407c-aef2-4df7f1cac903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "bdb1aead-1d5d-4d92-94b1-d1b3a46e99e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 23,
                "y": 59
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8f588ae1-db14-4539-91cf-47b9df78ee42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 12,
                "y": 59
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f033f9b8-ba9f-4e5f-a06b-d50f45aa749d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e8238bfc-12fe-4f76-80a8-372a6f932a96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 237,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bb5d9bd1-a643-4af7-a682-dd0a2c1370c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7fdda7dd-45d5-4c54-8b0e-1e9b660f2498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 241,
                "y": 21
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b9afa40f-b663-4db1-a9a9-b53df6e5458b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "382c9044-ee1b-4bd7-82d6-e84d438489a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "60b3f48c-9624-431c-bf99-6a17d5614cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "033ff6ca-51e1-42e9-8b63-884c2ca652ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a66e9b24-e3e7-4e30-972d-50c8e51b165e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3b11f2e5-9a97-4dcb-9250-ee431fe77266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "41b85ede-20ea-4453-aa64-eb68fba48cf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f2b8e6e8-b68d-4086-adb3-02f1c37aa2d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8dd28342-3eb2-4f40-b74e-99d2dcc235cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1db88575-523b-4897-8cc9-50c3db99fe2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "99ab4930-5a7a-4f80-860e-c57bb85ca7ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "141a87da-3b16-4eba-8091-51876805337c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "cc90189b-aed5-40d2-adbf-ef9aa645b3f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8934f97b-7dc1-4515-9a50-b0aea560013a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c4ffecc2-fff3-4502-bbd7-f4a2f7f5f8ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d0e39dfa-26ee-4a67-9de7-7d4a927712bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "01a6f3d7-958f-48fb-9ea8-746bf9370bc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "42c28456-6520-4ef1-9a00-523d2bbb9bdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a518f4f0-dd6a-4785-81ea-f59ebdfb6240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ec642224-89a4-45f3-a098-167b6191f89a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5584fc2d-80e0-4798-8f65-89c9de7ef490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "25feb0a7-a30f-4f5c-84c2-2799627dc82a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "64c326ea-7704-4238-aea4-bb01f9f7934d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ff3d542a-7718-4a9a-abd8-ac7b68e4fadd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "61bb61dc-3366-46e4-91b5-f03cf3b4edc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 111,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "dd8481f2-6971-487a-8dbf-d179a8d74ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 13,
                "y": 21
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "63ad7060-f72d-477a-88fa-0f92f7a6e232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 212,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cde2a6d3-219f-4800-b4d2-3a802426ba27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 201,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "81a882e0-423e-4fa0-b470-bf0d082d1624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 193,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "79cf5323-0953-4abf-99ba-a8ef045137d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 182,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e86547e8-e9c4-4577-a1dd-191eff5147b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 172,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ac506ca4-72cd-4c5c-9bcb-09e2795f15f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 161,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cef9e1b9-1337-4f6b-9958-f174f548ff51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 151,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2abeeb26-d5c5-499a-a76e-a5f5f2cb90ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 140,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5fb83848-3a29-4698-be48-b9748f6ace9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 131,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ad020250-3a4e-4679-a58d-a72f09e18334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 220,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "611e2098-c494-4485-8eea-c8ce18bab24b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 121,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "961ca0c5-07bf-431b-9580-0876366ea4f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 101,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f38c1f19-a56c-43fe-8a4a-b88436ebd990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 89,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d05320a6-2ff6-4c29-b3c2-b6dfc0850323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 77,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "11c648cd-1a30-4d61-896c-42086cc238b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 65,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2529af43-e529-4c45-9633-320e3b9aa4b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 53,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f0842d9c-cff7-49fe-8269-fbf9a2e9cc40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 43,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "52a9be5c-738a-47ce-8ff7-08ae64f5b7b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 34,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6ba610c8-20fd-4318-a0e4-74b3c9bc8637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 30,
                "y": 21
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "90ba8336-6017-46a5-b8ec-3c061ee3b0c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 21,
                "y": 21
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "da96d9ac-42ba-4002-a769-9d02220f9835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 204,
                "y": 59
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "0f7312a7-260f-4a4c-a25b-755b9aed4b8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 215,
                "y": 59
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}