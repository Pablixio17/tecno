{
    "id": "16b7aa42-0437-4140-9a8e-8f9f6fef9389",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fSign",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "MingLiU_HKSCS-ExtB",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "87ed9a1e-97f8-4b10-aa25-3a39027e79ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8e4863ba-1acd-41c2-911b-ec90bf30e038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 94,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "253ece09-4688-4390-af47-584f2e4ca3f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 88,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a9928dc3-55d7-4b75-9a56-67cf5e7bda12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d970f4c3-917a-4fb7-89f3-ad6efaa2e6f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 69,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "09317702-727d-4c9f-b961-883435d3eb56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 59,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0442d449-6c5a-46f9-86fd-b8146309645e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 49,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "816ea782-fe4b-4739-84de-8cfa29a333fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 45,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "721298a6-e76e-4f29-a2d5-8a158caacfad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 38,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "dacc349a-2853-4052-a489-bff81cd2ee1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 32,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "01d3b92e-6e02-4889-97e7-d32e25993856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "85b40735-24af-4296-9577-82ed6d33507f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8e8325ec-c9eb-472f-a377-2ed2ed47d913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 8,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7afb0217-97ff-469e-b34d-2aaabf06647a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a289c2ad-7aa1-40b0-8dac-fc7f4f27ace5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 249,
                "y": 23
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "68ab27d4-a284-4c06-84da-0c098319a840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 241,
                "y": 23
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "442ea088-56b9-47e1-b026-46dc2ae0af01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 231,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b53d84c1-f7a1-4f4f-9fe4-0fc292637189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 223,
                "y": 23
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9f742db8-f782-490d-8696-3ade9694957b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 213,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "429671ed-b9b6-4606-99ea-c318f074dbdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 205,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1ca2d677-5d16-4a18-808d-48ec3754b4b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 195,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ac67cbb3-fad3-4886-9887-0fbc2ea3de24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 13,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "cecef693-3f17-498f-9d3d-d3e99ebd93a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6fc1e6c7-9cd4-4e10-a45e-a2daec19c72c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8cdd43e4-0b72-4954-a464-6d25a549fd59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 126,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6196d516-8d81-4451-9285-f1f1a7f64c88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 69,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f9781ca6-2919-4d3b-aab6-419c138108b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 65,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6c57cba1-3f68-4dfc-8583-3e1680654d9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 60,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "69e32d2a-d5dd-4599-b967-25b61b004b1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 50,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6d55c953-a5db-470a-9f10-30f117d05030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 40,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "78c78a52-f941-4211-8e81-0e6e1f5858ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 30,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e047c0ae-8309-4c27-b6ae-6ecc2564a831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 22,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "43249b2f-d6bd-4231-a874-abd95e92f74d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "927d83ae-53e5-4ce4-af1c-7dce21df78f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8b5fb49b-76c9-44db-880a-c620b182bc65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 243,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cca262c7-6237-4297-a899-0e15e240139c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 233,
                "y": 44
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b049a7e9-d1e6-4315-84a6-b6fae1ab0359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 223,
                "y": 44
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f76087c8-c245-4321-a978-b65e662e2e72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 213,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6ddd7a73-b8ef-4dbf-b338-99d0042cf7ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 203,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "de4b17f3-6f8f-49e7-9687-07f4eda5ad21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "31839fb3-de9a-4345-af8a-60d709538ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 183,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d3ef5af2-68ab-45e4-8c85-ef76b4eb0e23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 175,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d4c093b1-ff57-4675-b6a3-189374fc69a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 166,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "23a8cbf0-54ca-4fa2-9d45-95c6cdd46d4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 156,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e4f1c2aa-c60a-4833-8db7-e8b651abedf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6b69c430-9292-45a6-8a1e-13b6e3b31b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 136,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1c7541fc-3171-4468-a14f-07c5317afada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 185,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cabc2af6-035d-4e61-9b52-885c7a98206e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 65
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f7fac01b-784c-4e1b-93e1-e78d09166324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 175,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "79026d58-9d02-4fd8-9639-7f0dca84dd74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 163,
                "y": 23
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "890c8547-1e8b-4ce4-a99c-c1c89655a1d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5671ca62-222b-4913-b246-52612e17feea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fb69bbe6-d231-43ae-ab7e-05189fa6cedb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "90f637d8-03ab-4ffc-abc5-58511d6ddd08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fbcc36f2-4d00-41a2-b733-31aca2a34cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fb5e1c12-6903-485a-93bc-897db3d8c577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "088e6327-993d-4be4-b0a0-f178e4b04bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c8ccbfbe-3ba6-4a24-9ecc-dfc2414ea4eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "63969f10-c711-4402-a91d-c07e193d267d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "276e2672-89c3-4911-8179-1be98795aee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c0e2de81-8b6a-4500-bfaa-b4c65ae21792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8281d728-f2f9-4f7b-ad79-d57e189480e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "84743315-ac3c-42c6-817d-c2443f90051b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "58ce51f1-867f-4549-90f4-4cc24cb7efd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f4c4afb2-028d-4fb6-9b7a-0a3939aa7adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8a48e2d0-464f-42c3-b851-2ae710b058b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f7c5c46b-7d99-4522-8afa-cfdcf18b4142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "19b86ef5-5a69-4335-81b1-758f6c915aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4171d5dd-2525-4e99-9e08-9398edc06829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "57c6d7d3-bf07-4c63-af76-aca5247180f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "977b13cb-387d-4a71-93ef-cb0862a1cef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "78601d2f-1e1f-45b9-8832-5639739d3afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4be0ab82-4b1a-4fce-b825-fc6a9e0c5c8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a70e0d93-57ca-4046-9505-087e224d446f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "845d37eb-48cf-4615-ae53-795a469962f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 156,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b3b89694-db53-477b-ba80-64f967169704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 146,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bbc62f79-ef54-47f3-9df4-d705ed30b9ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 140,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "56c19fd5-8963-4e3e-8b5a-0f0a04b5bf7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cf9c727f-c16a-4fc2-84e2-f23a45a1c3fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 120,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1e27e165-0ff5-405a-8b36-397ebce316d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 110,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a3e96c65-0367-479e-92f1-fe2e90dfe6f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 100,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "07f1fc67-b73b-4300-9ff6-a8dd55380ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 90,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "58fa392e-5b91-4e94-b8ed-f6e7d91e57c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9cf7af03-590b-4ea9-9903-eeded1e95e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "31c97b5a-ae12-45f1-8846-5c16f5c9cbb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 67,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d14a7aad-3fd5-471a-acdf-d9a0287d09e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 57,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "27a71398-2c7d-454f-8161-34257c2cbdad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 47,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fdc5973e-ffe3-4243-9c0d-2d714e4db2d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 37,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9956957a-b83a-4110-95df-4a683211de27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 27,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6e3beea2-2bf8-498a-bd6a-d3aa9a0b0b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 17,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d233f783-b113-4cfa-b38e-52bc6ee3e42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 9,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "60f6fe84-cd4b-429e-b5e3-d19a819199cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "90bbb309-6be7-4da1-bfb9-9c15760666f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 246,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "12c2df38-a8c4-45b1-8278-1546044e40eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "464b9823-ab85-4402-a897-1aa98795450d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "c15b1549-0e8d-4b5c-bb14-0d8dd8e56cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 0,
                "x": 173,
                "y": 23
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "a5af7ef1-32a5-443c-ac9f-521faacca574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 89,
                "y": 65
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}